package gpstjn;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.MultipleInputs;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Map;
import java.util.TreeMap;

import static java.lang.StrictMath.abs;

public class Gpstjn extends Configured implements Tool {


    @Override
    public int run(String[] args) throws Exception {
        int status = -1;
        Configuration configuration = new Configuration();
        Job job = Job.getInstance(configuration, Gpstjn.class.getSimpleName());
        MultipleInputs.addInputPath(job, new Path(args[0]), TextInputFormat.class, GasStationMapper.class);
        MultipleInputs.addInputPath(job, new Path(args[1]), TextInputFormat.class, ChangepriceMapper.class);
        job.setJarByClass(Gpstjn.class);
        job.setReducerClass(Gpstjn.GpstjnReducer.class);
        job.setMapOutputKeyClass(Text.class);
        job.setMapOutputValueClass(IntStringTriple.class);
        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(Text.class);
        FileOutputFormat.setOutputPath(job, new Path(args[2]));
        job.waitForCompletion(true);
        status = job.isSuccessful() ? 0 : -1;
        return status;
    }


    public static void main(String[] args) throws Exception {
        ToolRunner.run(new Configuration(), new Gpstjn(), args);
    }


    public static class GasStationMapper extends Mapper<LongWritable, Text, Text, IntStringTriple> {
        @Override
        public void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
//            System.out.println(value);

            String id = "";
            String latitude;
            String longitude;

            String[] line = value.toString().split("\t");
            if (line.length > 9) { //line is valid with length bigger than 9 to clean the database from broken entries
                id = line[0];
                latitude = line[line.length - 2];
                longitude = line[line.length - 1];
                context.write(new Text(id), new IntStringTriple(latitude, longitude));
            }


        }
    }

    public static class ChangepriceMapper extends Mapper<LongWritable, Text, Text, IntStringTriple> {
        @Override
        public void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
//            System.out.println(value);

            String id = "";
            String price = "";

            String[] line = value.toString().split("\t");
            if (line.length == 7) { //line is valid if its length is 7 to clean the database from broken entries
                id = line[1];
                price = line[4];

                //round and shorten price
                int intPrice = Integer.parseInt(price); //1121
                if (abs(intPrice) % 10 < 5) {
                    context.write(new Text(id), new IntStringTriple(intPrice / 10));
//                            Text(String.valueOf(intPrice/10)));
                } else {
                    context.write(new Text(id), new IntStringTriple((intPrice / 10 + 1)));
//                            new Text(String.valueOf(intPrice/10 +1)));
                }


            }


        }
    }

    /**
     * combiner is not possible to write with the used algorithm, list is needed as mapper output
     */
    public static class MyCombiner {
    }

    public static class GpstjnReducer extends Reducer<Text, IntStringTriple, Text, Text> {

        protected void reduce(Text key, Iterable<IntStringTriple> values, Context context)
                throws IOException, InterruptedException {


//            System.out.println(key + ":" + values);
            StringBuilder sb = new StringBuilder();

            String latitude = "";
            String longitude = "";
            Map<Integer, Integer> map = new TreeMap<Integer, Integer>();


            for (IntStringTriple d : values) {
//                System.out.println(d);
                if (d.getHashvalue().toString().length() != 0 && d.getCleanvalue().toString().length() != 0) {


//                    System.out.println("enth�lt latlong werte");//regex matches 49.2509000000000015	11.2281499999999994
                    //das ist der Fall bei einer Kommazahl
                    if (latitude.length() == 0 && longitude.length() == 0) { //checks only if latlong is empty because this is only needed once
                        latitude = d.getHashvalue().toString();
                        longitude = d.getCleanvalue().toString();
//                        System.out.println("Latitude Longitude Wert gefunden" + latitude + " " + longitude);
                    }
                } else {
//                    System.out.println("keine double values vorhanden");
                    //das ist der Fall bei einer Preisangabe
                    Integer intPrice = d.getI();
                    if (map.containsKey(intPrice)) { //new appearance of already sighted diesel price
                        map.replace(intPrice, map.get(intPrice) + 1);
//                        System.out.println("enth�lt key" + intPrice);
                    } else { //new diesel price
                        map.put(intPrice, 1);
//                        System.out.println("key neu gefunden" + intPrice);
                    }
                }
//forms the output: per key one name of the


            }
            for (Map.Entry<Integer, Integer> entry : map.entrySet()) {
                sb.append(entry.getKey());
                sb.append(":");
                sb.append(entry.getValue());
                sb.append(" ");
            }
            BigDecimal biglatitude = new BigDecimal(0);
            BigDecimal biglongitude = new BigDecimal(0);
            if (latitude.length() != 0 && longitude.length() != 0) {
                biglatitude = new BigDecimal(latitude).setScale(8, RoundingMode.DOWN);
                biglongitude = new BigDecimal(longitude).setScale(7, RoundingMode.DOWN);
            }
            context.write(new Text(biglatitude.toString() + " " + biglongitude.toString()), new Text(sb.toString()));
        }

    }
}
