package gpstjn;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.WritableComparable;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.util.List;
import java.util.Objects;

public class IntStringTripleTryout implements WritableComparable<IntStringTripleTryout>, Cloneable {

    private IntArrayWritable i;
    private Text latitude;
    private Text longitude;


    public IntStringTripleTryout() {
    this.latitude = new Text();
    this.longitude = new Text();
    this.i = new IntArrayWritable();
    }

    /**
     * Constructor for double values.
     *
     * @param latitude
     * @param longitude
     */
    public IntStringTripleTryout(String latitude, String longitude) {
        this.latitude = new Text(latitude);
        this.longitude = new Text(longitude) ;
        IntWritable[] intWritables = new IntWritable[10];
        intWritables[0] = new IntWritable(-1);
        this.i.set(intWritables);

    }

    /**
     * Constructor for int value.
     *
     * @param i
     */
    public IntStringTripleTryout(List<Integer> i) {
//        this.i = i;
        this.latitude = new Text();
        this.longitude = new Text();
    }

    /**
     * Constructor for all three params.
     *
     * @param latitude
     * @param longitude
     * @param i
     */
    public IntStringTripleTryout(String latitude, String longitude, List<Integer> i) {
        this.longitude = new Text(longitude);
        this.latitude = new Text(latitude);
//        this.i = i;
    }

    /**
     * compare To
     * @param other
     * @return
     */
    public int compareTo(IntStringTripleTryout other) {
//        for(int i: this.getI
//        if (delta != 0) {
//            return delta;
//        }
        int deltalat= latitude.compareTo(other.latitude);
        if(deltalat != 0){
            return deltalat;
        } int deltalong = longitude.compareTo(other.longitude);
            return deltalong;

    }

    /**
     * write method
     * @param dataOutput
     * @throws IOException
     */
    @Override
    public void write(DataOutput dataOutput) throws IOException {
     latitude.write(dataOutput);
     longitude.write(dataOutput);
//        dataOutput.write(this.i.to);
    }

    /**
     * read method
     * @param dataInput
     * @throws IOException
     */
    @Override
    public void readFields(DataInput dataInput) throws IOException {


       latitude.readFields(dataInput);
       longitude.readFields(dataInput);
//        i = dataInput.readInt();
    }

    /**
     * equals method
     * @param o
     * @return
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        IntStringTripleTryout that = (IntStringTripleTryout) o;
        return i == that.i &&
              this.latitude.equals(((IntStringTripleTryout) o).latitude)
                &&this.longitude.equals(((IntStringTripleTryout) o).longitude);
    }

    /**
     * hashcode method
     * @return
     */
    @Override
    public int hashCode() {
        return Objects.hash(i, latitude, longitude);
    }

    /**
     * clone method
     * @return
     */
    public Object clone() {
        return new IntStringTripleTryout(latitude.toString(), longitude.toString()
                );
    }

    public IntArrayWritable getI() {
        return i;
    }

    public void setI(IntArrayWritable i) {
        this.i = i;
    }

    public Text getLatitude() {
        return latitude;
    }

    public void setLatitude(Text latitude) {
        this.latitude = latitude;
    }

    public Text getLongitude() {
        return longitude;
    }

    public void setLongitude(Text longitude) {
        this.longitude = longitude;
    }

    @Override
    public String toString() {
        return "IntStringTriple{" +
                "i=" + i +
                ", latitude=" + latitude +
                ", longitude=" + longitude +
                '}';
    }
}
