package gpstjn;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.WritableComparable;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.util.Objects;

public class IntStringTriple implements WritableComparable<IntStringTriple>, Cloneable {

    private int i;
    private Text hashvalue;
    private Text cleanvalue;


    public IntStringTriple() {
    this.hashvalue = new Text();
    this.cleanvalue = new Text();
    }

    /**
     * Constructor for double values.
     *
     * @param hashvalue
     * @param cleanvalue
     */
    public IntStringTriple(String hashvalue, String cleanvalue) {
        this.hashvalue = new Text(hashvalue);
        this.cleanvalue = new Text(cleanvalue) ;
    }

    /**
     * Constructor for int value.
     *
     * @param i
     */
    public IntStringTriple(int i) {
        this.i = i;
        this.hashvalue = new Text();
        this.cleanvalue = new Text();
    }

    /**
     * Constructor for all three params.
     *
     * @param hashvalue
     * @param cleanvalue
     * @param i
     */
    public IntStringTriple(String hashvalue, String cleanvalue, int i) {
        this.cleanvalue = new Text(cleanvalue);
        this.hashvalue = new Text(hashvalue);
        this.i = i;
    }

    /**
     * compare To
     * @param other
     * @return
     */
    public int compareTo(IntStringTriple other) {
        int delta = this.i - other.i;
        if (delta != 0) {
            return delta;
        }
        int deltalat= hashvalue.compareTo(other.hashvalue);
        if(deltalat != 0){
            return deltalat;
        } int deltalong = cleanvalue.compareTo(other.cleanvalue);
            return deltalong;

    }

    /**
     * write method
     * @param dataOutput
     * @throws IOException
     */
    @Override
    public void write(DataOutput dataOutput) throws IOException {
     hashvalue.write(dataOutput);
     cleanvalue.write(dataOutput);
        dataOutput.writeInt(i);
    }

    /**
     * read method
     * @param dataInput
     * @throws IOException
     */
    @Override
    public void readFields(DataInput dataInput) throws IOException {


       hashvalue.readFields(dataInput);
       cleanvalue.readFields(dataInput);
        i = dataInput.readInt();
    }

    /**
     * equals method
     * @param o
     * @return
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        IntStringTriple that = (IntStringTriple) o;
        return i == that.i &&
              this.hashvalue.equals(((IntStringTriple) o).hashvalue)
                &&this.cleanvalue.equals(((IntStringTriple) o).cleanvalue);
    }

    /**
     * hashcode method
     * @return
     */
    @Override
    public int hashCode() {
        return Objects.hash(i, hashvalue, cleanvalue);
    }

    /**
     * clone method
     * @return
     */
    public Object clone() {
        return new IntStringTriple(hashvalue.toString(), cleanvalue.toString()
                , i);
    }

    public int getI() {
        return i;
    }

    public void setI(int i) {
        this.i = i;
    }

    public Text getHashvalue() {
        return hashvalue;
    }

    public void setHashvalue(Text hashvalue) {
        this.hashvalue = hashvalue;
    }

    public Text getCleanvalue() {
        return cleanvalue;
    }

    public void setCleanvalue(Text cleanvalue) {
        this.cleanvalue = cleanvalue;
    }

    @Override
    public String toString() {
        return  i + ","+ hashvalue + ","+cleanvalue;
    }
}
