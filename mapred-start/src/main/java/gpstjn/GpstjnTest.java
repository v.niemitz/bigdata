package gpstjn;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mrunit.mapreduce.MapDriver;
import org.apache.hadoop.mrunit.mapreduce.MultipleInputsMapReduceDriver;
import org.apache.hadoop.mrunit.mapreduce.ReduceDriver;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class GpstjnTest {


    MapDriver<LongWritable, Text, Text, IntStringTriple> gasStationMapDriver;
    MapDriver<LongWritable, Text, Text, IntStringTriple> changePriceMapDriver;
    ReduceDriver<Text, IntStringTriple, Text, Text> reduceDriver;
    MultipleInputsMapReduceDriver<Text, IntStringTriple, Text, Text> multipleInputsMapReduceDriver;


    @Before
    public void setUp() {
        Gpstjn.GasStationMapper gasStationMapper = new Gpstjn.GasStationMapper();
        Gpstjn.ChangepriceMapper changepriceMapper = new Gpstjn.ChangepriceMapper();
        Gpstjn.GpstjnReducer reducer = new Gpstjn.GpstjnReducer();
        gasStationMapDriver = MapDriver.newMapDriver(gasStationMapper);
        changePriceMapDriver = MapDriver.newMapDriver(changepriceMapper);
        multipleInputsMapReduceDriver = MultipleInputsMapReduceDriver.newMultipleInputMapReduceDriver(reducer);
        reduceDriver = ReduceDriver.newReduceDriver(reducer);
    }

    @Test
    public void testGasStationMapper() throws IOException {
        gasStationMapDriver.withInput(new LongWritable(), new Text(
                "secd94c07-aee2-41b8-a33f-0c21417be469\t360\t2016-04-30 11:31:56\tAllersberg Rother Str. 18\tOMV\tRother Str.\t18\t90584\tAllersberg\tdeBY\t49.2509000000000015\t11.2281499999999994"));
        gasStationMapDriver.withInput(new LongWritable(), new Text("f29073c4-70f3-4f12-91d4-778b4dd6befb\t331\t2016-04-30 11:31:56\tDirlewang Mindelheimer Stra�e 33\tOMV\tMindelheimer Str.\t33\t87742\tDirlewang\tdeBY\t48.0013700000000014\t10.5065000000000008"));
        gasStationMapDriver.withInput(new LongWritable(), new Text("5e793b3b-c021-4e36-bd2c-55e357a3638e\t329\t2016-04-30 11:31:56\tBergkirchen Gadastra�e 1\tOMV\tGadastra�e\t1\t85232\tBergkirchen\tdeBY\t48.2373800000000017\t11.3528300000000009"));
        gasStationMapDriver.withInput(new LongWritable(), new Text("00060566-0001-4444-8888-acdc00000001\t2\t2015-10-19 10:04:47.789\tSchmid Tank- und Waschcenter\tFreie Tankstelle\tHohe Stra�e\t2\t73492\tRainau-Dalkingen\t\\N\t48.9156951904300001\t10.1697340011600001"));
        gasStationMapDriver.withInput(new LongWritable(), new Text("f748b5c7-407d-42bc-88f2-a12a23495804\t5\t2016-04-29 16:02:44.515\tGeiger GmbH Tankstelle Aufkirchen\tFreie Tankstelle\tAufkirchen \t161\t91726\tGerolfingen\t\\N\t49.0506515502929972\t10.6155099868770009"));
        gasStationMapDriver.withOutput(new Text("secd94c07-aee2-41b8-a33f-0c21417be469"), new IntStringTriple("49.2509000000000015", "11.2281499999999994"));
        gasStationMapDriver.withOutput(new Text("f29073c4-70f3-4f12-91d4-778b4dd6befb"),new IntStringTriple("48.0013700000000014","10.5065000000000008"));
        gasStationMapDriver.withOutput(new Text("5e793b3b-c021-4e36-bd2c-55e357a3638e"), new IntStringTriple("48.2373800000000017","11.3528300000000009"));
        gasStationMapDriver.withOutput(new Text("00060566-0001-4444-8888-acdc00000001"), new IntStringTriple("48.9156951904300001","10.1697340011600001"));
        gasStationMapDriver.withOutput(new Text("f748b5c7-407d-42bc-88f2-a12a23495804"), new IntStringTriple("49.0506515502929972","10.6155099868770009"));
        gasStationMapDriver.runTest();

    }

    @Test
    public void testChhangePriceMapper() throws IOException {

        changePriceMapDriver.withInput(new LongWritable(), new Text("52325918\tsecd94c07-aee2-41b8-a33f-0c21417be469\t1339\t1319\t1129\t2015-09-10 16:02:01+02\t21"));
        changePriceMapDriver.withOutput(new Text("secd94c07-aee2-41b8-a33f-0c21417be469"), new IntStringTriple(113));
        changePriceMapDriver.runTest();
    }

    @Test
    public void testReducer() throws IOException {
        List<IntStringTriple> values = new ArrayList<>();
        values.add(new IntStringTriple("52.1565599999999989", "10.5396757000000001"));
        values.add(new IntStringTriple(113));
        values.add(new IntStringTriple(113));
        values.add(new IntStringTriple(113));
        values.add(new IntStringTriple(114));
//        values.add(new Text("9.2\t11.22"));
        values.add(new IntStringTriple("49.2509000000000015", "11.228167898765434567994"));
        values.add(new IntStringTriple(0));
        values.add(new IntStringTriple(100));
        values.add(new IntStringTriple(100));


        reduceDriver.withInput(new Text("secd94c07-aee2-41b8-a33f-0c21417be469"), values);
        reduceDriver.withOutput(new Text("52.15655999 10.5396757"), new Text("0:1 100:2 113:3 114:1 "));
        reduceDriver.runTest();
    }


    @Test
    public void testMapperReducer() throws IOException {

        multipleInputsMapReduceDriver.withMapper(gasStationMapDriver.getMapper());
        multipleInputsMapReduceDriver.withMapper(changePriceMapDriver.getMapper());
        multipleInputsMapReduceDriver.withReducer(reduceDriver.getReducer());

        multipleInputsMapReduceDriver.addInput((Mapper) gasStationMapDriver.getMapper(),
                new LongWritable(), new Text(
                        "secd94c07-aee2-41b8-a33f-0c21417be469\t360\t2016-04-30 11:31:56\tAllersberg Rother Str. 18\tOMV\tRother Str.\t18\t90584\tAllersberg\tdeBY\t49.2509000000000015\t11.2281499999999994"));
        multipleInputsMapReduceDriver.addInput((Mapper) changePriceMapDriver.getMapper(),
                new LongWritable(), new Text("52325918\tsecd94c07-aee2-41b8-a33f-0c21417be469\t1339\t1319\t1129\t2015-09-10 16:02:01+02\t21")
        );
        multipleInputsMapReduceDriver.addOutput(new Text("49.25090000 11.2281499"), new Text("113:1 "));
        multipleInputsMapReduceDriver.runTest();
    }
}
