package matmul;

import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mrunit.TestDriver;
import org.apache.hadoop.mrunit.mapreduce.MapDriver;
import org.apache.hadoop.mrunit.mapreduce.MultipleInputsMapReduceDriver;
import org.apache.hadoop.mrunit.mapreduce.ReduceDriver;
import org.junit.Before;
import org.junit.Test;
import types.IntDoubleWritable;
import types.IntPairWritable;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

public class MatrixMultiplicationFirstVersionTest {


    MapDriver<LongWritable, Text, IntPairWritable, IntDoubleWritable> leftMatrixMapperDriver;
    MapDriver<LongWritable, Text, IntPairWritable, IntDoubleWritable> rightMatrixMapperDriver;
    ReduceDriver<IntPairWritable, IntDoubleWritable, IntPairWritable, DoubleWritable> matrixReducerDriver;
    //ReduceDriver<IntPairWritable, IntDoubleWritable, IntPairWritable, IntDoubleWritable> matrixCombinerDriver;
    MultipleInputsMapReduceDriver<IntPairWritable, IntDoubleWritable, IntPairWritable, DoubleWritable> multipleInputsMapReduceDriver;

    ReduceDriver<IntPairWritable, IntDoubleWritable, IntPairWritable, DoubleWritable> matrixReducerDriverZero;
    MultipleInputsMapReduceDriver<IntPairWritable, IntDoubleWritable, IntPairWritable, DoubleWritable> multipleInputsMapReduceDriverWithZM;
    MultipleInputsMapReduceDriver<IntPairWritable, IntDoubleWritable, IntPairWritable, DoubleWritable> multipleInputsMapReduceDriverIndex;

    MatrixMultiplication.MapperLeftMatrix mapperLeftMatrix ;



    @Before
    public void setUp() {
      MatrixMultiplication.MapperLeftMatrix mapperLeftMatrix = new MatrixMultiplication.MapperLeftMatrix();
        MatrixMultiplication.MapperRightMatrix mapperRightMatrix = new MatrixMultiplication.MapperRightMatrix();
        MatrixMultiplication.MatrixReducer matrixReducer = new MatrixMultiplication.MatrixReducer();
        //MatrixMultiplication.MatrixCombiner matrixCombiner = new MatrixMultiplication.MatrixCombiner();
        leftMatrixMapperDriver = MapDriver.newMapDriver(mapperLeftMatrix);
        rightMatrixMapperDriver = MapDriver.newMapDriver(mapperRightMatrix);
        matrixReducerDriver = ReduceDriver.newReduceDriver(matrixReducer);
        //matrixCombinerDriver = ReduceDriver.newReduceDriver(matrixCombiner);
        multipleInputsMapReduceDriver = MultipleInputsMapReduceDriver.newMultipleInputMapReduceDriver(matrixReducer);

        MatrixMultiplication.MatrixReducer matrixReducerZero = new MatrixMultiplication.MatrixReducer();
        matrixReducerDriverZero = ReduceDriver.newReduceDriver(matrixReducerZero);

        multipleInputsMapReduceDriverWithZM = MultipleInputsMapReduceDriver.newMultipleInputMapReduceDriver(matrixReducer);


        multipleInputsMapReduceDriverIndex = MultipleInputsMapReduceDriver.newMultipleInputMapReduceDriver(matrixReducer);

        mapperLeftMatrix.setColumnNumberLeftMatrix(2);
        mapperLeftMatrix.setRowNumberRightMatrix(2);
        mapperLeftMatrix.setColumnNumberRightMatrix(2);
        mapperLeftMatrix.setTest(true);

        mapperRightMatrix.setColumnNumberLeftMatrix(2);
        mapperRightMatrix.setRowNumberRightMatrix(2);
        mapperRightMatrix.setRowNumberLeftMatrix(3);
        mapperRightMatrix.setTest(true);


    }


    @Test
    public void testLeftMatrixMapper() throws IOException {

        leftMatrixMapperDriver.withInput(new LongWritable(), new Text("0\t0\t0.212911"));
        leftMatrixMapperDriver.withInput(new LongWritable(), new Text("0\t1\t0.137713"));
        leftMatrixMapperDriver.withInput(new LongWritable(), new Text("0\t2\t-0.404688"));

        leftMatrixMapperDriver.withInput(new LongWritable(), new Text("1\t0\t0.197046"));
        leftMatrixMapperDriver.withInput(new LongWritable(), new Text("1\t1\t-0.729040"));
        leftMatrixMapperDriver.withInput(new LongWritable(), new Text("1\t2\t-0.791478"));

        leftMatrixMapperDriver.withInput(new LongWritable(), new Text("2\t0\t0.415584"));
        leftMatrixMapperDriver.withInput(new LongWritable(), new Text("2\t1\t2.043561"));
        leftMatrixMapperDriver.withInput(new LongWritable(), new Text("2\t2\t-1.405407"));

        leftMatrixMapperDriver.withOutput(new IntPairWritable(0, 0), new IntDoubleWritable(0, Double.parseDouble("0.212911")));
        leftMatrixMapperDriver.withOutput(new IntPairWritable(0, 1), new IntDoubleWritable(0, Double.parseDouble("0.212911")));
        leftMatrixMapperDriver.withOutput(new IntPairWritable(0, 0), new IntDoubleWritable(1, Double.parseDouble("0.137713")));
        leftMatrixMapperDriver.withOutput(new IntPairWritable(0, 1), new IntDoubleWritable(1, Double.parseDouble("0.137713")));
        leftMatrixMapperDriver.withOutput(new IntPairWritable(0, 0), new IntDoubleWritable(2, Double.parseDouble("-0.404688")));
        leftMatrixMapperDriver.withOutput(new IntPairWritable(0, 1), new IntDoubleWritable(2, Double.parseDouble("-0.404688")));

        leftMatrixMapperDriver.withOutput(new IntPairWritable(1, 0), new IntDoubleWritable(0, Double.parseDouble("0.197046")));
        leftMatrixMapperDriver.withOutput(new IntPairWritable(1, 1), new IntDoubleWritable(0, Double.parseDouble("0.197046")));
        leftMatrixMapperDriver.withOutput(new IntPairWritable(1, 0), new IntDoubleWritable(1, Double.parseDouble("-0.729040")));
        leftMatrixMapperDriver.withOutput(new IntPairWritable(1, 1), new IntDoubleWritable(1, Double.parseDouble("-0.729040")));
        leftMatrixMapperDriver.withOutput(new IntPairWritable(1, 0), new IntDoubleWritable(2, Double.parseDouble("-0.791478")));
        leftMatrixMapperDriver.withOutput(new IntPairWritable(1, 1), new IntDoubleWritable(2, Double.parseDouble("-0.791478")));

        leftMatrixMapperDriver.withOutput(new IntPairWritable(2, 0), new IntDoubleWritable(0, Double.parseDouble("0.415584")));
        leftMatrixMapperDriver.withOutput(new IntPairWritable(2, 1), new IntDoubleWritable(0, Double.parseDouble("0.415584")));
        leftMatrixMapperDriver.withOutput(new IntPairWritable(2, 0), new IntDoubleWritable(1, Double.parseDouble("2.043561")));
        leftMatrixMapperDriver.withOutput(new IntPairWritable(2, 1), new IntDoubleWritable(1, Double.parseDouble("2.043561")));
        leftMatrixMapperDriver.withOutput(new IntPairWritable(2, 0), new IntDoubleWritable(2, Double.parseDouble("-1.405407")));
        leftMatrixMapperDriver.withOutput(new IntPairWritable(2, 1), new IntDoubleWritable(2, Double.parseDouble("-1.405407")));

        leftMatrixMapperDriver.runTest();
    }

    @Test
    public void testRightMatrixMapper() throws IOException {
        rightMatrixMapperDriver.withInput(new LongWritable(), new Text("0\t0\t-3.986713311121"));
        rightMatrixMapperDriver.withInput(new LongWritable(), new Text("1\t0\t-0.00446092420399902"));
        rightMatrixMapperDriver.withInput(new LongWritable(), new Text("2\t0\t-1.498569127394"));

        rightMatrixMapperDriver.withInput(new LongWritable(), new Text("0\t1\t4.175140696934"));
        rightMatrixMapperDriver.withInput(new LongWritable(), new Text("1\t1\t2.157229789175"));
        rightMatrixMapperDriver.withInput(new LongWritable(), new Text("2\t1\t-11.571336176403"));

        rightMatrixMapperDriver.withOutput(new IntPairWritable(0, 0), new IntDoubleWritable(0, Double.parseDouble("-3.986713311121")));
        rightMatrixMapperDriver.withOutput(new IntPairWritable(1, 0), new IntDoubleWritable(0, Double.parseDouble("-3.986713311121")));
        rightMatrixMapperDriver.withOutput(new IntPairWritable(2, 0), new IntDoubleWritable(0, Double.parseDouble("-3.986713311121")));

        rightMatrixMapperDriver.withOutput(new IntPairWritable(0, 0), new IntDoubleWritable(1, Double.parseDouble("-0.00446092420399902")));
        rightMatrixMapperDriver.withOutput(new IntPairWritable(1, 0), new IntDoubleWritable(1, Double.parseDouble("-0.00446092420399902")));
        rightMatrixMapperDriver.withOutput(new IntPairWritable(2, 0), new IntDoubleWritable(1, Double.parseDouble("-0.00446092420399902")));

        rightMatrixMapperDriver.withOutput(new IntPairWritable(0, 0), new IntDoubleWritable(2, Double.parseDouble("-1.498569127394")));
        rightMatrixMapperDriver.withOutput(new IntPairWritable(1, 0), new IntDoubleWritable(2, Double.parseDouble("-1.498569127394")));
        rightMatrixMapperDriver.withOutput(new IntPairWritable(2, 0), new IntDoubleWritable(2, Double.parseDouble("-1.498569127394")));

        rightMatrixMapperDriver.withOutput(new IntPairWritable(0, 1), new IntDoubleWritable(0, Double.parseDouble("4.175140696934")));
        rightMatrixMapperDriver.withOutput(new IntPairWritable(1, 1), new IntDoubleWritable(0, Double.parseDouble("4.175140696934")));
        rightMatrixMapperDriver.withOutput(new IntPairWritable(2, 1), new IntDoubleWritable(0, Double.parseDouble("4.175140696934")));

        rightMatrixMapperDriver.withOutput(new IntPairWritable(0, 1), new IntDoubleWritable(1, Double.parseDouble("2.157229789175")));
        rightMatrixMapperDriver.withOutput(new IntPairWritable(1, 1), new IntDoubleWritable(1, Double.parseDouble("2.157229789175")));
        rightMatrixMapperDriver.withOutput(new IntPairWritable(2, 1), new IntDoubleWritable(1, Double.parseDouble("2.157229789175")));

        rightMatrixMapperDriver.withOutput(new IntPairWritable(0, 1), new IntDoubleWritable(2, Double.parseDouble("-11.571336176403")));
        rightMatrixMapperDriver.withOutput(new IntPairWritable(1, 1), new IntDoubleWritable(2, Double.parseDouble("-11.571336176403")));
        rightMatrixMapperDriver.withOutput(new IntPairWritable(2, 1), new IntDoubleWritable(2, Double.parseDouble("-11.571336176403")));

        rightMatrixMapperDriver.runTest();
    }
//    @Test
//    public void testCombiner() throws IOException{
//        double firstleft = Double.parseDouble("0.0");
//        double secondleft = Double.parseDouble("1.0");
//        double thirdleft = Double.parseDouble("2.0");
////        double fourthleft = Double.parseDouble("3.0");
////        double fifthleft = Double.parseDouble("4.0");
////        double sixthleft = Double.parseDouble("5.0");
//        double seventhleft = Double.parseDouble("6.0");
//        double eighthleft = Double.parseDouble("7.0");
//        double ninthleft = Double.parseDouble("8.0");
//
//        double firstright = Double.parseDouble("0.1");
//        double secondright = Double.parseDouble("0.2");
//        double thirdright = Double.parseDouble("0.3");
//        double fourthright = Double.parseDouble("0.4");
//        double fifthright = Double.parseDouble("0.5");
//        double sixthright = Double.parseDouble("0.6");
//
//
//        List list00 = Arrays.asList(new IntDoubleWritable((0), firstleft), new IntDoubleWritable(1, secondleft),
//                new IntDoubleWritable(2, thirdleft), new IntDoubleWritable(0, firstright), new IntDoubleWritable(1, thirdright),
//                new IntDoubleWritable(2, fifthright));
//
//        List list01 = Arrays.asList(new IntDoubleWritable((0), firstleft), new IntDoubleWritable(1, secondleft),
//                new IntDoubleWritable(2, thirdleft), new IntDoubleWritable(0, secondright), new IntDoubleWritable(1, fourthright),
//                new IntDoubleWritable(2, sixthright));
//
//
//        List list21 = Arrays.asList(new IntDoubleWritable((0), seventhleft), new IntDoubleWritable(1, eighthleft),
//                new IntDoubleWritable(2, ninthleft), new IntDoubleWritable(0, secondright), new IntDoubleWritable(1, fourthright),
//                new IntDoubleWritable(2, sixthright));
//
//        matrixCombinerDriver.withInput(new IntPairWritable(0, 0), list00);
//        matrixCombinerDriver.withInput(new IntPairWritable(0, 1), list01);
//        matrixCombinerDriver.withInput(new IntPairWritable(2, 1), list21);
//
//        matrixCombinerDriver.withOutput(new IntPairWritable(0,0), new IntDoubleWritable(0, 0.0));
//        matrixCombinerDriver.withOutput(new IntPairWritable(0,0), new IntDoubleWritable(1, 0.3));
//        matrixCombinerDriver.withOutput(new IntPairWritable(0,0), new IntDoubleWritable(2, 1.0));
//
//        matrixCombinerDriver.withOutput(new IntPairWritable(0,1), new IntDoubleWritable(0, 0.0));
//        matrixCombinerDriver.withOutput(new IntPairWritable(0,1), new IntDoubleWritable(1, 0.4));
//        matrixCombinerDriver.withOutput(new IntPairWritable(0,1), new IntDoubleWritable(2, 1.2));
//
//        matrixCombinerDriver.withOutput(new IntPairWritable(2,1), new IntDoubleWritable(0, 0.6));
//        matrixCombinerDriver.withOutput(new IntPairWritable(2,1), new IntDoubleWritable(1, 1.6));
//        matrixCombinerDriver.withOutput(new IntPairWritable(2,1), new IntDoubleWritable(2, 3.0));
//
//
//
//    }

/*
0 1 2
3 4 5
6 7 8

0.1 0.2
0.3 0.4
0.5 0.6
 */

    @Test
    public void testMatrixReduceDriver() throws IOException {
        double firstleft = Double.parseDouble("0.0");
        double secondleft = Double.parseDouble("1.0");
        double thirdleft = Double.parseDouble("2.0");
        double fourthleft = Double.parseDouble("3.0");
        double fifthleft = Double.parseDouble("4.0");
        double sixthleft = Double.parseDouble("5.0");
        double seventhleft = Double.parseDouble("6.0");
        double eighthleft = Double.parseDouble("7.0");
        double ninthleft = Double.parseDouble("8.0");

        double firstright = Double.parseDouble("0.1");
        double secondright = Double.parseDouble("0.2");
        double thirdright = Double.parseDouble("0.3");
        double fourthright = Double.parseDouble("0.4");
        double fifthright = Double.parseDouble("0.5");
        double sixthright = Double.parseDouble("0.6");


        List list00 = Arrays.asList(new IntDoubleWritable((0), firstleft), new IntDoubleWritable(1, secondleft),
                new IntDoubleWritable(2, thirdleft), new IntDoubleWritable(0, firstright), new IntDoubleWritable(1, thirdright),
                new IntDoubleWritable(2, fifthright));

        List list01 = Arrays.asList(new IntDoubleWritable((0), firstleft), new IntDoubleWritable(1, secondleft),
                new IntDoubleWritable(2, thirdleft), new IntDoubleWritable(0, secondright), new IntDoubleWritable(1, fourthright),
                new IntDoubleWritable(2, sixthright));

        List list10 = Arrays.asList(new IntDoubleWritable((0), fourthleft), new IntDoubleWritable(1, fifthleft),
                new IntDoubleWritable(2, sixthleft), new IntDoubleWritable(0, firstright), new IntDoubleWritable(1, thirdright),
                new IntDoubleWritable(2, fifthright));

        List list11 = Arrays.asList(new IntDoubleWritable((0), fourthleft), new IntDoubleWritable(1, fifthleft),
                new IntDoubleWritable(2, sixthleft), new IntDoubleWritable(0, secondright), new IntDoubleWritable(1, fourthright),
                new IntDoubleWritable(2, sixthright));

        List list20 = Arrays.asList(new IntDoubleWritable((0), seventhleft), new IntDoubleWritable(1, eighthleft),
                new IntDoubleWritable(2, ninthleft), new IntDoubleWritable(0, firstright), new IntDoubleWritable(1, thirdright),
                new IntDoubleWritable(2, fifthright));

        List list21 = Arrays.asList(new IntDoubleWritable((0), seventhleft), new IntDoubleWritable(1, eighthleft),
                new IntDoubleWritable(2, ninthleft), new IntDoubleWritable(0, secondright), new IntDoubleWritable(1, fourthright),
                new IntDoubleWritable(2, sixthright));


        matrixReducerDriver.withInput(new IntPairWritable(0, 0), list00);
        matrixReducerDriver.withInput(new IntPairWritable(0, 1), list01);
        matrixReducerDriver.withInput(new IntPairWritable(1, 0), list10);
        matrixReducerDriver.withInput(new IntPairWritable(1, 1), list11);
        matrixReducerDriver.withInput(new IntPairWritable(2, 0), list20);
        matrixReducerDriver.withInput(new IntPairWritable(2, 1), list21);


        matrixReducerDriver.withOutput(new IntPairWritable(0, 0), new DoubleWritable(1.3));
        matrixReducerDriver.withOutput(new IntPairWritable(0, 1), new DoubleWritable(1.6));
        matrixReducerDriver.withOutput(new IntPairWritable(1, 0), new DoubleWritable(4));
        matrixReducerDriver.withOutput(new IntPairWritable(1, 1), new DoubleWritable(5.2));
        matrixReducerDriver.withOutput(new IntPairWritable(2, 0), new DoubleWritable(6.7));
        matrixReducerDriver.withOutput(new IntPairWritable(2, 1), new DoubleWritable(8.8));

        matrixReducerDriver.runTest();
    }

    /*
0 1 2
3 4 5
0 0 0

0.0 0.2
0.3 0.4
0.5 0.0
 */

    @Test
    public void testMatrixWithZeroReduceDriver() throws IOException {
        double firstleft = Double.parseDouble("0.0");
        double secondleft = Double.parseDouble("1.0");
        double thirdleft = Double.parseDouble("2.0");
        double fourthleft = Double.parseDouble("3.0");
        double fifthleft = Double.parseDouble("4.0");
        double sixthleft = Double.parseDouble("5.0");
        double seventhleft = Double.parseDouble("0.0");
        double eighthleft = Double.parseDouble("0.0");
        double ninthleft = Double.parseDouble("0.0");

        double firstright = Double.parseDouble("0.0");
        double secondright = Double.parseDouble("0.2");
        double thirdright = Double.parseDouble("0.3");
        double fourthright = Double.parseDouble("0.4");
        double fifthright = Double.parseDouble("0.5");
        double sixthright = Double.parseDouble("0.0");


        List list00 = Arrays.asList(new IntDoubleWritable((0), firstleft), new IntDoubleWritable(1, secondleft),
                new IntDoubleWritable(2, thirdleft), new IntDoubleWritable(0, firstright), new IntDoubleWritable(1, thirdright),
                new IntDoubleWritable(2, fifthright));

        List list01 = Arrays.asList(new IntDoubleWritable((0), firstleft), new IntDoubleWritable(1, secondleft),
                new IntDoubleWritable(2, thirdleft), new IntDoubleWritable(0, secondright), new IntDoubleWritable(1, fourthright),
                new IntDoubleWritable(2, sixthright));

        List list10 = Arrays.asList(new IntDoubleWritable((0), fourthleft), new IntDoubleWritable(1, fifthleft),
                new IntDoubleWritable(2, sixthleft), new IntDoubleWritable(0, firstright), new IntDoubleWritable(1, thirdright),
                new IntDoubleWritable(2, fifthright));

        List list11 = Arrays.asList(new IntDoubleWritable((0), fourthleft), new IntDoubleWritable(1, fifthleft),
                new IntDoubleWritable(2, sixthleft), new IntDoubleWritable(0, secondright), new IntDoubleWritable(1, fourthright),
                new IntDoubleWritable(2, sixthright));

        List list20 = Arrays.asList(new IntDoubleWritable((0), seventhleft), new IntDoubleWritable(1, eighthleft),
                new IntDoubleWritable(2, ninthleft), new IntDoubleWritable(0, firstright), new IntDoubleWritable(1, thirdright),
                new IntDoubleWritable(2, fifthright));

        List list21 = Arrays.asList(new IntDoubleWritable((0), seventhleft), new IntDoubleWritable(1, eighthleft),
                new IntDoubleWritable(2, ninthleft), new IntDoubleWritable(0, secondright), new IntDoubleWritable(1, fourthright),
                new IntDoubleWritable(2, sixthright));


        matrixReducerDriverZero.withInput(new IntPairWritable(0, 0), list00);
        matrixReducerDriverZero.withInput(new IntPairWritable(0, 1), list01);
        matrixReducerDriverZero.withInput(new IntPairWritable(1, 0), list10);
        matrixReducerDriverZero.withInput(new IntPairWritable(1, 1), list11);
        matrixReducerDriverZero.withInput(new IntPairWritable(2, 0), list20);
        matrixReducerDriverZero.withInput(new IntPairWritable(2, 1), list21);


        matrixReducerDriverZero.withOutput(new IntPairWritable(0, 0), new DoubleWritable(1.3));
        matrixReducerDriverZero.withOutput(new IntPairWritable(0, 1), new DoubleWritable(0.4));
        matrixReducerDriverZero.withOutput(new IntPairWritable(1, 0), new DoubleWritable(3.7));
        matrixReducerDriverZero.withOutput(new IntPairWritable(1, 1), new DoubleWritable(2.2));
        matrixReducerDriverZero.withOutput(new IntPairWritable(2, 0), new DoubleWritable(0.0));
        matrixReducerDriverZero.withOutput(new IntPairWritable(2, 1), new DoubleWritable(0.0));

        matrixReducerDriverZero.runTest();
    }
    /*
0 1 2 4
3 4 5 -2.5
-3.5 0.5 -4.25 -3

0.0 0.2
0.3 -0.4
-0.5 0.0
-0.1 5
 */
    @Test
    public void matrixMapperReducerWithZM() throws IOException {

        multipleInputsMapReduceDriverWithZM.withMapper(leftMatrixMapperDriver.getMapper());
        multipleInputsMapReduceDriverWithZM.withMapper(rightMatrixMapperDriver.getMapper());
        multipleInputsMapReduceDriverWithZM.withReducer(matrixReducerDriver.getReducer());

        /** Input 3x4 Matrix left**/
        multipleInputsMapReduceDriverWithZM.addInput(leftMatrixMapperDriver.getMapper(), new LongWritable(), new Text("0\t0\t0"));
        multipleInputsMapReduceDriverWithZM.addInput(leftMatrixMapperDriver.getMapper(), new LongWritable(), new Text("0\t1\t1"));
        multipleInputsMapReduceDriverWithZM.addInput(leftMatrixMapperDriver.getMapper(), new LongWritable(), new Text("0\t2\t2"));
        multipleInputsMapReduceDriverWithZM.addInput(leftMatrixMapperDriver.getMapper(), new LongWritable(), new Text("0\t3\t4"));

        multipleInputsMapReduceDriverWithZM.addInput(leftMatrixMapperDriver.getMapper(), new LongWritable(), new Text("1\t0\t3"));
        multipleInputsMapReduceDriverWithZM.addInput(leftMatrixMapperDriver.getMapper(), new LongWritable(), new Text("1\t1\t4"));
        multipleInputsMapReduceDriverWithZM.addInput(leftMatrixMapperDriver.getMapper(), new LongWritable(), new Text("1\t2\t5"));
        multipleInputsMapReduceDriverWithZM.addInput(leftMatrixMapperDriver.getMapper(), new LongWritable(), new Text("1\t3\t-2.5"));

        multipleInputsMapReduceDriverWithZM.addInput(leftMatrixMapperDriver.getMapper(), new LongWritable(), new Text("2\t0\t-3.5"));
        multipleInputsMapReduceDriverWithZM.addInput(leftMatrixMapperDriver.getMapper(), new LongWritable(), new Text("2\t1\t0.5"));
        multipleInputsMapReduceDriverWithZM.addInput(leftMatrixMapperDriver.getMapper(), new LongWritable(), new Text("2\t2\t-4.25"));
        multipleInputsMapReduceDriverWithZM.addInput(leftMatrixMapperDriver.getMapper(), new LongWritable(), new Text("2\t3\t-3"));

        /** Input 4x2 Matrix right **/
        multipleInputsMapReduceDriverWithZM.addInput(rightMatrixMapperDriver.getMapper(), new LongWritable(), new Text("0\t0\t0"));
        multipleInputsMapReduceDriverWithZM.addInput(rightMatrixMapperDriver.getMapper(), new LongWritable(), new Text("0\t1\t0.2"));

        multipleInputsMapReduceDriverWithZM.addInput(rightMatrixMapperDriver.getMapper(), new LongWritable(), new Text("1\t0\t0.3"));
        multipleInputsMapReduceDriverWithZM.addInput(rightMatrixMapperDriver.getMapper(), new LongWritable(), new Text("1\t1\t-0.4"));

        multipleInputsMapReduceDriverWithZM.addInput(rightMatrixMapperDriver.getMapper(), new LongWritable(), new Text("2\t0\t-0.5"));
        multipleInputsMapReduceDriverWithZM.addInput(rightMatrixMapperDriver.getMapper(), new LongWritable(), new Text("2\t1\t0.0"));

        multipleInputsMapReduceDriverWithZM.addInput(rightMatrixMapperDriver.getMapper(), new LongWritable(), new Text("3\t0\t-0.1"));
        multipleInputsMapReduceDriverWithZM.addInput(rightMatrixMapperDriver.getMapper(), new LongWritable(), new Text("3\t1\t5"));

        //3x2
        multipleInputsMapReduceDriverWithZM.addOutput(new IntPairWritable(0,0), new DoubleWritable(-1.1));
        multipleInputsMapReduceDriverWithZM.addOutput(new IntPairWritable(0,1), new DoubleWritable(19.6));

        multipleInputsMapReduceDriverWithZM.addOutput(new IntPairWritable(1,0), new DoubleWritable(-1.05));
        multipleInputsMapReduceDriverWithZM.addOutput(new IntPairWritable(1,1), new DoubleWritable(-13.5));

        multipleInputsMapReduceDriverWithZM.addOutput(new IntPairWritable(2,0), new DoubleWritable(2.575));
        multipleInputsMapReduceDriverWithZM.addOutput(new IntPairWritable(2,1), new DoubleWritable(-15.9));

        multipleInputsMapReduceDriverWithZM.runTest();
    }

    /*
0 1 2 4
3 4 5 -2.5
-3.5 0.5 -4.25 -3

0.0 0.2
0.3 -0.4
-0.5 0.0
-0.1 5
*/
    @Test
    public void matrixMapperReducerIndex() throws IOException {
        multipleInputsMapReduceDriverIndex.withMapper(leftMatrixMapperDriver.getMapper());
        multipleInputsMapReduceDriverIndex.withMapper(rightMatrixMapperDriver.getMapper());
        multipleInputsMapReduceDriverIndex.withReducer(matrixReducerDriver.getReducer());

        /** Input 3x4 Matrix left**/

        multipleInputsMapReduceDriverIndex.addInput(leftMatrixMapperDriver.getMapper(), new LongWritable(), new Text("0\t1\t1"));
        multipleInputsMapReduceDriverIndex.addInput(leftMatrixMapperDriver.getMapper(), new LongWritable(), new Text("0\t0\t0"));
        multipleInputsMapReduceDriverIndex.addInput(leftMatrixMapperDriver.getMapper(), new LongWritable(), new Text("0\t2\t2"));
        multipleInputsMapReduceDriverIndex.addInput(leftMatrixMapperDriver.getMapper(), new LongWritable(), new Text("0\t3\t4"));

        multipleInputsMapReduceDriverIndex.addInput(leftMatrixMapperDriver.getMapper(), new LongWritable(), new Text("2\t2\t-4.25"));

        multipleInputsMapReduceDriverIndex.addInput(leftMatrixMapperDriver.getMapper(), new LongWritable(), new Text("1\t0\t3"));
        multipleInputsMapReduceDriverIndex.addInput(leftMatrixMapperDriver.getMapper(), new LongWritable(), new Text("1\t1\t4"));
        multipleInputsMapReduceDriverIndex.addInput(leftMatrixMapperDriver.getMapper(), new LongWritable(), new Text("1\t3\t-2.5"));
        multipleInputsMapReduceDriverIndex.addInput(leftMatrixMapperDriver.getMapper(), new LongWritable(), new Text("1\t2\t5"));

        multipleInputsMapReduceDriverIndex.addInput(leftMatrixMapperDriver.getMapper(), new LongWritable(), new Text("2\t0\t-3.5"));
        multipleInputsMapReduceDriverIndex.addInput(leftMatrixMapperDriver.getMapper(), new LongWritable(), new Text("2\t1\t0.5"));

        multipleInputsMapReduceDriverIndex.addInput(leftMatrixMapperDriver.getMapper(), new LongWritable(), new Text("2\t3\t-3"));

        /** Input 4x2 Matrix right **/
        multipleInputsMapReduceDriverIndex.addInput(rightMatrixMapperDriver.getMapper(), new LongWritable(), new Text("0\t0\t0"));
        multipleInputsMapReduceDriverIndex.addInput(rightMatrixMapperDriver.getMapper(), new LongWritable(), new Text("0\t1\t0.2"));
        multipleInputsMapReduceDriverIndex.addInput(rightMatrixMapperDriver.getMapper(), new LongWritable(), new Text("3\t0\t-0.1"));
        multipleInputsMapReduceDriverIndex.addInput(rightMatrixMapperDriver.getMapper(), new LongWritable(), new Text("1\t0\t0.3"));
        multipleInputsMapReduceDriverIndex.addInput(rightMatrixMapperDriver.getMapper(), new LongWritable(), new Text("1\t1\t-0.4"));
        multipleInputsMapReduceDriverIndex.addInput(rightMatrixMapperDriver.getMapper(), new LongWritable(), new Text("2\t1\t0.0"));
        multipleInputsMapReduceDriverIndex.addInput(rightMatrixMapperDriver.getMapper(), new LongWritable(), new Text("3\t1\t5"));
        multipleInputsMapReduceDriverIndex.addInput(rightMatrixMapperDriver.getMapper(), new LongWritable(), new Text("2\t0\t-0.5"));
        //3x2
        multipleInputsMapReduceDriverIndex.addOutput(new IntPairWritable(0,0), new DoubleWritable(-1.1));
        multipleInputsMapReduceDriverIndex.addOutput(new IntPairWritable(0,1), new DoubleWritable(19.6));

        multipleInputsMapReduceDriverIndex.addOutput(new IntPairWritable(1,0), new DoubleWritable(-1.05));
        multipleInputsMapReduceDriverIndex.addOutput(new IntPairWritable(1,1), new DoubleWritable(-13.5));

        multipleInputsMapReduceDriverIndex.addOutput(new IntPairWritable(2,0), new DoubleWritable(2.575));
        multipleInputsMapReduceDriverIndex.addOutput(new IntPairWritable(2,1), new DoubleWritable(-15.9));

        multipleInputsMapReduceDriverIndex.runTest();
    }


    @Test
    public void matrixMapperReducer() throws IOException{
        multipleInputsMapReduceDriver.withMapper(leftMatrixMapperDriver.getMapper());
        multipleInputsMapReduceDriver.withMapper(rightMatrixMapperDriver.getMapper());
        multipleInputsMapReduceDriver.withReducer(matrixReducerDriver.getReducer());

        /** Input 6x9 Matrix left**/
        multipleInputsMapReduceDriver.addInput(leftMatrixMapperDriver.getMapper(), new LongWritable(), new Text("0\t0\t10000"));
        multipleInputsMapReduceDriver.addInput(leftMatrixMapperDriver.getMapper(), new LongWritable(), new Text("0\t1\t5"));
        multipleInputsMapReduceDriver.addInput(leftMatrixMapperDriver.getMapper(), new LongWritable(), new Text("0\t2\t4"));
        multipleInputsMapReduceDriver.addInput(leftMatrixMapperDriver.getMapper(), new LongWritable(), new Text("0\t3\t3"));
        multipleInputsMapReduceDriver.addInput(leftMatrixMapperDriver.getMapper(), new LongWritable(), new Text("0\t4\t500"));
        multipleInputsMapReduceDriver.addInput(leftMatrixMapperDriver.getMapper(), new LongWritable(), new Text("0\t5\t200"));
        multipleInputsMapReduceDriver.addInput(leftMatrixMapperDriver.getMapper(), new LongWritable(), new Text("0\t6\t8"));
        multipleInputsMapReduceDriver.addInput(leftMatrixMapperDriver.getMapper(), new LongWritable(), new Text("0\t7\t0.5"));
        multipleInputsMapReduceDriver.addInput(leftMatrixMapperDriver.getMapper(), new LongWritable(), new Text("0\t8\t0.2"));

        multipleInputsMapReduceDriver.addInput(leftMatrixMapperDriver.getMapper(), new LongWritable(), new Text("1\t0\t81"));
        multipleInputsMapReduceDriver.addInput(leftMatrixMapperDriver.getMapper(), new LongWritable(), new Text("1\t1\t63"));
        multipleInputsMapReduceDriver.addInput(leftMatrixMapperDriver.getMapper(), new LongWritable(), new Text("1\t2\t45"));
        multipleInputsMapReduceDriver.addInput(leftMatrixMapperDriver.getMapper(), new LongWritable(), new Text("1\t3\t12"));
        multipleInputsMapReduceDriver.addInput(leftMatrixMapperDriver.getMapper(), new LongWritable(), new Text("1\t4\t50"));
        multipleInputsMapReduceDriver.addInput(leftMatrixMapperDriver.getMapper(), new LongWritable(), new Text("1\t5\t9"));
        multipleInputsMapReduceDriver.addInput(leftMatrixMapperDriver.getMapper(), new LongWritable(), new Text("1\t6\t6"));
        multipleInputsMapReduceDriver.addInput(leftMatrixMapperDriver.getMapper(), new LongWritable(), new Text("1\t7\t300"));
        multipleInputsMapReduceDriver.addInput(leftMatrixMapperDriver.getMapper(), new LongWritable(), new Text("1\t8\t2"));

        multipleInputsMapReduceDriver.addInput(leftMatrixMapperDriver.getMapper(), new LongWritable(), new Text("2\t0\t0"));
        multipleInputsMapReduceDriver.addInput(leftMatrixMapperDriver.getMapper(), new LongWritable(), new Text("2\t1\t0"));
        multipleInputsMapReduceDriver.addInput(leftMatrixMapperDriver.getMapper(), new LongWritable(), new Text("2\t2\t0"));
        multipleInputsMapReduceDriver.addInput(leftMatrixMapperDriver.getMapper(), new LongWritable(), new Text("2\t3\t0"));
        multipleInputsMapReduceDriver.addInput(leftMatrixMapperDriver.getMapper(), new LongWritable(), new Text("2\t4\t0"));
        multipleInputsMapReduceDriver.addInput(leftMatrixMapperDriver.getMapper(), new LongWritable(), new Text("2\t5\t0"));
        multipleInputsMapReduceDriver.addInput(leftMatrixMapperDriver.getMapper(), new LongWritable(), new Text("2\t6\t0"));
        multipleInputsMapReduceDriver.addInput(leftMatrixMapperDriver.getMapper(), new LongWritable(), new Text("2\t7\t0"));
        multipleInputsMapReduceDriver.addInput(leftMatrixMapperDriver.getMapper(), new LongWritable(), new Text("2\t8\t0"));

        multipleInputsMapReduceDriver.addInput(leftMatrixMapperDriver.getMapper(), new LongWritable(), new Text("3\t0\t-10000"));
        multipleInputsMapReduceDriver.addInput(leftMatrixMapperDriver.getMapper(), new LongWritable(), new Text("3\t1\t3.5"));
        multipleInputsMapReduceDriver.addInput(leftMatrixMapperDriver.getMapper(), new LongWritable(), new Text("3\t2\t-3.5"));
        multipleInputsMapReduceDriver.addInput(leftMatrixMapperDriver.getMapper(), new LongWritable(), new Text("3\t3\t0.0025"));
        multipleInputsMapReduceDriver.addInput(leftMatrixMapperDriver.getMapper(), new LongWritable(), new Text("3\t4\t5"));
        multipleInputsMapReduceDriver.addInput(leftMatrixMapperDriver.getMapper(), new LongWritable(), new Text("3\t5\t15"));
        multipleInputsMapReduceDriver.addInput(leftMatrixMapperDriver.getMapper(), new LongWritable(), new Text("3\t6\t8"));
        multipleInputsMapReduceDriver.addInput(leftMatrixMapperDriver.getMapper(), new LongWritable(), new Text("3\t7\t0.25"));
        multipleInputsMapReduceDriver.addInput(leftMatrixMapperDriver.getMapper(), new LongWritable(), new Text("3\t8\t1"));

        multipleInputsMapReduceDriver.addInput(leftMatrixMapperDriver.getMapper(), new LongWritable(), new Text("4\t0\t0"));
        multipleInputsMapReduceDriver.addInput(leftMatrixMapperDriver.getMapper(), new LongWritable(), new Text("4\t1\t18"));
        multipleInputsMapReduceDriver.addInput(leftMatrixMapperDriver.getMapper(), new LongWritable(), new Text("4\t2\t69"));
        multipleInputsMapReduceDriver.addInput(leftMatrixMapperDriver.getMapper(), new LongWritable(), new Text("4\t3\t89"));
        multipleInputsMapReduceDriver.addInput(leftMatrixMapperDriver.getMapper(), new LongWritable(), new Text("4\t4\t7"));
        multipleInputsMapReduceDriver.addInput(leftMatrixMapperDriver.getMapper(), new LongWritable(), new Text("4\t5\t4"));
        multipleInputsMapReduceDriver.addInput(leftMatrixMapperDriver.getMapper(), new LongWritable(), new Text("4\t6\t8"));
        multipleInputsMapReduceDriver.addInput(leftMatrixMapperDriver.getMapper(), new LongWritable(), new Text("4\t7\t2"));
        multipleInputsMapReduceDriver.addInput(leftMatrixMapperDriver.getMapper(), new LongWritable(), new Text("4\t8\t666"));

        multipleInputsMapReduceDriver.addInput(leftMatrixMapperDriver.getMapper(), new LongWritable(), new Text("5\t0\t72"));
        multipleInputsMapReduceDriver.addInput(leftMatrixMapperDriver.getMapper(), new LongWritable(), new Text("5\t1\t42"));
        multipleInputsMapReduceDriver.addInput(leftMatrixMapperDriver.getMapper(), new LongWritable(), new Text("5\t2\t71"));
        multipleInputsMapReduceDriver.addInput(leftMatrixMapperDriver.getMapper(), new LongWritable(), new Text("5\t3\t41"));
        multipleInputsMapReduceDriver.addInput(leftMatrixMapperDriver.getMapper(), new LongWritable(), new Text("5\t4\t60"));
        multipleInputsMapReduceDriver.addInput(leftMatrixMapperDriver.getMapper(), new LongWritable(), new Text("5\t5\t30"));
        multipleInputsMapReduceDriver.addInput(leftMatrixMapperDriver.getMapper(), new LongWritable(), new Text("5\t6\t22"));
        multipleInputsMapReduceDriver.addInput(leftMatrixMapperDriver.getMapper(), new LongWritable(), new Text("5\t7\t52"));
        multipleInputsMapReduceDriver.addInput(leftMatrixMapperDriver.getMapper(), new LongWritable(), new Text("5\t8\t51"));

        /** Input 9x7 Matrix right **/
        multipleInputsMapReduceDriver.addInput(rightMatrixMapperDriver.getMapper(), new LongWritable(), new Text("0\t0\t11000"));
        multipleInputsMapReduceDriver.addInput(rightMatrixMapperDriver.getMapper(), new LongWritable(), new Text("0\t1\t-1"));
        multipleInputsMapReduceDriver.addInput(rightMatrixMapperDriver.getMapper(), new LongWritable(), new Text("0\t2\t-2"));
        multipleInputsMapReduceDriver.addInput(rightMatrixMapperDriver.getMapper(), new LongWritable(), new Text("0\t3\t-3"));
        multipleInputsMapReduceDriver.addInput(rightMatrixMapperDriver.getMapper(), new LongWritable(), new Text("0\t4\t-4"));
        multipleInputsMapReduceDriver.addInput(rightMatrixMapperDriver.getMapper(), new LongWritable(), new Text("0\t5\t-5"));
        multipleInputsMapReduceDriver.addInput(rightMatrixMapperDriver.getMapper(), new LongWritable(), new Text("0\t6\t-6"));

        multipleInputsMapReduceDriver.addInput(rightMatrixMapperDriver.getMapper(), new LongWritable(), new Text("1\t0\t0.2"));
        multipleInputsMapReduceDriver.addInput(rightMatrixMapperDriver.getMapper(), new LongWritable(), new Text("1\t1\t0.3"));
        multipleInputsMapReduceDriver.addInput(rightMatrixMapperDriver.getMapper(), new LongWritable(), new Text("1\t2\t0"));
        multipleInputsMapReduceDriver.addInput(rightMatrixMapperDriver.getMapper(), new LongWritable(), new Text("1\t3\t0"));
        multipleInputsMapReduceDriver.addInput(rightMatrixMapperDriver.getMapper(), new LongWritable(), new Text("1\t4\t1"));
        multipleInputsMapReduceDriver.addInput(rightMatrixMapperDriver.getMapper(), new LongWritable(), new Text("1\t5\t2"));
        multipleInputsMapReduceDriver.addInput(rightMatrixMapperDriver.getMapper(), new LongWritable(), new Text("1\t6\t3"));

        multipleInputsMapReduceDriver.addInput(rightMatrixMapperDriver.getMapper(), new LongWritable(), new Text("2\t0\t0.1"));
        multipleInputsMapReduceDriver.addInput(rightMatrixMapperDriver.getMapper(), new LongWritable(), new Text("2\t1\t0.2"));
        multipleInputsMapReduceDriver.addInput(rightMatrixMapperDriver.getMapper(), new LongWritable(), new Text("2\t2\t2.5"));
        multipleInputsMapReduceDriver.addInput(rightMatrixMapperDriver.getMapper(), new LongWritable(), new Text("2\t3\t-10"));
        multipleInputsMapReduceDriver.addInput(rightMatrixMapperDriver.getMapper(), new LongWritable(), new Text("2\t4\t-25"));
        multipleInputsMapReduceDriver.addInput(rightMatrixMapperDriver.getMapper(), new LongWritable(), new Text("2\t5\t30"));
        multipleInputsMapReduceDriver.addInput(rightMatrixMapperDriver.getMapper(), new LongWritable(), new Text("2\t6\t0.005"));

        multipleInputsMapReduceDriver.addInput(rightMatrixMapperDriver.getMapper(), new LongWritable(), new Text("3\t0\t-2"));
        multipleInputsMapReduceDriver.addInput(rightMatrixMapperDriver.getMapper(), new LongWritable(), new Text("3\t1\t2.5"));
        multipleInputsMapReduceDriver.addInput(rightMatrixMapperDriver.getMapper(), new LongWritable(), new Text("3\t2\t0"));
        multipleInputsMapReduceDriver.addInput(rightMatrixMapperDriver.getMapper(), new LongWritable(), new Text("3\t3\t9"));
        multipleInputsMapReduceDriver.addInput(rightMatrixMapperDriver.getMapper(), new LongWritable(), new Text("3\t4\t11"));
        multipleInputsMapReduceDriver.addInput(rightMatrixMapperDriver.getMapper(), new LongWritable(), new Text("3\t5\t0.10005"));
        multipleInputsMapReduceDriver.addInput(rightMatrixMapperDriver.getMapper(), new LongWritable(), new Text("3\t6\t23"));

        multipleInputsMapReduceDriver.addInput(rightMatrixMapperDriver.getMapper(), new LongWritable(), new Text("4\t0\t0.5"));
        multipleInputsMapReduceDriver.addInput(rightMatrixMapperDriver.getMapper(), new LongWritable(), new Text("4\t1\t-0.5"));
        multipleInputsMapReduceDriver.addInput(rightMatrixMapperDriver.getMapper(), new LongWritable(), new Text("4\t2\t0"));
        multipleInputsMapReduceDriver.addInput(rightMatrixMapperDriver.getMapper(), new LongWritable(), new Text("4\t3\t0"));
        multipleInputsMapReduceDriver.addInput(rightMatrixMapperDriver.getMapper(), new LongWritable(), new Text("4\t4\t0"));
        multipleInputsMapReduceDriver.addInput(rightMatrixMapperDriver.getMapper(), new LongWritable(), new Text("4\t5\t-50"));
        multipleInputsMapReduceDriver.addInput(rightMatrixMapperDriver.getMapper(), new LongWritable(), new Text("4\t6\t-70"));

        multipleInputsMapReduceDriver.addInput(rightMatrixMapperDriver.getMapper(), new LongWritable(), new Text("5\t0\t0.1"));
        multipleInputsMapReduceDriver.addInput(rightMatrixMapperDriver.getMapper(), new LongWritable(), new Text("5\t1\t7"));
        multipleInputsMapReduceDriver.addInput(rightMatrixMapperDriver.getMapper(), new LongWritable(), new Text("5\t2\t-0.007"));
        multipleInputsMapReduceDriver.addInput(rightMatrixMapperDriver.getMapper(), new LongWritable(), new Text("5\t3\t0.089"));
        multipleInputsMapReduceDriver.addInput(rightMatrixMapperDriver.getMapper(), new LongWritable(), new Text("5\t4\t1"));
        multipleInputsMapReduceDriver.addInput(rightMatrixMapperDriver.getMapper(), new LongWritable(), new Text("5\t5\t0"));
        multipleInputsMapReduceDriver.addInput(rightMatrixMapperDriver.getMapper(), new LongWritable(), new Text("5\t6\t2"));

        multipleInputsMapReduceDriver.addInput(rightMatrixMapperDriver.getMapper(), new LongWritable(), new Text("6\t0\t0.00001"));
        multipleInputsMapReduceDriver.addInput(rightMatrixMapperDriver.getMapper(), new LongWritable(), new Text("6\t1\t8"));
        multipleInputsMapReduceDriver.addInput(rightMatrixMapperDriver.getMapper(), new LongWritable(), new Text("6\t2\t0.01"));
        multipleInputsMapReduceDriver.addInput(rightMatrixMapperDriver.getMapper(), new LongWritable(), new Text("6\t3\t0"));
        multipleInputsMapReduceDriver.addInput(rightMatrixMapperDriver.getMapper(), new LongWritable(), new Text("6\t4\t0"));
        multipleInputsMapReduceDriver.addInput(rightMatrixMapperDriver.getMapper(), new LongWritable(), new Text("6\t5\t0"));
        multipleInputsMapReduceDriver.addInput(rightMatrixMapperDriver.getMapper(), new LongWritable(), new Text("6\t6\t0"));

        multipleInputsMapReduceDriver.addInput(rightMatrixMapperDriver.getMapper(), new LongWritable(), new Text("7\t0\t0.00446092420399902"));
        multipleInputsMapReduceDriver.addInput(rightMatrixMapperDriver.getMapper(), new LongWritable(), new Text("7\t1\t0.01"));
        multipleInputsMapReduceDriver.addInput(rightMatrixMapperDriver.getMapper(), new LongWritable(), new Text("7\t2\t1"));
        multipleInputsMapReduceDriver.addInput(rightMatrixMapperDriver.getMapper(), new LongWritable(), new Text("7\t3\t1"));
        multipleInputsMapReduceDriver.addInput(rightMatrixMapperDriver.getMapper(), new LongWritable(), new Text("7\t4\t0"));
        multipleInputsMapReduceDriver.addInput(rightMatrixMapperDriver.getMapper(), new LongWritable(), new Text("7\t5\t0"));
        multipleInputsMapReduceDriver.addInput(rightMatrixMapperDriver.getMapper(), new LongWritable(), new Text("7\t6\t0"));

        multipleInputsMapReduceDriver.addInput(rightMatrixMapperDriver.getMapper(), new LongWritable(), new Text("8\t0\t0.000000000019"));
        multipleInputsMapReduceDriver.addInput(rightMatrixMapperDriver.getMapper(), new LongWritable(), new Text("8\t1\t6"));
        multipleInputsMapReduceDriver.addInput(rightMatrixMapperDriver.getMapper(), new LongWritable(), new Text("8\t2\t0"));
        multipleInputsMapReduceDriver.addInput(rightMatrixMapperDriver.getMapper(), new LongWritable(), new Text("8\t3\t1"));
        multipleInputsMapReduceDriver.addInput(rightMatrixMapperDriver.getMapper(), new LongWritable(), new Text("8\t4\t2"));
        multipleInputsMapReduceDriver.addInput(rightMatrixMapperDriver.getMapper(), new LongWritable(), new Text("8\t5\t3"));
        multipleInputsMapReduceDriver.addInput(rightMatrixMapperDriver.getMapper(), new LongWritable(), new Text("8\t6\t4"));


        /** Output 6x7 Matrix result **/
        multipleInputsMapReduceDriver.addOutput(new IntPairWritable(0,0), new DoubleWritable(1.1000026540231048E8));
        multipleInputsMapReduceDriver.addOutput(new IntPairWritable(0,1), new DoubleWritable(-8774.995));
        multipleInputsMapReduceDriver.addOutput(new IntPairWritable(0,2), new DoubleWritable(-19990.82)); //-19990.1
        multipleInputsMapReduceDriver.addOutput(new IntPairWritable(0,3), new DoubleWritable(-29994.5));
        multipleInputsMapReduceDriver.addOutput(new IntPairWritable(0,4), new DoubleWritable(-39861.6));
        multipleInputsMapReduceDriver.addOutput(new IntPairWritable(0,5), new DoubleWritable(-74869.09985)); //-74869.0985
        multipleInputsMapReduceDriver.addOutput(new IntPairWritable(0,6), new DoubleWritable(-94515.18000000001));

        multipleInputsMapReduceDriver.addOutput(new IntPairWritable(1,0), new DoubleWritable(891020.3383372612));
        multipleInputsMapReduceDriver.addOutput(new IntPairWritable(1,1), new DoubleWritable(77.9)); //74.605
        multipleInputsMapReduceDriver.addOutput(new IntPairWritable(1,2), new DoubleWritable(250.497)); //251.037
        multipleInputsMapReduceDriver.addOutput(new IntPairWritable(1,3), new DoubleWritable(-282.19899999999996));
        multipleInputsMapReduceDriver.addOutput(new IntPairWritable(1,4), new DoubleWritable(-1241));
        multipleInputsMapReduceDriver.addOutput(new IntPairWritable(1,5), new DoubleWritable(-1421.7994));
        multipleInputsMapReduceDriver.addOutput(new IntPairWritable(1,6), new DoubleWritable(-3494.775));

        multipleInputsMapReduceDriver.addOutput(new IntPairWritable(2,0), new DoubleWritable(0));
        multipleInputsMapReduceDriver.addOutput(new IntPairWritable(2,1), new DoubleWritable(0));
        multipleInputsMapReduceDriver.addOutput(new IntPairWritable(2,2), new DoubleWritable(0));
        multipleInputsMapReduceDriver.addOutput(new IntPairWritable(2,3), new DoubleWritable(0));
        multipleInputsMapReduceDriver.addOutput(new IntPairWritable(2,4), new DoubleWritable(0));
        multipleInputsMapReduceDriver.addOutput(new IntPairWritable(2,5), new DoubleWritable(0));
        multipleInputsMapReduceDriver.addOutput(new IntPairWritable(2,6), new DoubleWritable(0));

        multipleInputsMapReduceDriver.addOutput(new IntPairWritable(3,0), new DoubleWritable(-109999995.6538047499999999999));
        multipleInputsMapReduceDriver.addOutput(new IntPairWritable(3,1), new DoubleWritable(10172.85875));
        multipleInputsMapReduceDriver.addOutput(new IntPairWritable(3,2), new DoubleWritable(19991.475000000002));
        multipleInputsMapReduceDriver.addOutput(new IntPairWritable(3,3), new DoubleWritable(30037.6075));
        multipleInputsMapReduceDriver.addOutput(new IntPairWritable(3,4), new DoubleWritable(40108.0275));
        multipleInputsMapReduceDriver.addOutput(new IntPairWritable(3,5), new DoubleWritable(49655.000250125));
        multipleInputsMapReduceDriver.addOutput(new IntPairWritable(3,6), new DoubleWritable(59694.54));

        multipleInputsMapReduceDriver.addOutput(new IntPairWritable(4,0), new DoubleWritable(-163.590998138938));
        multipleInputsMapReduceDriver.addOutput(new IntPairWritable(4,1), new DoubleWritable(4326.22));
        multipleInputsMapReduceDriver.addOutput(new IntPairWritable(4,2), new DoubleWritable(174.55200000000002));//175.272
        multipleInputsMapReduceDriver.addOutput(new IntPairWritable(4,3), new DoubleWritable(779.356));
        multipleInputsMapReduceDriver.addOutput(new IntPairWritable(4,4), new DoubleWritable(608.0));
        multipleInputsMapReduceDriver.addOutput(new IntPairWritable(4,5), new DoubleWritable(3762.90445));//3762.9445
        multipleInputsMapReduceDriver.addOutput(new IntPairWritable(4,6), new DoubleWritable(4283.344999999999));

        multipleInputsMapReduceDriver.addOutput(new IntPairWritable(5,0), new DoubleWritable(791966.73218805957694904));
        multipleInputsMapReduceDriver.addOutput(new IntPairWritable(5,1), new DoubleWritable(719.8199999999999));
        multipleInputsMapReduceDriver.addOutput(new IntPairWritable(5,2), new DoubleWritable(85.50999999999999));
        multipleInputsMapReduceDriver.addOutput(new IntPairWritable(5,3), new DoubleWritable(-451.33000000000004));
        multipleInputsMapReduceDriver.addOutput(new IntPairWritable(5,4), new DoubleWritable(-1438.0));
        multipleInputsMapReduceDriver.addOutput(new IntPairWritable(5,5), new DoubleWritable(-988.89795));
        multipleInputsMapReduceDriver.addOutput(new IntPairWritable(5,6), new DoubleWritable(-3298.645));

        multipleInputsMapReduceDriver.runTest();
    }
}