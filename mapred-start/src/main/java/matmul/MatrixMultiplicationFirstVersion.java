package matmul;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.MultipleInputs;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;
import types.IntDoubleWritable;
import types.IntPairWritable;

import java.io.IOException;
import java.util.Map;
import java.util.TreeMap;

public class MatrixMultiplicationFirstVersion extends Configured implements Tool {

    public static void main(String[] args) throws Exception {
        ToolRunner.run(new Configuration(), new MatrixMultiplicationFirstVersion(), args);
    }


    @Override
    public int run(String[] args) throws Exception {
        int status = -1;
        Configuration configuration = new Configuration();
        //add parameters of matrix size, read out of filename

        String firstnum = args[0].split("-")[1];
        String secondnum = args[0].split("-")[2].split("\\.")[0];
        String thirdnum = args[1].split("-")[1];
        String fourthnum = args[1].split("-")[2].split("\\.")[0];

        configuration.setInt("maxSizeOfColumns", Integer.parseInt(fourthnum)); //80
        configuration.setInt("maxSizeOfRows", Integer.parseInt(firstnum)); //100
        configuration.setInt("columnNumberLeftMatrix", Integer.parseInt(secondnum)); //120
        configuration.setInt("rowNumberRightMatrix", Integer.parseInt(thirdnum)); //120

        //generate job
        Job job = Job.getInstance(configuration, MatrixMultiplicationFirstVersion.class.getSimpleName());
        job.setJarByClass(MatrixMultiplicationFirstVersion.class);
        MultipleInputs.addInputPath(job, new Path(args[0]), TextInputFormat.class, MatrixMultiplicationFirstVersion.MapperLeftMatrix.class);
        MultipleInputs.addInputPath(job, new Path(args[1]), TextInputFormat.class, MatrixMultiplicationFirstVersion.MapperRightMatrix.class);
        job.setReducerClass(MatrixMultiplicationFirstVersion.MatrixReducer.class);
        job.setCombinerClass(MatrixCombiner.class);
        job.setMapOutputValueClass(IntDoubleWritable.class);
        job.setMapOutputKeyClass(IntPairWritable.class);
        job.setOutputKeyClass(LongWritable.class);
        job.setOutputValueClass(IntWritable.class);
        //specify output file name
        job.getConfiguration().set("mapreduce.output.basename", "SC-" + secondnum + "-" + thirdnum + ".txt");
        FileOutputFormat.setOutputPath(job, new Path(args[2]));
        job.waitForCompletion(true);
        status = job.isSuccessful() ? 0 : -1;
        return status;
    }


    /**
     * Mapper used for the left matrix.
     */
    public static class MapperLeftMatrix extends Mapper<LongWritable, Text, IntPairWritable, IntDoubleWritable> {
        @Override
        public void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {

            int columnNumberLeftMatrix = context.getConfiguration().getInt("columnNumberLeftMatrix", 0);
            int rowNumberRightMatrix = context.getConfiguration().getInt("rowNumberRightMatrix", 0);

            if (columnNumberLeftMatrix == rowNumberRightMatrix) {
                int columnNumberRightMatrix = context.getConfiguration().getInt("maxSizeOfColumns", 0);
//TODO entfernen
                columnNumberRightMatrix = 7;
                String[] splitInput = value.toString().split("\t");
                int rowIndex = Integer.parseInt(splitInput[0]); //row of tuple
                int columnIndex = Integer.parseInt(splitInput[1]); //column index shows which element from left matrix should be multiplied with which element from right matrix
                double valueOfIndex = Double.parseDouble(splitInput[2]); //value
                //iterates over the number of elements this element is part of multiplication
                for (int i = 0; i < columnNumberRightMatrix; i++) {
                    context.write(new IntPairWritable(rowIndex, i), new IntDoubleWritable(columnIndex, valueOfIndex));
                }
            } else {
                System.out.println("Sizes of matrices do not match.");
                throw new IOException();
            }
        }
    }

    /**
     * Mapper used for the right matrix.
     */
    public static class MapperRightMatrix extends Mapper<LongWritable, Text, IntPairWritable, IntDoubleWritable> {
        @Override
        public void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {

            int columnNumberLeftMatrix = context.getConfiguration().getInt("columnNumberLeftMatrix", 0);
            int rowNumberRightMatrix = context.getConfiguration().getInt("rowNumberRightMatrix", 0);

            if (columnNumberLeftMatrix == rowNumberRightMatrix) {
                int rowNumberLeftMatrix = context.getConfiguration().getInt("maxSizeOfRows", 0);
//TODO entfernen
                rowNumberLeftMatrix = 6;
                String[] splitInput = value.toString().split("\t");
                int rowIndex = Integer.parseInt(splitInput[0]); //row index shows which element from left matrix should be multiplied with which element from right matrix
                int columnIndex = Integer.parseInt(splitInput[1]); //column index
                double valueOfIndex = Double.parseDouble(splitInput[2]); //value of element
                //iterates over the number of elements this element is part of multiplication
                for (int i = 0; i < rowNumberLeftMatrix; i++) {
                    context.write(new IntPairWritable(i, columnIndex), new IntDoubleWritable(rowIndex, valueOfIndex));
                }
            } else {
                System.out.println("Sizes of matrices do not match.");
                throw new IOException();
            }
        }
    }

    /**
     * Combiner.reduces amount of multiplication in Reducer.
     */
    public static class MatrixCombiner extends Reducer<IntPairWritable, IntDoubleWritable, IntPairWritable, IntDoubleWritable> {
        public void reduce(IntPairWritable key, Iterable<IntDoubleWritable> values, Context context)
                throws IOException, InterruptedException {
            Map<Integer, Double> map = new TreeMap<>();
//for every element in values
            for (IntDoubleWritable v : values) {
//check if map contains the key
                if (map.containsKey(v.getInt())) { //if map contains the key: multiply the value with the already known value
                    map.put(v.getInt(), map.get(v.getInt()).doubleValue() * v.getDouble());
                } else { //if map does not contain the key, add the key to the map
                    map.put(v.getInt(), v.getDouble());
                }
//every entry in map is written out
                for (Map.Entry<Integer, Double> entry : map.entrySet()) {
                    context.write(key, new IntDoubleWritable(entry.getKey(), entry.getValue()));
                }
            }
        }
    }

    /**
     * Reducer.
     */
    public static class MatrixReducer extends Reducer<IntPairWritable, IntDoubleWritable, IntPairWritable, DoubleWritable> {
        //        @Override
        protected void reduce(IntPairWritable key, Iterable<IntDoubleWritable> values, Context context)
                throws IOException, InterruptedException {

            Map<Integer, Double> map = new TreeMap<>();
//every key value pair is written in map
            for (IntDoubleWritable v : values) {
                if (map.containsKey(v.getInt())) { ////if map contains the key: multiply the value with the already known value
                    map.put(v.getInt(), map.get(v.getInt()).doubleValue() * v.getDouble());
                } else { //if map does not contain the key, add the key to the map
                    map.put(v.getInt(), v.getDouble());
                }
            } //after multiplication of items, sum values up per key
            double sum = 0;
            for (double i : map.values()) {
                sum += i;
            }
            context.write(key, new DoubleWritable(sum));


        }
    }

}
