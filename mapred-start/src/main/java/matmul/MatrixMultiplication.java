package matmul;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.MultipleInputs;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;
import types.IntDoubleWritable;
import types.IntPairWritable;

import java.io.IOException;
import java.util.Map;
import java.util.TreeMap;

public class MatrixMultiplication extends Configured implements Tool {


    public static void main(String[] args) throws Exception {
        ToolRunner.run(new Configuration(), new MatrixMultiplication(), args);
    }


    @Override
    public int run(String[] args) throws Exception {
        int status = -1;
        Configuration configuration = new Configuration();
        //add parameters of matrix size, read out of filename

        String firstnum = args[0].split("-")[1];
        String secondnum = args[0].split("-")[2].split("\\.")[0];
        String thirdnum = args[1].split("-")[1];
        String fourthnum = args[1].split("-")[2].split("\\.")[0];

        configuration.setInt("maxSizeOfColumns", Integer.parseInt(fourthnum)); //80
        configuration.setInt("maxSizeOfRows", Integer.parseInt(firstnum)); //100
        configuration.setInt("columnNumberLeftMatrix", Integer.parseInt(secondnum)); //120
        configuration.setInt("rowNumberRightMatrix", Integer.parseInt(thirdnum)); //120

        //generate job
        Job job = Job.getInstance(configuration, MatrixMultiplication.class.getSimpleName());
        job.setJarByClass(MatrixMultiplication.class);
        MultipleInputs.addInputPath(job, new Path(args[0]), TextInputFormat.class, MatrixMultiplication.MapperLeftMatrix.class);
        MultipleInputs.addInputPath(job, new Path(args[1]), TextInputFormat.class, MatrixMultiplication.MapperRightMatrix.class);
        job.setReducerClass(MatrixMultiplication.MatrixReducer.class);

        job.setMapOutputValueClass(IntDoubleWritable.class);
        job.setMapOutputKeyClass(IntPairWritable.class);

        job.setOutputKeyClass(IntPairWritable.class);
        job.setOutputValueClass(DoubleWritable.class);

        //specify output file name = SC-100-80
        job.getConfiguration().set("mapreduce.output.basename", "SC-" + firstnum + "-" + fourthnum);

        FileOutputFormat.setOutputPath(job, new Path(args[2]));
        job.waitForCompletion(true);
        status = job.isSuccessful() ? 0 : -1;
        return status;
    }


    /**
     * Mapper used for the left matrix.
     */
    public static class MapperLeftMatrix extends Mapper<LongWritable, Text, IntPairWritable, IntDoubleWritable> {

        private int columnNumberLeftMatrix;
        private int rowNumberRightMatrix;
        private  int columnNumberRightMatrix=-1;
        private boolean isTest = false;

        IntPairWritable rowIndexOutLeft = new IntPairWritable();
        IntDoubleWritable columnIndexOutLeft = new IntDoubleWritable();

        public boolean isTest() {
            return isTest;
        }

        public void setTest(boolean test) {
            isTest = test;
        }

        public int getColumnNumberLeftMatrix() {
            return columnNumberLeftMatrix;
        }

        public void setColumnNumberLeftMatrix(int columnNumberLeftMatrix) {
            this.columnNumberLeftMatrix = columnNumberLeftMatrix;
        }

        public int getRowNumberRightMatrix() {
            return rowNumberRightMatrix;
        }

        public void setRowNumberRightMatrix(int rowNumberRightMatrix) {
            this.rowNumberRightMatrix = rowNumberRightMatrix;
        }

        public int getColumnNumberRightMatrix() {
            return columnNumberRightMatrix;
        }

        public void setColumnNumberRightMatrix(int columnNumberRightMatrix) {
            this.columnNumberRightMatrix = columnNumberRightMatrix;
        }



        @Override
        protected void setup(Context context) throws IOException, InterruptedException {
            super.setup(context);
            if(!isTest) { //not true in test environment

                setColumnNumberLeftMatrix(context.getConfiguration().getInt("columnNumberLeftMatrix", 0));
                setRowNumberRightMatrix(context.getConfiguration().getInt("rowNumberRightMatrix", 0));
                if (this.getColumnNumberLeftMatrix() == this.getRowNumberRightMatrix()) {
                setColumnNumberRightMatrix(context.getConfiguration().getInt("maxSizeOfColumns", 0));
            }}
        }

        @Override
        public void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {

//            columnNumberLeftMatrix = context.getConfiguration().getInt("columnNumberLeftMatrix", 0);
//             rowNumberRightMatrix= context.getConfiguration().getInt("rowNumberRightMatrix", 0);

            if (this.columnNumberRightMatrix >-1) {
//               columnNumberRightMatrix = context.getConfiguration().getInt("maxSizeOfColumns", 0);

                String[] splitInput = value.toString().split("\t");
                int rowIndex = Integer.parseInt(splitInput[0]); //row of tuple
                int columnIndex = Integer.parseInt(splitInput[1]); //column index shows which element from left matrix should be multiplied with which element from right matrix
                double valueOfIndex = Double.parseDouble(splitInput[2]); //value
                //iterates over the number of elements this element is part of multiplication
                for (int i = 0; i < this.getColumnNumberRightMatrix(); i++) {

                    rowIndexOutLeft.set(rowIndex, i);
                    columnIndexOutLeft.set(columnIndex, valueOfIndex);
                    context.write(rowIndexOutLeft, columnIndexOutLeft);
                }
            } else {
                System.out.println("Sizes of matrices do not match.");
                throw new IOException();
            }
        }
    }

    /**
     * Mapper used for the right matrix.
     */
    public static class MapperRightMatrix extends Mapper<LongWritable, Text, IntPairWritable, IntDoubleWritable> {

        IntPairWritable rowIndexOutRight = new IntPairWritable();
        IntDoubleWritable columnIndexOutRight = new IntDoubleWritable();

        private int columnNumberLeftMatrix;
        private int rowNumberRightMatrix;
        private int rowNumberLeftMatrix=-1;
        boolean isTest= false;

        public int getColumnNumberLeftMatrix() {
            return columnNumberLeftMatrix;
        }

        public void setColumnNumberLeftMatrix(int columnNumberLeftMatrix) {
            this.columnNumberLeftMatrix = columnNumberLeftMatrix;
        }

        public int getRowNumberRightMatrix() {
            return rowNumberRightMatrix;
        }

        public void setRowNumberRightMatrix(int rowNumberRightMatrix) {
            this.rowNumberRightMatrix = rowNumberRightMatrix;
        }

        public int getRowNumberLeftMatrix() {
            return rowNumberLeftMatrix;
        }

        public void setRowNumberLeftMatrix(int rowNumberLeftMatrix) {
            this.rowNumberLeftMatrix = rowNumberLeftMatrix;
        }

        public boolean isTest() {
            return isTest;
        }

        public void setTest(boolean test) {
            isTest = test;
        }


        @Override
        protected void setup(Context context) throws IOException, InterruptedException {
            super.setup(context);
            if(!isTest){
                columnNumberLeftMatrix = context.getConfiguration().getInt("columnNumberLeftMatrix", 0);
                rowNumberRightMatrix = context.getConfiguration().getInt("rowNumberRightMatrix", 0);
                if (columnNumberLeftMatrix == rowNumberRightMatrix) {
                    rowNumberLeftMatrix = context.getConfiguration().getInt("maxSizeOfRows", 0);
                }
            }
        }

        @Override
        public void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {


                if(this.getRowNumberLeftMatrix() > -1){


                String[] splitInput = value.toString().split("\t");
                int rowIndex = Integer.parseInt(splitInput[0]); //row index shows which element from left matrix should be multiplied with which element from right matrix
                int columnIndex = Integer.parseInt(splitInput[1]); //column index
                double valueOfIndex = Double.parseDouble(splitInput[2]); //value of element
                //iterates over the number of elements this element is part of multiplication
                for (int i = 0; i < rowNumberLeftMatrix; i++) {

                    rowIndexOutRight.set(i, columnIndex);
                    columnIndexOutRight.set(rowIndex, valueOfIndex);
                    context.write(rowIndexOutRight, columnIndexOutRight);
                }
            } else {
                System.out.println("Sizes of matrices do not match.");
                throw new IOException();
            }
        }
    }

    /**
     * Reducer.
     */
    public static class MatrixReducer extends Reducer<IntPairWritable, IntDoubleWritable, IntPairWritable, DoubleWritable> {

        DoubleWritable sumOut = new DoubleWritable();

        @Override
        protected void reduce(IntPairWritable key, Iterable<IntDoubleWritable> values, Context context)
                throws IOException, InterruptedException {

            Map<Integer, Double> map = new TreeMap<>();
            //every key value pair is written in map
            for (IntDoubleWritable v : values) {
                if (map.containsKey(v.getInt())) { ////if map contains the key: multiply the value with the already known value
                    // not multiplicate the values of A and B if value of matrix A is 0.0 or value of matrix B is 0.0
                    if(!(map.get(v.getInt()) == 0.0) && (!(v.getDouble() == 0.0))){
                        map.put(v.getInt(), map.get(v.getInt()) * v.getDouble());
                    } else{
                        // we have to put a value in the map
                        map.put(v.getInt(), 0.0);
                    }
                } else { //if map does not contain the key, add the key to the map
                    map.put(v.getInt(), v.getDouble());
                }
            } //after multiplication of items, sum values up per key
            double sum = 0;
            for (double i : map.values()) {
                sum += i;
            }
            sumOut.set(sum);
            context.write(key, sumOut);
        }
    }

}