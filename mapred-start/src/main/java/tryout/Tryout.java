package tryout;

import java.util.*;

/*
 * Beispiel f�r ToolRunner-Einsatz. Die eigentliche Anwendung wird als Tool
 * implementiert, die Funktionalit�t in run.
 * yarn jar hadoop-0.0.1-SNAPSHOT.jar configured.TooledApp linelengths x
 *
 * Damit k�nnen folgende Kommandozeilenoptionen behandelt werden (werden aber
 * selten ben�tigt, daher Tool nicht unbedingt zwingend).
<pre>
    -archives <comma separated list of archives>- Specify comma separated archives to be unarchived on the compute machines. Applies only to job.
    -conf <configuration file>- Specify an application configuration file.
    -D <property>=<value>- Use value for given property.
    -files <comma separated list of files>- Specify comma separated files to be copied to the map reduce cluster. Applies only to job.
    -fs <file:///> or <hdfs://namenode:port>- Specify default filesystem URL to use. Overrides ?fs.defaultFS? property from configurations.
    -jt <local> or <resourcemanager:port>- Specify a ResourceManager. Applies only to job.
    -libjars <comma seperated list of jars>- Specify comma separated jar files to include in the classpath. Applies only to job.
</pre>
 */

class TooledApp {

    public static void main(String[] args) {

        for (int i = 0; i < 50; i++) {
            double random = Math.random();

            if(random < 0.1){
                System.out.print("UNTER 0.1");
            }

            System.out.println(random);
        }
    }


    /**
     * Prints all subsets of the list with k elements.
     * If two or more subsets contain the same elements in a different order, only
     * one of those subsets (that in which the order is like in the original list)
     * is printed.
     */
    public static <T> Map<String, String> printPowerset(List<T> list, int k) {
        if (list == null) throw new NullPointerException();
        if (k < 0 || k > list.size()) throw new IllegalArgumentException();
        if (k == 0) return new HashMap<>(

        );

        List<T> given = new LinkedList<T>(list);
        List<T> chosen = new LinkedList<T>();

        chosen.addAll(printRec(given, chosen, k, 0));

        Map<String, String> hintRainbowTable = new HashMap<>();
        StringBuilder sb = new StringBuilder();

        for (int i = 0; i < chosen.size(); i++) {
            sb.append(chosen.get(i));
            if ((i + 1) % k == 0) {
                hintRainbowTable.put(sb.toString(), "");
                sb.setLength(0);

            }
        }
        return hintRainbowTable;

//        System.out.println(chosen);
    }


    /**
     * Takes all elements (starting at element l) from given, adds them to chosen
     * and then calls itself.
     * If chosen contains k elements, the list is printed, the function returns
     * and the last element is put back in the given list.
     */
    private static <T> List<T> printRec(List<T> given, List<T> chosen, int k, int l) {
        List<T> finalList = new ArrayList<>();

        if (chosen.size() == k) {
            return chosen;
        } else {
            for (int i = l; i < given.size(); i++) {
                T elem = given.get(i);
                given.remove(elem);
                chosen.add(elem);
                finalList.addAll(printRec(given, chosen, k, 0)); // put 0 instead of i and all subsets will
                given.add(i, elem);            // be printed, even if they are permutations
                chosen.remove(elem);
            }
        }
        return finalList;
    }





}
