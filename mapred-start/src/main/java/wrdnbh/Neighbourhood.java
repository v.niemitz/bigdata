package wrdnbh;

import lsh.LSHMinHash;
import lsh.MinHash;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;
//Note types.IntArrayWritable; is not the same like  wrdnbh.IntArrayWritable;
//import types.IntArrayWritable;
import types.IntPairWritable;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;

public class Neighbourhood extends Configured implements Tool {

    public static void main(String[] args) throws Exception {
        ToolRunner.printGenericCommandUsage(System.out);

        int result = -1;
        result = ToolRunner.run(new Neighbourhood(), args);
        System.exit(result);
    }

    public int run(String[] args) throws Exception {
        // Configuration processed by ToolRunner
        Configuration conf = new Configuration();

        Job job = Job.getInstance(conf, "neighbourhood first");
        job.setJarByClass(Neighbourhood.class);

        job.setMapperClass(NeighbourhoodCountMapper.class);
        job.setReducerClass(NeighbourhoodCountReducer.class);

        job.setMapOutputKeyClass(IntPairWritable.class);
        job.setMapOutputValueClass(IntWritable.class);

        job.setOutputKeyClass(IntWritable.class);
        job.setOutputValueClass(IntWritable.class);

        job.setNumReduceTasks(10);
        job.setInputFormatClass(TextInputFormat.class);
        job.setOutputFormatClass(TextOutputFormat.class);

        FileInputFormat.setInputPaths(job, new Path(args[0]));
        FileOutputFormat.setOutputPath(job, new Path(args[1]));

        job.waitForCompletion(true);

        //second job
        Configuration secondconf = new Configuration();

        Job secondjob = Job.getInstance(secondconf, "neighbourhood second");
        secondjob.setJarByClass(Neighbourhood.class);

        secondjob.setMapperClass(NeighbourhoodSortMapper.class);
        secondjob.setReducerClass(NeighbourhoodSortReducer.class);

        secondjob.setMapOutputKeyClass(IntWritable.class);
        secondjob.setMapOutputValueClass(IntWritable.class);

        secondjob.setOutputKeyClass(IntWritable.class);
        secondjob.setOutputValueClass(IntArrayWritable.class);

        secondjob.setNumReduceTasks(10);
        secondjob.setInputFormatClass(TextInputFormat.class);
        secondjob.setOutputFormatClass(TextOutputFormat.class);

        FileInputFormat.setInputPaths(secondjob, new Path(args[1]));
        FileOutputFormat.setOutputPath(secondjob, new Path(args[2]));

        secondjob.waitForCompletion(true);

        Configuration thirdconf = new Configuration();

        Job thirdjob = Job.getInstance(thirdconf, "neighbourhood third");
        thirdjob.setJarByClass(Neighbourhood.class);

        thirdjob.setMapperClass(NeighbourhoodJaccardMapper.class);
        thirdjob.setReducerClass(NeighbourhoodJaccardReducer.class);

        thirdjob.setMapOutputKeyClass(IntWritable.class);
        thirdjob.setMapOutputValueClass(IntArrayWritable.class);

        thirdjob.setOutputKeyClass(IntWritable.class);
        thirdjob.setOutputValueClass(IntWritable.class);

        thirdjob.setNumReduceTasks(10);
        thirdjob.setInputFormatClass(TextInputFormat.class);
        thirdjob.setOutputFormatClass(TextOutputFormat.class);

        FileInputFormat.setInputPaths(thirdjob, new Path(args[2]));
        FileOutputFormat.setOutputPath(thirdjob, new Path(args[3]));


        int result = thirdjob.waitForCompletion(true) ? 0 : 1;

        // remove empty output path
//        FileSystem.get(conf).delete(new Path(args[1]), true);

        return result;
    }

    public static class NeighbourhoodCountMapper extends Mapper<LongWritable, Text, IntPairWritable, IntWritable> {

        /*
        (Sanchez,worked),1
         */

        /**
         * counts tupels of
         *
         * @param key
         * @param value
         * @param ctx
         * @throws IOException
         * @throws InterruptedException
         */

        //new
        IntPairWritable keyNeighbourOut = new IntPairWritable();
        IntWritable counterOut = new IntWritable(1);
        List<String> cleanWords = new ArrayList<>();


        @Override
        public void map(LongWritable key, Text value, Context ctx) throws IOException, InterruptedException {

            String[] split = value.toString()
                            .toLowerCase()
            .split("\\s+");

            if(split.length>1) {

                for (String word : split) {
                    char[] c = new char[word.length()];
                    int index;
                    int inside = 0;

                    for (index = 0; index < word.length(); index++) {
                        char buchstabe = word.charAt(index);
                        if ("abcdefghijklmnopqrstuvwxyz".indexOf(buchstabe) >= 0)
                            //continue; // Wird continue ausgelassen, werden nur genau die Zeichen angezeigt wie in der if bedingung aufgelistet
                            c[inside++] = buchstabe;
                    }
                    cleanWords.add(new String(c, 0 , inside)); // Gebe neuen String zurück, da Strings "immutable" sind.
                }
            }


            //TODO sonderfaelle abfangen: zeile hat nur zwei elemente: direkt hinschreiben, umdrehen danach
                for (int s = 0; s < cleanWords.size(); s++) {
                    int keyout = cleanWords.get(s).hashCode(); //every string is key once
                    for (int i = -3; i <= 3; i++) { //we need 3 neighbours
                        if ((s + i) < cleanWords.size() && (s + i) >= 0 && i != 0) { //bounds left, bounds right, not use the same value that is used
                            //new
                            keyNeighbourOut.set(keyout, cleanWords.get(s+i).hashCode());
                            ctx.write(keyNeighbourOut, counterOut); //counted once
                        }
                    }
                }

        }

    }
/*
Sanchez, 1,1,1,1, -> Sanchez,4 -> wird nicht in den Kontext geschrieben
 */

    /**
     * counts the occurences of neighbourhood pairs and filters out occurences smaller then 10
     */
    public static class NeighbourhoodCountReducer extends Reducer<IntPairWritable, IntWritable, IntWritable, IntWritable> {
        //new
        IntWritable keyOut = new IntWritable();
        IntWritable valueOut = new IntWritable();

        public void reduce(IntPairWritable key, Iterable<IntWritable> values,
                           Context context) throws IOException, InterruptedException {
            int h = 10;

            int sum = 0;
            for (IntWritable i : values) {
                sum += i.get();
            }
            if (sum >= h) { //breaking point
                //new
                keyOut.set(key.getX());
                valueOut.set(key.getY());
                context.write(keyOut, valueOut); //write out key and value pair
                //old
                //context.write(new IntWritable(key.getX()), new IntWritable(key.getY())); //write out key and value pair
            }

        }
    }

    /**
     * read in values again
     */
    public static class NeighbourhoodSortMapper extends Mapper<LongWritable, Text, IntWritable, IntWritable> {
        //new
        //TODO namen aendern
        IntWritable splitNull = new IntWritable();
        IntWritable splitEins = new IntWritable();

        @Override
        public void map(LongWritable key, Text value, Context ctx) throws IOException, InterruptedException {
            String[] split = value.toString().replaceAll(" ", "").split("\t");
            //new
            splitNull.set(Integer.parseInt(split[0]));
            splitEins.set(Integer.parseInt(split[1]));
            ctx.write(splitNull, splitEins);
            //old
            //ctx.write(new IntWritable(Integer.parseInt(split[0])), new IntWritable(Integer.parseInt(split[1])));

        }

    }

    /**
     * Sanchez,(worked, is)
     * <p>
     * fasst values zu listen zusammen
     */
    public static class NeighbourhoodSortReducer extends Reducer<IntWritable, IntWritable, IntWritable, IntArrayWritable> {

        //new
        IntArrayWritable arrayOut = new IntArrayWritable();

        public void reduce(IntWritable key, Iterable<IntWritable> values,
                           Context context) throws IOException, InterruptedException {
            List<Integer> outlist = new ArrayList<>();

            for (IntWritable i : values) {
                outlist.add(i.get());

            }

            Collections.sort(outlist); //TODO pruefen ob das wirklich hilft

            int[] outArray = new int[outlist.size()];
            for (int i = 0; i < outArray.length; i++) {
                outArray[i] = outlist.get(i);
            }
            //new
            arrayOut.set(outArray);
            context.write(key, new IntArrayWritable(outArray)); //write out key and value pair
            //old
            //context.write(key, new IntArrayWritable(outArray)); //write out key and value pair
        }

    }

    public static class NeighbourhoodJaccardMapper
            extends Mapper<LongWritable, Text, IntWritable, IntArrayWritable> {

        //TODO namen aendern
        IntWritable aInteger = new IntWritable();
        IntArrayWritable aArray = new IntArrayWritable();

        //rein kommt: Shingling Repraesentation
        @Override
        public void map(LongWritable key, Text value, Context ctx) throws IOException, InterruptedException {
//key nachbar1 nachbar2 nachbar3
            String[] split = value.toString().split("\t");
            //value before tab is the key
            Integer intkey = (Integer.parseInt(split[0].replace(" ", "")));

            String[] splitValues = split[1].replaceAll("\\[", "").replace("]", "")

                    .split(", ");

            List<Integer> outlist = new ArrayList<>();
            for (int i = 0; i < splitValues.length; i++) {
                outlist.add(Integer.parseInt(splitValues[i].replace(" ", "")));
            }
            int[] outArray = new int[outlist.size() + 1];
            outArray[0] = intkey;
            //first element of array is the key already
            for (int i = 1; i < outArray.length; i++) {
                outArray[i] = outlist.get(i - 1);
            }




//provide division by zero exception
            int numberOfStages = Math.max(outlist.size() / 3,1);

            UtilFunctions utilFunctions = new UtilFunctions(numberOfStages, 20, outlist.size());
            int[] hashArray = utilFunctions.hashSignature(outArray);

            for (int i : hashArray) {
                //new
                aInteger.set(i);
                aArray.set(outArray);
                ctx.write(aInteger, aArray);
                //old
                //ctx.write(new IntWritable(i), new IntArrayWritable(outArray));
            }

        }
    }

    public static class NeighbourhoodJaccardReducer
            extends Reducer<IntWritable, IntArrayWritable, IntWritable, IntWritable> {
        //new
        //TODO namen aendern
        IntWritable intOne = new IntWritable();
        IntWritable intTwo = new IntWritable();

        public void reduce(IntWritable key, Iterable<IntArrayWritable> values,
                           Context context) throws IOException, InterruptedException {

            //input is integer array -> convert to list of integer sets

            List<SetIntegerWrapper> integersetList = new ArrayList<>();

            //add values to a list
            for (IntArrayWritable i : values) {
                List<Integer> integerList = new ArrayList<>();

                for (int iw : i.getIntArray()) {
                    integerList.add(iw);
                }
                int listkey = integerList.get(0);
                integerList.remove(0);


                integersetList.add(new SetIntegerWrapper(new HashSet(integerList), listkey));
            }

            //foreach: key
            for (int i = 0; i < integersetList.size(); i++) {

                //foreach: other keys
                for (int j = 0; j < integersetList.size(); j++) {
                    if (i != j) {
                        //if jaccard index > 0.5
                        //context write key - value
                        if (UtilFunctions.jaccardIndex(integersetList.get(i).getSet(), integersetList.get(j).getSet()) > 0.5) {
                            //new
                            intOne.set(integersetList.get(i).getI());
                            intTwo.set(integersetList.get(j).getI());
                            context.write(intOne, intTwo);
                            //old
                            //context.write(new IntWritable(integersetList.get(i).getI()), new IntWritable(integersetList.get(j).getI()));
                        }
                    }
                }
            }

        }


    }
}

