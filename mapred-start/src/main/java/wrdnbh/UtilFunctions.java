package wrdnbh;

import lsh.LSH;
import lsh.MinHash;

import java.util.HashSet;
import java.util.Set;

/**
 * util functions copied from the given source code
 */
public class UtilFunctions extends LSH {

    private final MinHash mh;
    private static final double THRESHOLD = 0.5;


    public UtilFunctions(final int s, final int b, final int n) {
        super(s, b);
        int signature_size = computeSignatureSize(s, n);
        this.mh = new MinHash(signature_size, n);
    }




    private int computeSignatureSize(final int s, final int n) {

        int r = (int) Math.ceil(Math.log(1.0 / s) / Math.log(THRESHOLD)) + 1;
        return r * s;
    }


    public static double jaccardIndex(
            final Set<Integer> s1, final Set<Integer> s2) {

        Set<Integer> intersection = new HashSet<Integer>(s1);
        intersection.retainAll(s2);

        Set<Integer> union = new HashSet<Integer>(s1);
        union.addAll(s2);

        if (union.isEmpty()) {
            return 0;
        }

        return (double) intersection.size() / union.size();
    }

}
