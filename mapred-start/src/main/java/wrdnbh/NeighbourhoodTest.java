package wrdnbh;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mrunit.mapreduce.MapDriver;
import org.apache.hadoop.mrunit.mapreduce.ReduceDriver;
import org.junit.Before;
import org.junit.Test;
//Note types.IntArrayWritable; is not the same like  wrdnbh.IntArrayWritable;
//import types.IntArrayWritable;
import types.IntPairWritable;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

public class NeighbourhoodTest {

    MapDriver<LongWritable, Text, IntWritable, IntArrayWritable> neighbourhoodMapDriver;
    ReduceDriver<IntPairWritable, IntWritable, IntWritable, IntWritable> neighbourhoodReduceDriver;
    MapDriver<LongWritable, Text, IntPairWritable, IntWritable> neighbourhoodMapCountDriver;
    MapDriver<LongWritable, Text, IntWritable, IntWritable> neighbourhoodSortMapperDriver;
    ReduceDriver<IntWritable, IntWritable, IntWritable, IntArrayWritable> neigbourhoodSortReducerDriver;
    ReduceDriver<IntWritable, IntArrayWritable, IntWritable, IntWritable> neighbourhoodJaccardReduceDriver;

    @Before
    public void setUp() {
        Neighbourhood.NeighbourhoodJaccardMapper neighborhoodMapper = new Neighbourhood.NeighbourhoodJaccardMapper();
        Neighbourhood.NeighbourhoodCountMapper neighbourhoodCountMapper = new Neighbourhood.NeighbourhoodCountMapper();
        Neighbourhood.NeighbourhoodCountReducer neighbourhoodCountReducer = new Neighbourhood.NeighbourhoodCountReducer();
        Neighbourhood.NeighbourhoodSortMapper neighbourhoodSortMapper = new Neighbourhood.NeighbourhoodSortMapper();
        Neighbourhood.NeighbourhoodSortReducer neighbourhoodSortReducer = new Neighbourhood.NeighbourhoodSortReducer();
        Neighbourhood.NeighbourhoodJaccardReducer neighbourhoodJaccardReducer = new Neighbourhood.NeighbourhoodJaccardReducer();
        neighbourhoodMapDriver = MapDriver.newMapDriver(neighborhoodMapper);
        neighbourhoodMapCountDriver = MapDriver.newMapDriver(neighbourhoodCountMapper);
        neighbourhoodReduceDriver = ReduceDriver.newReduceDriver(neighbourhoodCountReducer);
        neighbourhoodSortMapperDriver = MapDriver.newMapDriver(neighbourhoodSortMapper);
        neigbourhoodSortReducerDriver = ReduceDriver.newReduceDriver(neighbourhoodSortReducer);
        neighbourhoodJaccardReduceDriver = ReduceDriver.newReduceDriver(neighbourhoodJaccardReducer);
    }


    @Test
    public void neighbourhoodMapTest() throws IOException {
        neighbourhoodMapCountDriver.withInput(new LongWritable(), new Text("Sanchez worked with Sylvanus Morley, a noted Mayanist."));
        neighbourhoodMapCountDriver.withOutput(new IntPairWritable("sanchez".hashCode(), "worked".hashCode()), new IntWritable(1));
        neighbourhoodMapCountDriver.withOutput(new IntPairWritable("sanchez".hashCode(), "with".hashCode()), new IntWritable(1));
        neighbourhoodMapCountDriver.withOutput(new IntPairWritable("sanchez".hashCode(), "sylvanus".hashCode()), new IntWritable(1));

        neighbourhoodMapCountDriver.withOutput(new IntPairWritable("worked".hashCode(), "sanchez".hashCode()), new IntWritable(1));
        neighbourhoodMapCountDriver.withOutput(new IntPairWritable("worked".hashCode(), "with".hashCode()), new IntWritable(1));
        neighbourhoodMapCountDriver.withOutput(new IntPairWritable("worked".hashCode(), "sylvanus".hashCode()), new IntWritable(1));
        neighbourhoodMapCountDriver.withOutput(new IntPairWritable("worked".hashCode(), "morley".hashCode()), new IntWritable(1));

        neighbourhoodMapCountDriver.withOutput(new IntPairWritable("with".hashCode(), "sanchez".hashCode()), new IntWritable(1));
        neighbourhoodMapCountDriver.withOutput(new IntPairWritable("with".hashCode(), "worked".hashCode()), new IntWritable(1));
        neighbourhoodMapCountDriver.withOutput(new IntPairWritable("with".hashCode(), "sylvanus".hashCode()), new IntWritable(1));
        neighbourhoodMapCountDriver.withOutput(new IntPairWritable("with".hashCode(), "morley".hashCode()), new IntWritable(1));
        neighbourhoodMapCountDriver.withOutput(new IntPairWritable("with".hashCode(), "a".hashCode()), new IntWritable(1));

        neighbourhoodMapCountDriver.withOutput(new IntPairWritable("sylvanus".hashCode(), "sanchez".hashCode()), new IntWritable(1));
        neighbourhoodMapCountDriver.withOutput(new IntPairWritable("sylvanus".hashCode(), "worked".hashCode()), new IntWritable(1));
        neighbourhoodMapCountDriver.withOutput(new IntPairWritable("sylvanus".hashCode(), "with".hashCode()), new IntWritable(1));
        neighbourhoodMapCountDriver.withOutput(new IntPairWritable("sylvanus".hashCode(), "morley".hashCode()), new IntWritable(1));
        neighbourhoodMapCountDriver.withOutput(new IntPairWritable("sylvanus".hashCode(), "a".hashCode()), new IntWritable(1));
        neighbourhoodMapCountDriver.withOutput(new IntPairWritable("sylvanus".hashCode(), "noted".hashCode()), new IntWritable(1));

        neighbourhoodMapCountDriver.withOutput(new IntPairWritable("morley".hashCode(), "worked".hashCode()), new IntWritable(1));
        neighbourhoodMapCountDriver.withOutput(new IntPairWritable("morley".hashCode(), "with".hashCode()), new IntWritable(1));
        neighbourhoodMapCountDriver.withOutput(new IntPairWritable("morley".hashCode(), "sylvanus".hashCode()), new IntWritable(1));
        neighbourhoodMapCountDriver.withOutput(new IntPairWritable("morley".hashCode(), "a".hashCode()), new IntWritable(1));
        neighbourhoodMapCountDriver.withOutput(new IntPairWritable("morley".hashCode(), "noted".hashCode()), new IntWritable(1));
        neighbourhoodMapCountDriver.withOutput(new IntPairWritable("morley".hashCode(), "mayanist".hashCode()), new IntWritable(1));

        neighbourhoodMapCountDriver.withOutput(new IntPairWritable("a".hashCode(), "with".hashCode()), new IntWritable(1));
        neighbourhoodMapCountDriver.withOutput(new IntPairWritable("a".hashCode(), "sylvanus".hashCode()), new IntWritable(1));
        neighbourhoodMapCountDriver.withOutput(new IntPairWritable("a".hashCode(), "morley".hashCode()), new IntWritable(1));
        neighbourhoodMapCountDriver.withOutput(new IntPairWritable("a".hashCode(), "noted".hashCode()), new IntWritable(1));
        neighbourhoodMapCountDriver.withOutput(new IntPairWritable("a".hashCode(), "mayanist".hashCode()), new IntWritable(1));

        neighbourhoodMapCountDriver.withOutput(new IntPairWritable("noted".hashCode(), "sylvanus".hashCode()), new IntWritable(1));
        neighbourhoodMapCountDriver.withOutput(new IntPairWritable("noted".hashCode(), "morley".hashCode()), new IntWritable(1));
        neighbourhoodMapCountDriver.withOutput(new IntPairWritable("noted".hashCode(), "a".hashCode()), new IntWritable(1));
        neighbourhoodMapCountDriver.withOutput(new IntPairWritable("noted".hashCode(), "mayanist".hashCode()), new IntWritable(1));

        neighbourhoodMapCountDriver.withOutput(new IntPairWritable("mayanist".hashCode(), "morley".hashCode()), new IntWritable(1));
        neighbourhoodMapCountDriver.withOutput(new IntPairWritable("mayanist".hashCode(), "a".hashCode()), new IntWritable(1));
        neighbourhoodMapCountDriver.withOutput(new IntPairWritable("mayanist".hashCode(), "noted".hashCode()), new IntWritable(1));

        neighbourhoodMapCountDriver.runTest();
    }

    @Test
    public void neighbourhoodReduceCountTest() throws IOException {
        neighbourhoodReduceDriver.withInput(new IntPairWritable(1, 1), new ArrayList<>(Arrays.asList((new IntWritable(1)), (new IntWritable(12)))));
        neighbourhoodReduceDriver.withInput(new IntPairWritable(1, 2), new ArrayList<>(Arrays.asList((new IntWritable(3)), (new IntWritable(3)))));
        neighbourhoodReduceDriver.withInput(new IntPairWritable(2, 1), new ArrayList<>(Arrays.asList((new IntWritable(5)), (new IntWritable(5)))));
        neighbourhoodReduceDriver.withOutput(new IntWritable(1), new IntWritable(1));
        neighbourhoodReduceDriver.withOutput(new IntWritable(2), new IntWritable(1));

        neighbourhoodReduceDriver.runTest();
    }


    @Test
    public void neighbourhoodSortMapperTest() throws IOException {
        neighbourhoodSortMapperDriver.withInput(new LongWritable(), new Text(" 1\t11 "));
        neighbourhoodSortMapperDriver.withInput(new LongWritable(), new Text(" 1\t13 "));
        neighbourhoodSortMapperDriver.withInput(new LongWritable(), new Text(" 1\t12 "));

        neighbourhoodSortMapperDriver.withOutput(new IntWritable(1), new IntWritable(11));
        neighbourhoodSortMapperDriver.withOutput(new IntWritable(1), new IntWritable(13));
        neighbourhoodSortMapperDriver.withOutput(new IntWritable(1), new IntWritable(12));
        neighbourhoodSortMapperDriver.runTest();
    }

    @Test
    public void neighbourhoodSortReducerTest() throws IOException{
        neigbourhoodSortReducerDriver.withInput(new IntWritable(1), new ArrayList<>(Arrays.asList((new IntWritable(1)), (new IntWritable(2)), new IntWritable(3))));
        neigbourhoodSortReducerDriver.withOutput(new IntWritable(1), new IntArrayWritable(new int[]{1,2,3}));
        neigbourhoodSortReducerDriver.runTest();
    }


    @Test
    public void neighbourhoodJaccardMapper() throws IOException {

        neighbourhoodMapDriver.withInput(new LongWritable(), new Text(" 1\t11, 12, 14, 15, 17, 200, 205, 2007"));
        neighbourhoodMapDriver.withInput(new LongWritable(), new Text(" 1\t11, 12, 14, 15, 17, 200, 205, 2006"));
        neighbourhoodMapDriver.withOutput(new IntWritable(), new IntArrayWritable(new int[]{1, 11,12,14,15,17,200,205,2007}));

        neighbourhoodMapDriver.runTest();
    }



    @Test
    public void neigbourhoodJaccardReduceDriverTest() throws IOException{

        neighbourhoodJaccardReduceDriver.withInput(new IntWritable(44), new  ArrayList<>(Arrays.asList(new IntArrayWritable(new int[]{789,1,2,3,4}), new IntArrayWritable(new int[]{799,1,2,3,5}), new IntArrayWritable(new int[]{800,6,7}))));
                neighbourhoodJaccardReduceDriver.withOutput(new IntWritable(789), new IntWritable(799));
        neighbourhoodJaccardReduceDriver.withOutput(new IntWritable(799), new IntWritable(789));
                neighbourhoodJaccardReduceDriver.runTest();
    }

}

