package wrdnbh;

//import ncdc.IntArrayWritable;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.*;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;
import types.IntArrayWritable;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Nachbarschaft extends Configured implements Tool {

    public static void main(String[] args) throws Exception {
        int exitcode = ToolRunner.run(new Nachbarschaft(), args);
        System.exit(exitcode);
    }

    @Override
    public int run(String[] args) throws Exception {
        if (args.length != 2) {
            System.err.printf("Usage: %s [generic options] <input> <output>\n", getClass().getSimpleName());
            ToolRunner.printGenericCommandUsage(System.err);
            return -1;
        }

        Job job = Job.getInstance(getConf(), getClass().getSimpleName());

        FileInputFormat.addInputPath(job, new Path(args[0]));
        FileOutputFormat.setOutputPath(job, new Path(args[1]));

        job.setJarByClass(Nachbarschaft.class);
        job.setMapperClass(Nachbarschaft.NeighborhoodMapper.class);

        job.setMapOutputKeyClass(IntWritable.class);
        job.setMapOutputValueClass(IntArrayWritable.class);

        job.setOutputKeyClass(IntWritable.class);
        job.setOutputValueClass(IntArrayWritable.class);

        return (job.waitForCompletion(true) ? 0 : 1);
    }



    /*Unter der Nachbarschaft eines Wortes w wollen wir alle Worte verstehen,
    die mindestens h Mal in einem Abstand von maximal k Worten von w in einem Satz vorkommen.
    (Die Eingabe besteht aus einem Satz pro Zeile.)
    Danach sollen die Worte identifiziert werden, die eine �hnliche Nachbarschaft besitzen
    (und daher vielleicht �hnlich benutzt und somit �hnlich zueinander sind).
    Die finale Ausgabe enth�lt (m�glichst viele, aber nicht unbedingt alle) Wortpaare,
    deren Jaccard-Index auf ihren Wort-Nachbarschaften >= 1/2 ist.
    Testm�glichkeit:
    F�hren Sie das Programm f�r die Eingabe enwiki-latest-sentences-head10k.txt im Ordner /user/data/raw/wsen/ aus.
    (10000 S�tze aus der englischen Wikipedia.) (Die Datei ohne den Sufix head10k ist die komplette englischsprachige Wikipedia.
    Um Ihr Programm auf dieser Datei laufen zu lassen (13.7 GB), m�ssen wir Ausf�hrungen koordinieren...)
    Vereinheitlichungen:
    Alle Zeichen, die keine Buchstaben sind, werden einfach gel�scht.
    Alle Texte werden in Kleinbuchstaben konvertiert.
    Aus einer Zeile Dies Ist2 Ein. 23 Text! werden also die Worte dies ist ein text.
    Statt der Worte im Klartext k�nnen Sie die Worte durch eine Zahl repr�sentieren
    (String.hashCode()), wobei wir (seltene) Hash-Kollisionen ignorieren.
    (Bei Bedarf losgel�st von dieser Aufgabe eine �bersetzungstabelle von Wort zu Hashcode anlegen.)
    Als Gr��e der Nachbarschaft setzen wir k = 3.
    Als Mindesth�ufigkeit setzen wir h = 10.
    In diesem Satz besteht die Nachbarschaft des sechsten Wortes also aus den Worten: Satz, besteht, die, des, sechsten, Wortes.
    */

    //Unter der Nachbarschaft eines Wortes w wollen wir alle Worte verstehen, die mindestens 10 Mal in einem Abstand von maximal 3 Worten von w in einem Satz vorkommen.
    //word w
    //neighborhood size k = 3
    //minimum frequency h = 10

    //public static class NeighborhoodMapper extends Mapper<LongWritable, Text, IntWritable, IntArrayWritable> {
    public static class NeighborhoodMapper extends Mapper<LongWritable, Text, IntWritable, IntArrayWritable> {

        IntWritable keyWord = new IntWritable();
        //TODO umstellen auf hash und value als Liste von Intwirtable
        //IntWritable keyWord = new IntWritable();
        //IntWritable[] neighbors = new IntWritable[6];

        int[] neighbors = new int[6];
        IntArrayWritable neighborhood = new IntArrayWritable();

        List<String> cleanWords = new ArrayList<>();
        List<Integer> hashedWords = new ArrayList<>();
        List<Integer> nW = new ArrayList<>();

        @Override
        public void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {

            //Alle Zeilen werden in Kleinbuchstaben konvertiert an Leerzeichen (auch Tabulatoren) gesplittet und in ein String-Array "line" geschrieben.
            String[] line = value.toString().toLowerCase().split("\\s");

            // Bereinige Satz von allem was keine Buchstaben sind
            for(String w : line) {
                //Quelle: https://www.java-forum.org/thema/string-einzelne-buchstaben-ersetzen.143168/ Note: angepasst
                char[] c = new char[w.length()];
                int index;
                int inside = 0;

                for (index = 0; index < w.length(); index++) {
                    char buchstabe = w.charAt(index);
                    if ("abcdefghijklmnopqrstuvwxyz".indexOf(buchstabe) >= 0)
                        //continue; // Wird continue ausgelassen, werden nur genau die Zeichen angezeigt wie in der if bedingung aufgelistet
                        c[inside++] = buchstabe;
                }
                cleanWords.add(new String(c, 0 , inside)); // Gebe neuen String zur�ck, da Strings "immutable" sind.
            }

            for(String w:cleanWords){
                hashedWords.add(w.hashCode());
            }
            // System.out.println(hashedWords);
            int neighborOneAfter = 0;
            int neighborTwoAfter = 0;
            int neighborThreeAfter = 0;
            int neighborOneBefore = 0;
            int neighborTwoBefore = 0;
            int neighborThreeBefore = 0;


            for (int w:hashedWords){
                keyWord.set(w);
                System.out.println(keyWord);
                int neighborsLength = neighbors.length;

                for(int i=0;i<hashedWords.size();i++) {
                    if(neighborsLength==-1) {
                        break;
                    }
                    //System.out.println("neighborsLength au�en "+neighborsLength);
                    for (int n =0;n<neighborsLength;n++) {
                        //System.out.println("neighborsLength innen "+neighborsLength);
                        if ((i + 1) < hashedWords.size()) {
                            // System.out.println("1. danach n innen "+ n + " i "+i);
                            neighborOneAfter = hashedWords.get(i + 1);
                            //neighbors[n]=new IntWritable(neighborOneAfter);
                            neighbors[n]=neighborOneAfter;
                            n++;
                            neighborsLength -= 1;
                            System.out.println("1. danach " + neighborOneAfter + " n "+ n);
                        }
                        if ((i + 2) < hashedWords.size()) {
                            // System.out.println("2. danach n innen "+ n+ " i "+i);
                            neighborTwoAfter = hashedWords.get(i + 2);
                            //neighbors[n]=new IntWritable(neighborTwoAfter);
                            neighbors[n]=neighborTwoAfter;
                            n++;
                            neighborsLength -= 1;
                            System.out.println("2. danach " + neighborTwoAfter + " n "+ n);
                        }
                        if ((i + 3) < hashedWords.size()) {
                            //System.out.println(" n innen"+ n+ " i "+i);
                            neighborThreeAfter = hashedWords.get(i + 3);
                            //neighbors[n]=new IntWritable(neighborThreeAfter);
                            neighbors[n]=neighborThreeAfter;
                            n++;
                            neighborsLength -= 1;
                            System.out.println("3. danach " + neighborThreeAfter + " n "+ n);
                        }
                        if ((i - 1) >= 0) {
                            //System.out.println("1.vorher n innen "+ n+ " i "+i);
                            neighborOneBefore = hashedWords.get(i - 1);
                            //neighbors[n]=new IntWritable(neighborOneBefore);
                            neighbors[n]=neighborOneBefore;
                            n++;
                            neighborsLength -= 1;
                            System.out.println("1. vorher " + neighborOneBefore + " n "+ n);
                        }
                        if ((i - 2) >= 0) {
                            // System.out.println("2. vorher n innen "+ n+ " i "+i);
                            neighborTwoBefore = hashedWords.get(i - 2);
                            //neighbors[n]=new IntWritable(neighborTwoBefore);
                            neighbors[n]=neighborTwoBefore;
                            n++;
                            neighborsLength -= 1;
                            System.out.println("2. vorher " + neighborTwoBefore + " n "+ n);
                        }
                        if ((i - 3) >= 0) {
                            //  System.out.println("3. vorher n innen "+ n+ " i "+i);
                            neighborThreeBefore = hashedWords.get(i - 3);
                            //neighbors[n]=new IntWritable(neighborThreeBefore);
                            neighbors[n]=neighborThreeBefore;
                            n++;
                            neighborsLength -= 1;
                            System.out.println("3. vorher " + neighborThreeBefore + " n "+ n);
                        }
                        System.out.println("nachbar "+neighbors);
                    }
                    //System.out.println("nachbar "+neighbors.);
                    //nW.add(neighbor);
                    //Alle Nachbarn speichern
                    //neighborhood.set(neighbor);
                    //                    neighborhood.set(count, neighbor);
//                    count++;
                }

            }
            System.out.println("nachbar aussen:  "+neighbors);
            neighborhood.set(neighbors);
            System.out.println("keyWord "+keyWord + " nachbarn "+neighbors);
            context.write(keyWord, neighborhood);
        }
        //Alle Nachbarn speichern
//            int count=0;
//            for(int i=0;i<nW.size();i++) {
//                System.out.println("elem"+nW.get(i));
//                neighborhood.set(count, nW.get(i));
//                count++;
//            }
        //System.out.println("key "+keyWord+" nW "+nW);
        //System.out.println("key "+keyWord+" n3 "+neighborhood);


        //Fuer alle W�rter im Satz, nehme wenn m�glich die 3 vorherigen und die 3 nachfolgenden W�rter als Nachbarn auf


        //Fuer das erste Wort im Satz, nehme die naechsten 3 als Nachbarn in die Nachbarschafts liste auf
        //Fuer das letze Wort  im Satz nehme die vorherigen 3 als Nachbarn in die Nachbarschafts liste auf
        //F�r das zweite nimm das vorherige und die naechsten 3 als Nachbarn in die Nachbarschafts liste auf



    }


//            word.set(line[0]);
//            neighborhood.set(line[5]);
//

//            context.write(word, neighborhood);
}
//lsh-Algo
//






