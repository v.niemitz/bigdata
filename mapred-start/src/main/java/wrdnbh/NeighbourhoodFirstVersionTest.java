package wrdnbh;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mrunit.mapreduce.MapDriver;
import org.apache.hadoop.mrunit.mapreduce.ReduceDriver;
import org.junit.Before;
import org.junit.Test;
import types.IntArrayWritable;
import types.IntPairWritable;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

public class NeighbourhoodFirstVersionTest {

    MapDriver<LongWritable, Text, IntWritable, IntArrayWritable> neighbourhoodMapDriver;
    ReduceDriver<IntPairWritable, IntWritable, IntWritable, IntWritable> neighbourhoodReduceDriver;
    MapDriver<LongWritable, Text, IntPairWritable, IntWritable> neighbourhoodMapCountDriver;
    MapDriver<LongWritable, Text, IntWritable, IntWritable> neighbourhoodSortMapperDriver;
    ReduceDriver<IntWritable, IntWritable, IntWritable, IntArrayWritable> neigbourhoodSortReducerDriver;
    ReduceDriver<IntWritable, IntArrayWritable, IntWritable, IntWritable> neighbourhoodJaccardReduceDriver;

    @Before
    public void setUp() {
        NeighbourhoodFirstVersion.NeighbourhoodJaccardMapper neighborhoodMapper = new NeighbourhoodFirstVersion.NeighbourhoodJaccardMapper();
        NeighbourhoodFirstVersion.NeighbourhoodCountMapper neighbourhoodCountMapper = new NeighbourhoodFirstVersion.NeighbourhoodCountMapper();
        NeighbourhoodFirstVersion.NeighbourhoodCountReducer neighbourhoodCountReducer = new NeighbourhoodFirstVersion.NeighbourhoodCountReducer();
        NeighbourhoodFirstVersion.NeighbourhoodSortMapper neighbourhoodSortMapper = new NeighbourhoodFirstVersion.NeighbourhoodSortMapper();
        NeighbourhoodFirstVersion.NeighbourhoodSortReducer neighbourhoodSortReducer = new NeighbourhoodFirstVersion.NeighbourhoodSortReducer();
        NeighbourhoodFirstVersion.NeighbourhoodJaccardReducer neighbourhoodJaccardReducer = new NeighbourhoodFirstVersion.NeighbourhoodJaccardReducer();
        neighbourhoodMapDriver = MapDriver.newMapDriver(neighborhoodMapper);
        neighbourhoodMapCountDriver = MapDriver.newMapDriver(neighbourhoodCountMapper);
        neighbourhoodReduceDriver = ReduceDriver.newReduceDriver(neighbourhoodCountReducer);
        neighbourhoodSortMapperDriver = MapDriver.newMapDriver(neighbourhoodSortMapper);
        neigbourhoodSortReducerDriver = ReduceDriver.newReduceDriver(neighbourhoodSortReducer);
        neighbourhoodJaccardReduceDriver = ReduceDriver.newReduceDriver(neighbourhoodJaccardReducer);


    }


    @Test
    public void neighbourhoodMapTest() throws IOException {
        neighbourhoodMapCountDriver.withInput(new LongWritable(), new Text("Sanchez worked with Sylvanus Morley, a noted Mayanist."));
        neighbourhoodMapCountDriver.withOutput(new IntPairWritable("sanchez".hashCode(), "worked".hashCode()), new IntWritable(1));
        neighbourhoodMapCountDriver.runTest();
    }

    @Test
    public void neighbourhoodReduceCountTest() throws IOException {
        neighbourhoodReduceDriver.withInput(new IntPairWritable(1, 1), new ArrayList<>(Arrays.asList((new IntWritable(1)), (new IntWritable(12)))));
        neighbourhoodReduceDriver.withInput(new IntPairWritable(1, 2), new ArrayList<>(Arrays.asList((new IntWritable(3)), (new IntWritable(3)))));
        neighbourhoodReduceDriver.withInput(new IntPairWritable(2, 1), new ArrayList<>(Arrays.asList((new IntWritable(5)), (new IntWritable(5)))));
        neighbourhoodReduceDriver.withOutput(new IntWritable(1), new IntWritable(1));
        neighbourhoodReduceDriver.withOutput(new IntWritable(2), new IntWritable(1));

        neighbourhoodReduceDriver.runTest();
    }


    @Test
    public void neighbourhoodSortMapperTest() throws IOException {
        neighbourhoodSortMapperDriver.withInput(new LongWritable(), new Text(" 1\t11 "));
        neighbourhoodSortMapperDriver.withInput(new LongWritable(), new Text(" 1\t13 "));
        neighbourhoodSortMapperDriver.withInput(new LongWritable(), new Text(" 1\t12 "));

        neighbourhoodSortMapperDriver.withOutput(new IntWritable(1), new IntWritable(11));
        neighbourhoodSortMapperDriver.withOutput(new IntWritable(1), new IntWritable(13));
        neighbourhoodSortMapperDriver.withOutput(new IntWritable(1), new IntWritable(12));
        neighbourhoodSortMapperDriver.runTest();
    }

    @Test
    public void neighbourhoodSortReducerTest() throws IOException{
        neigbourhoodSortReducerDriver.withInput(new IntWritable(1), new ArrayList<>(Arrays.asList((new IntWritable(1)), (new IntWritable(2)), new IntWritable(3))));
        neigbourhoodSortReducerDriver.withOutput(new IntWritable(1), new IntArrayWritable(new int[]{1,2,3}));
        neigbourhoodSortReducerDriver.runTest();
    }


    @Test
    public void neighbourhoodJaccardMapper() throws IOException {

        neighbourhoodMapDriver.withInput(new LongWritable(), new Text(" 1\t11, 12, 14, 15, 17, 200, 205, 2007"));
        neighbourhoodMapDriver.withInput(new LongWritable(), new Text(" 1\t11, 12, 14, 15, 17, 200, 205, 2006"));
        neighbourhoodMapDriver.withOutput(new IntWritable(), new IntArrayWritable(new int[]{1, 11,12,14,15,17,200,205,2007}));

        neighbourhoodMapDriver.runTest();


    }



    @Test
    public void neigbourhoodJaccardReduceDriverTest() throws IOException{

        neighbourhoodJaccardReduceDriver.withInput(new IntWritable(44), new  ArrayList<>(Arrays.asList(new IntArrayWritable(new int[]{789,1,2,3,4}), new IntArrayWritable(new int[]{799,1,2,3,5}), new IntArrayWritable(new int[]{800,6,7}))));
                neighbourhoodJaccardReduceDriver.withOutput(new IntWritable(789), new IntWritable(799));
        neighbourhoodJaccardReduceDriver.withOutput(new IntWritable(799), new IntWritable(789));
                neighbourhoodJaccardReduceDriver.runTest();


    }


}
