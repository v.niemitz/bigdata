package wrdnbh;

import java.util.Set;

public class SetIntegerWrapper {

    private Set set;
    private int i;

    public SetIntegerWrapper(Set set, int i){
        this.set=set;
        this.i=i;
    }

    public Set getSet() {
        return set;
    }

    public void setSet(Set set) {
        this.set = set;
    }

    public int getI() {
        return i;
    }

    public void setI(int i) {
        this.i = i;
    }
}
