package wrdnbh;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mrunit.mapreduce.MapDriver;
import org.apache.hadoop.mrunit.mapreduce.MultipleInputsMapReduceDriver;
import org.apache.hadoop.mrunit.mapreduce.ReduceDriver;
import org.junit.Before;
import org.junit.Test;
import types.IntArrayWritable;


import java.io.IOException;


public class WrdNbhTest {

    MapDriver<LongWritable, Text, IntWritable, IntArrayWritable> nachbarschaftMapperDriver;

    @Before
    public void setUp() {
        Nachbarschaft.NeighborhoodMapper neighborhoodMapper = new Nachbarschaft.NeighborhoodMapper();

        nachbarschaftMapperDriver = MapDriver.newMapDriver(neighborhoodMapper);

    }

    @Test
    public void testNachbarschaftrMapper() throws IOException {
        //[99222, 104586, 100426, 3522900, 99345, 3314158, 104586]
        nachbarschaftMapperDriver.withInput(new LongWritable(), new Text("Das ist ein Satz, der Lang2 ist."));

//        nachbarschaftMapperDriver.withOutput(new Text("das"),new Text( "ist, ein, satz"));
//        nachbarschaftMapperDriver.withOutput(new Text("ist"),new Text( "das, ein, satz, der"));
//        nachbarschaftMapperDriver.withOutput(new Text("ein"), new Text("das, ist, satz, der, lang"));
//        nachbarschaftMapperDriver.withOutput(new Text("satz"),new Text( "das, ist, ein, der, lang, ist"));
//        nachbarschaftMapperDriver.withOutput(new Text("der"), new Text("ist, ein, satz, lang, ist"));
//        nachbarschaftMapperDriver.withOutput(new Text("lang"),new Text( "ein, satz, ist"));
//        nachbarschaftMapperDriver.withOutput(new Text("ist"), new Text("satz, der, lang"));
        IntWritable[] values1 = new IntWritable[3];
        values1[0]= new IntWritable(104586);
        values1[1]= new IntWritable(100426);
        values1[2]= new IntWritable(3522900);

        IntWritable[] values2 = new IntWritable[4];
        values2[0]= new IntWritable(99222);
        values2[1]= new IntWritable(100426);
        values2[2]= new IntWritable(3522900);
        values2[3]= new IntWritable(99345);

        IntWritable[] values3 = new IntWritable[5];
        values3[0]= new IntWritable(99222);
        values3[1]= new IntWritable(100426);
        values3[2]= new IntWritable(3522900);
        values3[3]= new IntWritable(99345);
        values3[4]= new IntWritable(3314158);

        IntWritable[] values4 = new IntWritable[6];
        values4[0]= new IntWritable(99222);
        values4[1]= new IntWritable(100426);
        values4[2]= new IntWritable(100426);
        values4[3]= new IntWritable(99345);
        values4[4]= new IntWritable(3314158);
        values4[5]= new IntWritable(104586);

        IntWritable[] values5 = new IntWritable[5];
        values5[0]= new IntWritable(100426);
        values5[1]= new IntWritable(100426);
        values5[2]= new IntWritable(3522900);
        values5[3]= new IntWritable(3314158);
        values5[4]= new IntWritable(104586);

        IntWritable[] values6 = new IntWritable[3];
        values6[0]= new IntWritable(100426);
        values6[1]= new IntWritable(3522900);
        values6[2]= new IntWritable(104586);


        IntWritable[] values7 = new IntWritable[3];
        values7[0]= new IntWritable(3522900);
        values7[1]= new IntWritable(99345);
        values7[2]= new IntWritable(3314158);

        //nachbarschaftMapperDriver.withOutput(new IntWritable(99222),new IntArrayWritable(new IntWritable(104586), new IntWritable(100426), new IntWritable(3522900)));
        nachbarschaftMapperDriver.withOutput(new IntWritable(99222),new IntArrayWritable(new int[]{1,2,3,4,5}));
//        nachbarschaftMapperDriver.withOutput(new IntWritable(99222),new IntArrayWritable(values1));
//        nachbarschaftMapperDriver.withOutput(new IntWritable(104586),new IntArrayWritable(values2));
//        nachbarschaftMapperDriver.withOutput(new IntWritable(100426), new IntArrayWritable(values3));
//        nachbarschaftMapperDriver.withOutput(new IntWritable(3522900),new IntArrayWritable(values4));
//        nachbarschaftMapperDriver.withOutput(new IntWritable(99345), new IntArrayWritable(values5));
//        nachbarschaftMapperDriver.withOutput(new IntWritable(3314158),new IntArrayWritable(values6));
//        nachbarschaftMapperDriver.withOutput(new IntWritable(104586), new IntArrayWritable(values7));



//        nachbarschaftMapperDriver.withOutput(new IntWritable(99222),new IntArrayWritable(104586, 100426, 3522900));
//        nachbarschaftMapperDriver.withOutput(new IntWritable(104586),new IntArrayWritable( 99222, 100426, 3522900, 99345));
//        nachbarschaftMapperDriver.withOutput(new IntWritable(100426), new IntArrayWritable(99222, 104586, 3522900, 99345, 3314158));
//        nachbarschaftMapperDriver.withOutput(new IntWritable(3522900),new IntArrayWritable( 99222, 104586, 100426, 99345, 3314158, 104586));
//        nachbarschaftMapperDriver.withOutput(new IntWritable(99345), new IntArrayWritable(104586, 100426, 3522900, 3314158, 104586));
//        nachbarschaftMapperDriver.withOutput(new IntWritable(3314158),new IntArrayWritable( 100426, 3522900, 104586));
//        nachbarschaftMapperDriver.withOutput(new IntWritable(104586), new IntArrayWritable(3522900, 99345, 3314158));



        nachbarschaftMapperDriver.runTest();
    }
}