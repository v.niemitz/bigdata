package ncdc;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Partitioner;
import types.IntPairWritable;

public class NcdcPartitioner<Text, IntPairWritable> extends Partitioner<Text, IntPairWritable> {

    @Override
    public int getPartition(Text key, IntPairWritable value, int numReduceTasks) {
        //latitude + "\t" + longitude + "\t" + year + "\t" + month
        String[] str=key.toString().split("\t");
        int year = Integer.parseInt(str[2]);
        return year % 10;
    }
}
