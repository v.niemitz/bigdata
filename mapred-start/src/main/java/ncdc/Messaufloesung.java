package ncdc;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;
import types.IntPairWritable;

import java.io.IOException;
import java.util.*;

public class Messaufloesung extends Configured implements Tool {

    static RecordParser recordParser = new RecordParser();

    //Ausgabeformat: Text (tabulator-getrennt):Latitude (-90..90) Longitude (-180..180) Jahr (1900..20xx) Monat (1..12) Durchschnittswert

    /**
     * Mapper wandelt Eingabedaten in Tupel ((latitude   longitude   year    month ) (day, station)) um.
     */
    public static class TmpresMapper extends Mapper<LongWritable, Text, Text, IntPairWritable> {

        @Override
        public void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {

            String line = value.toString();
            recordParser.parse(line); //Nutzung der Parser-Klasse.
            int latitude = recordParser.getLat();
            int longitude = recordParser.getLong();
            int month = recordParser.getMonth();
            int year = recordParser.getYear();
            int station = recordParser.getStation();
            int day = recordParser.getDay();
//Schreiben in den Kontext (wie in der Aufgabenstellung spezifiziert: tabulator-getrennt.)
            context.write(new Text(latitude + "\t" + longitude + "\t" + year + "\t" + month), new IntPairWritable(station, day));
        }
    }

    /**
     * Reducer wandelt Eingabetupel ((latitude   longitude   year    month   station), day1, day2, day3,...) um in
     * (latitude    longitude   year    month   station, durchschnittliche Anzahl von Messungen pro Tag) um.
     */
    public static class TmpresReducer extends Reducer<Text, IntPairWritable, Text, DoubleWritable> {

        @Override
        public void reduce(Text key, Iterable<IntPairWritable> values, Context context) throws IOException, InterruptedException {
            HashMap<Integer, List<Integer>> listOfStationAndDaysWithMeasure = new HashMap<>();
//            System.out.println(listOfStationAndDaysWithMeasure);

            //x =Station, y = Messung
            for (IntPairWritable i : values) {
                if (listOfStationAndDaysWithMeasure.containsKey(i.getX())) {//if station is known
//                    System.out.println("old value" + listOfStationAndDaysWithMeasure.get(i.getX()));
                    listOfStationAndDaysWithMeasure.get(i.getX()).add(i.getY()); //add value of y to the list
//                    System.out.println(listOfStationAndDaysWithMeasure.get(i.getX()) + "new value");
                } else { //if station is not known
                    listOfStationAndDaysWithMeasure.put(i.getX(), new ArrayList<>(Arrays.asList(i.getY()))); //add value of x and y to the map
                }
            }
            int sumOfDays = 0;
            double sumOfAverages = 0;
            List<Integer> uniqueDays = new ArrayList<>();
            Iterator it = listOfStationAndDaysWithMeasure.entrySet().iterator();
            while (it.hasNext()) { //iterate over the hashmap
                Map.Entry mapElement = (Map.Entry) it.next();
                List<Integer> listOfValues = (List<Integer>) mapElement.getValue();
                int sumOfEntries = listOfValues.size(); //number of entries for the station
                //filters unique day elements in the list
                for (Integer i : listOfValues) {
                    if (!uniqueDays.contains(i)) {
                        uniqueDays.add(i);
                    }
                }
                sumOfDays = uniqueDays.size(); //number of unique element is size of the unique day elements list
                double averagePerStation = (double) sumOfEntries / sumOfDays; //average per station is the number of entries divided by the number of days
                sumOfAverages += averagePerStation;
            }

            //station is the key so number of stations is the number of keys
            int numberOfStations = listOfStationAndDaysWithMeasure.keySet().size();

            //the average shall be calculated over all stations in the same location
            double avg = 0.0;
            if (numberOfStations > 0) {
                avg = sumOfAverages / numberOfStations;

            }
            context.write(key, new DoubleWritable(avg));
        }

    }


    public int run(String[] args) throws Exception{
        if (args.length != 2) {
            System.err.printf("Usage: %s [generic options] <input> <output>\n", getClass().getSimpleName());
            ToolRunner.printGenericCommandUsage(System.err);
            return -1;
        }
            Job job = Job.getInstance(new Configuration());

            job.setMapOutputKeyClass(Text.class);
            job.setMapOutputValueClass(IntPairWritable.class);

            job.setOutputKeyClass(Text.class);
            job.setOutputValueClass(DoubleWritable.class);

            job.setMapperClass(TmpresMapper.class);
            job.setReducerClass(TmpresReducer.class);
            job.setPartitionerClass(NcdcPartitioner.class);
            job.setNumReduceTasks(10);

            job.setInputFormatClass(TextInputFormat.class);
            job.setOutputFormatClass(TextOutputFormat.class);

            FileInputFormat.setInputPaths(job, new Path(args[0]));
            FileOutputFormat.setOutputPath(job, new Path(args[1]));

            job.setJarByClass(Messaufloesung.class);
        job.setMaxMapAttempts(2);
        job.setMaxReduceAttempts(2);
        return (job.waitForCompletion(true) ? 0 : 1);
        }

    public static void main(String[] args) throws Exception {
        int exitcode = ToolRunner.run(new Messaufloesung(), args);
        System.exit(exitcode);


    }
}