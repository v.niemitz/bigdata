package ncdc;


import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mrunit.mapreduce.MapDriver;
import org.apache.hadoop.mrunit.mapreduce.MapReduceDriver;
import org.apache.hadoop.mrunit.mapreduce.ReduceDriver;
import org.junit.Before;
import org.junit.Test;
import types.IntPairWritable;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class MessaufloesungTest {

    MapDriver<LongWritable, Text, Text, IntPairWritable> mapDriver;
    ReduceDriver<Text, IntPairWritable, Text, DoubleWritable> reduceDriver;
    MapReduceDriver<LongWritable, Text, Text, IntPairWritable, Text, DoubleWritable> mapReduceDriver;

    @Before
    public void setUp() {
        Messaufloesung.TmpresMapper mapper = new Messaufloesung.TmpresMapper();
        Messaufloesung.TmpresReducer reducer = new Messaufloesung.TmpresReducer();
        mapDriver = MapDriver.newMapDriver(mapper);
        reduceDriver = ReduceDriver.newReduceDriver(reducer);
        mapReduceDriver = MapReduceDriver.newMapReduceDriver(mapper, reducer);
    }

    @Test
    public void testMapper() throws IOException {
        mapDriver.withInput(new LongWritable(), new Text(
                "0064010010999991985101703004+70933-008667FM-12+0009ENJA V0203001N01441220001CN0070001N9-00151-00281102161ADDAG12001AY171031GF102991021061001501001001MD1710181+9999MW1771"));
        mapDriver.withInput(new LongWritable(), new Text(
                "0029129070999991901110106004+64333+123451FM?12+000599999V0202701N015919999999N0000001N9-00781"));
        mapDriver.withInput(new LongWritable(), new Text(
                "0064010010999991985010221004+70933-008667FM-12+0009ENJA V0201701N00771007501CN0750001N9+00041-00281102761ADDAG11000AY101031GF107991071051008001999999MD1710071+9999MW1021"));
        mapDriver.withInput(new LongWritable(), new Text(
                "0106010010999991985030300004+70933-008667FM-12+0009ENJA V0201801N00771007501CN0750001N9+00031-00221102651ADDAA106000091AG10000AY101061GF107991071051008001001001MD1710121+9999MW1021OA149901131REMSYN011333   91122"));
        mapDriver.withInput(new LongWritable(), new Text(
                "0144129070999991901030906004+64333-123451FM-12+0009ENJA V0201801N00931220001CN0080001N9+00001-00131099281ADDAG11000AY101031GF103991999999002501999999MD1810061+9999MW1101"));
        mapDriver.withInput(new LongWritable(), new Text(
                "0144129070999991901030906004+64333-123451FM-12+0009ENJA V0201901N00721220001CN0100001N9-00021-00121099231ADDAA106000091AG14000AY101061GF104991999061002501001001KA1120N-00041MD1710061+9999MW1101OA149901081REMSYN017333   21004 91121EQDQ01+000032SCOTLC"));
//                      latitude + "-" + longitude + "-" + year + "-" + month +"-" + station
        mapDriver.withOutput(new Text("70933\t-8667\t1985\t10"), new IntPairWritable(10010, 17));
        mapDriver.withOutput(new Text("64333\t123451\t1901\t11"), new IntPairWritable(129070, 01));
        mapDriver.withOutput(new Text("70933\t-8667\t1985\t1"), new IntPairWritable(10010, 02));
        mapDriver.withOutput(new Text("70933\t-8667\t1985\t3"), new IntPairWritable(10010, 03));
        mapDriver.withOutput(new Text("64333\t-123451\t1901\t3"), new IntPairWritable(129070, 9));
        mapDriver.withOutput(new Text("64333\t-123451\t1901\t3"), new IntPairWritable(129070, 9));
        mapDriver.runTest();

    }

    @Test
    public void testReducer() throws IOException {


        List<IntPairWritable> values = new ArrayList<IntPairWritable>();
        values.add(new IntPairWritable(1, 1));
        values.add(new IntPairWritable(1, 1));
        values.add(new IntPairWritable(1, 1));
        values.add(new IntPairWritable(2, 1));
        values.add(new IntPairWritable(2, 1));
        values.add(new IntPairWritable(2, 1));


        reduceDriver.withInput(new Text("70933\t-8667\t1985\t10"), values);
        reduceDriver.withOutput(new Text("70933\t-8667\t1985\t10"), new DoubleWritable(3.0));
        reduceDriver.runTest();
    }

    @Test
    public void testMapperReducer() throws IOException {

        mapReduceDriver.withInput(new LongWritable(), new Text(
                "0064010010999991985101703004+70933-008667FM-12+0009ENJA V0203001N01441220001CN0070001N9-00151-00281102161ADDAG12001AY171031GF102991021061001501001001MD1710181+9999MW1771"));
        mapReduceDriver.withInput(new LongWritable(), new Text(
                "0029129070999991901110106004+64333+123451FM?12+000599999V0202701N015919999999N0000001N9-00781"));
        mapReduceDriver.withInput(new LongWritable(), new Text(
                "0064010010999991985010221004+70933-008667FM-12+0009ENJA V0201701N00771007501CN0750001N9+00041-00281102761ADDAG11000AY101031GF107991071051008001999999MD1710071+9999MW1021"));
        mapReduceDriver.withInput(new LongWritable(), new Text(
                "0106010010999991985030300004+70933-008667FM-12+0009ENJA V0201801N00771007501CN0750001N9+00031-00221102651ADDAA106000091AG10000AY101061GF107991071051008001001001MD1710121+9999MW1021OA149901131REMSYN011333   91122"));
        mapReduceDriver.withInput(new LongWritable(), new Text(
                "0144129070999991901030906004+64333-123451FM-12+0009ENJA V0201801N00931220001CN0080001N9+00001-00131099281ADDAG11000AY101031GF103991999999002501999999MD1810061+9999MW1101"));
        mapReduceDriver.withInput(new LongWritable(), new Text(
                "0144129070999991901030906004+64333-123451FM-12+0009ENJA V0201901N00721220001CN0100001N9-00021-00121099231ADDAA106000091AG14000AY101061GF104991999061002501001001KA1120N-00041MD1710061+9999MW1101OA149901081REMSYN017333   21004 91121EQDQ01+000032SCOTLC"));
        mapReduceDriver.withInput(new LongWritable(), new Text(
                "0144129070999991901031006004+64333-123451FM-12+0009ENJA V0201901N00721220001CN0100001N9-00021-00121099231ADDAA106000091AG14000AY101061GF104991999061002501001001KA1120N-00041MD1710061+9999MW1101OA149901081REMSYN017333   21004 91121EQDQ01+000032SCOTLC"));



        mapReduceDriver.addOutput(new Text("64333\t-123451\t1901\t3"), new DoubleWritable(1.5));
        mapReduceDriver.addOutput(new Text("64333\t123451\t1901\t11"), new DoubleWritable(1.0));
        mapReduceDriver.addOutput(new Text("70933\t-8667\t1985\t1"), new DoubleWritable(1.0));
        mapReduceDriver.addOutput(new Text("70933\t-8667\t1985\t10"), new DoubleWritable(1.0));
        mapReduceDriver.addOutput(new Text("70933\t-8667\t1985\t3"), new DoubleWritable(1.0));

        mapReduceDriver.runTest();
    }
}
