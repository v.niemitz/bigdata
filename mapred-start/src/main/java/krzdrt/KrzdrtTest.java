package krzdrt;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mrunit.mapreduce.MapDriver;
import org.apache.hadoop.mrunit.mapreduce.ReduceDriver;
import org.junit.Before;
import org.junit.Test;
import types.IntPairWritable;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

public class KrzdrtTest {


    MapDriver<LongWritable, Text, IntPairWritable, IntPairWritable> krzdrtfirstMapDriver;
    ReduceDriver<IntPairWritable, IntPairWritable, IntPairWritable, Text> krzdrtFirstReduceDriver;
    MapDriver<LongWritable, Text, IntPairWritable, IntPairWritable> getKrzdrtSecondMapDriver;
    MapDriver<LongWritable, Text, IntPairWritable, IntPairWritable> krzdrtSecondMapDriverOriginalFile;
    ReduceDriver<IntPairWritable, IntPairWritable, IntPairWritable, Text> secondReduceDriver;
    MapDriver<LongWritable, Text, IntPairWritable, Text> mapThirdPhaseForGeneratedInputDriver;
    MapDriver<LongWritable, Text, IntPairWritable, Text> mapThirdPhaseForOriginalInputDriver;
    ReduceDriver<IntPairWritable, Text, IntPairWritable, Text> thirdReduceDriver;
    MapDriver<LongWritable, Text, IntWritable, IntWritable> fourthMapDriver;
    ReduceDriver<IntWritable, IntWritable, IntWritable, IntWritable> fourthReduceDriver;
//        MultipleInputsMapReduceDriver<Text, IntStringTriple, Text, Text> multipleInputsMapReduceDriver;


    @Before
    public void setUp() {
        Krzdrt.KrzdrzFirstMapper krzdrzFirstMapper = new Krzdrt.KrzdrzFirstMapper();
        Krzdrt.KrzDrtFirstReducer krzDrtFirstReducer = new Krzdrt.KrzDrtFirstReducer();
        Krzdrt.KrzdrzSecondMapper krzdrzSecondMapper = new Krzdrt.KrzdrzSecondMapper();
        Krzdrt.KrzdrtSecondMapperOriginalFile krzdrtSecondMapperOriginalFile = new Krzdrt.KrzdrtSecondMapperOriginalFile();
        Krzdrt.KrzDrtSecondReducer krzDrtSecondReducer = new Krzdrt.KrzDrtSecondReducer();
        Krzdrt.KrzdrtThirdPhaseMapperForGeneratedInput krzdrtThirdPhaseMapperForGeneratedInput = new Krzdrt.KrzdrtThirdPhaseMapperForGeneratedInput();
        Krzdrt.KrzdrtThirdPhaseMapperForOriginalInput krzdrtThirdPhaseMapperForOriginalInput = new Krzdrt.KrzdrtThirdPhaseMapperForOriginalInput();
        Krzdrt.KrzDrtThirdPhaseReducer krzDrtThirdPhaseReducer = new Krzdrt.KrzDrtThirdPhaseReducer();
        Krzdrt.KrzdrtFourthMapper krzdrtFourthMapper = new Krzdrt.KrzdrtFourthMapper();
        Krzdrt.FourthPhaseReducer fourthPhaseReducer = new Krzdrt.FourthPhaseReducer();
        krzdrtfirstMapDriver = MapDriver.newMapDriver(krzdrzFirstMapper);
        krzdrtFirstReduceDriver = ReduceDriver.newReduceDriver(krzDrtFirstReducer);
        getKrzdrtSecondMapDriver = MapDriver.newMapDriver(krzdrzSecondMapper);
        krzdrtSecondMapDriverOriginalFile = MapDriver.newMapDriver(krzdrtSecondMapperOriginalFile);
        secondReduceDriver = ReduceDriver.newReduceDriver(krzDrtSecondReducer);
        mapThirdPhaseForGeneratedInputDriver = MapDriver.newMapDriver(krzdrtThirdPhaseMapperForGeneratedInput);
        mapThirdPhaseForOriginalInputDriver = MapDriver.newMapDriver(krzdrtThirdPhaseMapperForOriginalInput);
        thirdReduceDriver=ReduceDriver.newReduceDriver(krzDrtThirdPhaseReducer);
        fourthMapDriver = MapDriver.newMapDriver(krzdrtFourthMapper);
        fourthReduceDriver = ReduceDriver.newReduceDriver(fourthPhaseReducer);
    }

    @Test
    public void testFirstKrzdrtFirstMapper() throws IOException {

        krzdrtfirstMapDriver.withInput(new LongWritable(), new Text("% sym positive"));
        krzdrtfirstMapDriver.withInput(new LongWritable(), new Text("1000 2"));
        krzdrtfirstMapDriver.withInput(new LongWritable(), new Text("1000 3"));
        krzdrtfirstMapDriver.withInput(new LongWritable(), new Text("2 1000"));
        krzdrtfirstMapDriver.withInput(new LongWritable(), new Text("3 1000"));
        krzdrtfirstMapDriver.withInput(new LongWritable(), new Text("2 4"));
        krzdrtfirstMapDriver.withInput(new LongWritable(), new Text("2 5"));
        krzdrtfirstMapDriver.withInput(new LongWritable(), new Text("3 6"));
        krzdrtfirstMapDriver.withInput(new LongWritable(), new Text("3 7"));

        krzdrtfirstMapDriver.withOutput(new IntPairWritable(2, 1), new IntPairWritable(1000, 2));
        krzdrtfirstMapDriver.withOutput(new IntPairWritable(3, 1), new IntPairWritable(1000, 2));
        krzdrtfirstMapDriver.withOutput(new IntPairWritable(2, 1), new IntPairWritable(4, 2));
        krzdrtfirstMapDriver.withOutput(new IntPairWritable(2, 1), new IntPairWritable(5, 2));
        krzdrtfirstMapDriver.withOutput(new IntPairWritable(3, 1), new IntPairWritable(6, 2));
        krzdrtfirstMapDriver.withOutput(new IntPairWritable(3, 1), new IntPairWritable(7, 2));

        krzdrtfirstMapDriver.runTest();

    }

    /*
    2: 1000,3,4
    3: 6,7,1000
     */
    @Test
    public void testKrzdrtFirstReducer() throws IOException {
        List<IntPairWritable> valuesForTwo = new ArrayList<>(Arrays.asList(new IntPairWritable(1000, 2), new IntPairWritable(3, 2), new IntPairWritable(4, 2)));
        List<IntPairWritable> valuesForThree = new ArrayList<>(Arrays.asList(new IntPairWritable(6, 2), new IntPairWritable(1000, 2), new IntPairWritable(7, 2)));
        List<IntPairWritable> valuesForFour = new ArrayList<>(Arrays.asList(new IntPairWritable(6, 2), new IntPairWritable(7, 2)));

        krzdrtFirstReduceDriver.withInput(new IntPairWritable(2, 1), valuesForTwo);
        krzdrtFirstReduceDriver.withInput(new IntPairWritable(3, 1), valuesForThree);
        krzdrtFirstReduceDriver.withInput(new IntPairWritable(4, 1), valuesForFour);
        krzdrtFirstReduceDriver.withOutput(new IntPairWritable(2, 1), new Text(";3 4 "));
        krzdrtFirstReduceDriver.withOutput(new IntPairWritable(3, 1), new Text(";6 7 "));
        krzdrtFirstReduceDriver.runTest();
    }




 /*
 46	1		83 6092 1536 8051 1154
  */
    @Test
    public void testSecondKrzdrtFirstMapper() throws IOException {

        getKrzdrtSecondMapDriver.withInput(new LongWritable(), new Text("46\t1;83 6092"));
        getKrzdrtSecondMapDriver.withOutput(new IntPairWritable(83, 2), new IntPairWritable(46, 1));
        getKrzdrtSecondMapDriver.withOutput(new IntPairWritable(6092, 2), new IntPairWritable(46, 1));

        getKrzdrtSecondMapDriver.runTest();

    }
//Originalmapper
    @Test
    public void testSecondKrzdrtSecondMapDriverOriginalFile() throws IOException {

        krzdrtSecondMapDriverOriginalFile.withInput(new LongWritable(), new Text("% sym positive"));
        krzdrtSecondMapDriverOriginalFile.withInput(new LongWritable(), new Text("3 6"));
        krzdrtSecondMapDriverOriginalFile.withInput(new LongWritable(), new Text("3 7"));
        krzdrtSecondMapDriverOriginalFile.withInput(new LongWritable(), new Text("5 7"));



        krzdrtSecondMapDriverOriginalFile.withOutput(new IntPairWritable(3, 2), new IntPairWritable(6, 3));
        krzdrtSecondMapDriverOriginalFile.withOutput(new IntPairWritable(3, 2), new IntPairWritable(7, 3));
        krzdrtSecondMapDriverOriginalFile.withOutput(new IntPairWritable(5, 2), new IntPairWritable(7, 3));
        krzdrtSecondMapDriverOriginalFile.runTest();

    }
/*
k:(3,2),v:(2,1), (6,3),(7,3)
k:(4,2), v:(2,1)
k:(5,2),v:(7,3)

out:
(3,2)\t2,1 6,3 7,3
c
 */
    @Test
    public void testKrzdrtSecondReducer() throws IOException {
        List<IntPairWritable> valuesForTwo = new ArrayList<>(Arrays.asList( new IntPairWritable(2, 1), new IntPairWritable(6, 3), new IntPairWritable(7, 3)));
        List<IntPairWritable> valuesForThree = new ArrayList<>(Arrays.asList(  new IntPairWritable(2, 1)));
        List<IntPairWritable> valuesForFour = new ArrayList<>(Arrays.asList( new IntPairWritable(7, 3)));

        secondReduceDriver.withInput(new IntPairWritable(3, 2), valuesForTwo);
        secondReduceDriver.withInput(new IntPairWritable(4, 2), valuesForThree);
        secondReduceDriver.withInput(new IntPairWritable(5, 2), valuesForFour);
        secondReduceDriver.withOutput(new IntPairWritable(3, 2), new Text(";2,1;6,3;7,3;"));
        secondReduceDriver.withOutput(new IntPairWritable(4, 2), new Text(";2,1;"));
        secondReduceDriver.runTest();
    }

    @Test
    public void testThirdPhaseMapperForGeneratedInput() throws IOException {

//        mapThirdPhaseForGeneratedInputDriver.withInput(new LongWritable(), new Text("% sym positive"));
        mapThirdPhaseForGeneratedInputDriver.withInput(new LongWritable(), new Text("43\t2\t;133,1;133,3;"));
        mapThirdPhaseForGeneratedInputDriver.withInput(new LongWritable(), new Text("3\t2\t;2,1;4,3"));



        mapThirdPhaseForGeneratedInputDriver.withOutput(new IntPairWritable(133, 3), new Text("43,2;133,1;"));
        mapThirdPhaseForGeneratedInputDriver.withOutput(new IntPairWritable(4, 3), new Text("3,2;2,1;"));
        mapThirdPhaseForGeneratedInputDriver.runTest();

    }
    @Test
    public void testThirdPhaseMapperForOriginalInput() throws IOException {


        mapThirdPhaseForOriginalInputDriver.withInput(new LongWritable(), new Text("% sym positive"));
        mapThirdPhaseForOriginalInputDriver.withInput(new LongWritable(), new Text("3 6"));
        mapThirdPhaseForOriginalInputDriver.withInput(new LongWritable(), new Text("3 7"));
        mapThirdPhaseForOriginalInputDriver.withInput(new LongWritable(), new Text("5 7"));



        mapThirdPhaseForOriginalInputDriver.withOutput(new IntPairWritable(3, 3),new Text(new IntPairWritable(6,4).toString()));
        mapThirdPhaseForOriginalInputDriver.withOutput(new IntPairWritable(3, 3),new Text(new IntPairWritable(7,4).toString()));
        mapThirdPhaseForOriginalInputDriver.withOutput(new IntPairWritable(5, 3),new Text(new IntPairWritable(7,4).toString()));
        mapThirdPhaseForOriginalInputDriver.runTest();

    }
    @Test
    public void testThirdPhaseReducer() throws IOException {
        List<Text> valuesForTwo = new ArrayList<Text>(Arrays.asList( new Text(new IntPairWritable(6,4).toString()),  new Text(new IntPairWritable(5,2).toString())));
        thirdReduceDriver.withInput(new IntPairWritable(3, 3), valuesForTwo);
        thirdReduceDriver.withOutput(new IntPairWritable(3, 3), new Text(";6,4;5,2;"));
        thirdReduceDriver.runTest();
    }


    @Test
    public void testFourthMapDriver() throws IOException {

//        fourthMapDriver.withInput(new LongWritable(), new Text("% sym positive"));
        fourthMapDriver.withInput(new LongWritable(), new Text("22\t2\t;2116,1;572,1;"));
//        fourthMapDriver.withInput(new LongWritable(), new Text("1000 3"));

        fourthMapDriver.withOutput(new IntWritable(22), new IntWritable(2));
        fourthMapDriver.withOutput(new IntWritable(2116), new IntWritable(1));
        fourthMapDriver.withOutput(new IntWritable(572), new IntWritable(1));

        fourthMapDriver.runTest();

    }



    @Test
    public void testFourthPhaseReducer() throws IOException {
        List<IntWritable> valuesForTwo = new ArrayList<>(Arrays.asList( new IntWritable(2), new IntWritable(3), new IntWritable(1)));

        fourthReduceDriver.withInput(new IntWritable(2), valuesForTwo);
        fourthReduceDriver.withOutput(new IntWritable(2), new IntWritable(1));
        fourthReduceDriver.runTest();
    }

}


