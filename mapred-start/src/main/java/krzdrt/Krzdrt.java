package krzdrt;

import org.apache.commons.collections.list.TreeList;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.MultipleInputs;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;
import types.IntPairWritable;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

public class Krzdrt extends Configured implements Tool {


    public static void main(String[] args) throws Exception {
        ToolRunner.run(new Configuration(), new Krzdrt(), args);
    }
//TODO write partitioner sodass alle j bei einem reducer ankommen

    @Override
    public int run(String[] args) throws Exception {

        // TODO: 08.11.20 die tempor�ren Dateien vor Durchlaufen jedes Jobs l�schen
        int status = -1;
        Configuration configuration = new Configuration();
        Job job = Job.getInstance(configuration, Krzdrt.class.getSimpleName());
        FileInputFormat.setInputPaths(job, new Path(args[0]));
        FileOutputFormat.setOutputPath(job, new Path(args[1]));
        job.setMapperClass(KrzdrzFirstMapper.class);
        job.setJarByClass(Krzdrt.class);
        job.setReducerClass(Krzdrt.KrzDrtFirstReducer.class);
        job.setMapOutputKeyClass(IntPairWritable.class);
        job.setMapOutputValueClass(IntPairWritable.class);
        job.setOutputKeyClass(IntPairWritable.class);
        job.setOutputValueClass(Text.class);
        job.waitForCompletion(true);

        Configuration secondConfiguration = new Configuration();
        Job secondJob = Job.getInstance(secondConfiguration, Krzdrt.class.getSimpleName());
        secondJob.setJarByClass(Krzdrt.class
        );
        MultipleInputs.addInputPath(secondJob, new Path(args[0]), TextInputFormat.class, Krzdrt.KrzdrzSecondMapper.class); //original file reader
        MultipleInputs.addInputPath(secondJob, new Path(args[1]), TextInputFormat.class, Krzdrt.KrzdrzSecondMapper.class);//reads output of first mapper
        secondJob.setMapOutputValueClass(IntPairWritable.class);
        secondJob.setMapOutputKeyClass(IntPairWritable.class);
        secondJob.setReducerClass(KrzDrtSecondReducer.class);
        secondJob.setOutputKeyClass(IntPairWritable.class);
        secondJob.setOutputValueClass(Text.class);
        FileOutputFormat.setOutputPath(secondJob, new Path(args[2]));
        secondJob.waitForCompletion(true);

        Configuration thirdConfiguration = new Configuration();
        Job thirdJob = Job.getInstance(thirdConfiguration, Krzdrt.class.getSimpleName());
        thirdJob.setJarByClass(Krzdrt.class
        );
        MultipleInputs.addInputPath(thirdJob, new Path(args[0]), TextInputFormat.class, Krzdrt.KrzdrtThirdPhaseMapperForOriginalInput.class); //original file reader
        MultipleInputs.addInputPath(thirdJob, new Path(args[2]), TextInputFormat.class, Krzdrt.KrzdrtThirdPhaseMapperForGeneratedInput.class);//reads output of first mapper
        thirdJob.setReducerClass(KrzDrtThirdPhaseReducer.class);
        thirdJob.setMapOutputKeyClass(IntPairWritable.class);
        thirdJob.setMapOutputValueClass(Text.class);
        thirdJob.setOutputKeyClass(IntPairWritable.class);
        thirdJob.setOutputValueClass(Text.class);
        FileOutputFormat.setOutputPath(thirdJob, new Path(args[3]));
        thirdJob.waitForCompletion(true);

        Configuration fourthConfiguration = new Configuration();
        Job fourthJob = Job.getInstance(fourthConfiguration, Krzdrt.class.getSimpleName());
        fourthJob.setJarByClass(Krzdrt.class
        );
        fourthJob.setMapperClass(KrzdrtFourthMapper.class);
        fourthJob.setReducerClass(FourthPhaseReducer.class);
        fourthJob.setOutputKeyClass(IntWritable.class);
        fourthJob.setOutputValueClass(IntWritable.class);
        FileInputFormat.setInputPaths(fourthJob, new Path(args[3]));
        FileOutputFormat.setOutputPath(fourthJob, new Path(args[4]));
        fourthJob.waitForCompletion(true);


        status = fourthJob.isSuccessful() ? 0 : -1;
        // TODO: 08.11.20 remove
        return status;
    }

    public static class KrzdrzFirstMapper extends Mapper<LongWritable, Text, IntPairWritable, IntPairWritable> {
        @Override
        public void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {

            String[] line = value.toString().split(" ");
            if (line[0].matches("[0-9]+")) {
                int i = Integer.parseInt(line[0]);
                int j = Integer.parseInt(line[1]);
                if (i != 1000) //ignore contacts of 100 (we know them later)
                    context.write(new IntPairWritable(i, 1), new IntPairWritable(j, 2));
            }

        }
    }


    public static class KrzDrtFirstReducer extends Reducer<IntPairWritable, IntPairWritable, IntPairWritable, Text> {
        protected void reduce(IntPairWritable key, Iterable<IntPairWritable> values, Context context)
                throws IOException, InterruptedException {
            boolean ok = false;
            LinkedList<IntPairWritable> list = new LinkedList<>();
            for (IntPairWritable v : values) {
                if (v.getX() == 1000) { //only let tupels go further having connection to actor 1000
                    ok = true;
                }
                list.add(new IntPairWritable(v.getX(), v.getY()));
            }

            if (ok) {
                StringBuilder sb = new StringBuilder();
                for (IntPairWritable k : list) {
                    if (k.getX() != 1000)
                        sb.append(k.getX() + " ");

                }
                context.write(new IntPairWritable(key.getX(), key.getY()), new Text(";" + sb.toString()));
            }

        }
    }


    /**
     * combiner is not possible to write with the used algorithm, list is needed as mapper output
     */
    /*
    public static class KrzDrtCombiner extends Reducer<IntPairWritable, IntPairWritable, IntPairWritable, IntPairWritable> {

        protected void reduce(IntPairWritable key, Iterable<IntPairWritable> values, Context context)
                throws IOException, InterruptedException {

            LinkedList<IntPairWritable> list = new LinkedList<>();

            boolean ok = false;
            for (IntPairWritable v : values) {
                //suche nach 1000 in values, wenn vorhanden, schreibe sie weg
                //TODO 1000 hard coden herausnehmens
                if (v.getX() == 1000) {
                    ok = true;
                }
                list.add(new IntPairWritable(v.getX(), v.getY()));
            }
            if (ok) {
                final LinkedList kollegenListe = new LinkedList();

                for (IntPairWritable k : list) {
                    if (k.getX() != 1000) {
                        if (!kollegenListe.contains(k.getX())) {
                            kollegenListe.add(new IntPairWritable(k.getX(), k.getY()));
                            if (!kollegenListe.contains(key.getX() & key.getY())) {
                                kollegenListe.add(new IntPairWritable(key.getX(), key.getY()));
                                context.write(new IntPairWritable(1000, 2), new IntPairWritable(k.getX(), k.getY()));
                            }
                        }
                    }
                }
            }
        }
    }
*/


/* (2,1)\t(3 4)
-> (3,2),(2,1)
-> (4,2),(2,1)
 */

    /**
     * this mapper is used for the produced output file
     * reads in friends from friends from the actor 1000
     * output key: new friend from friend
     * output value: friend who has the phone number
     */
    public static class KrzdrzSecondMapper extends Mapper<LongWritable, Text, IntPairWritable, IntPairWritable> {
        @Override
        public void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
// TODO: 06.11.20 L�sung f�r die urspr�nglichen keys bedenkenls

            String[] line = value.toString().split(";"); //(2,1), (3 4)
            if (line.length > 1) {
                String stringKeyPair = line[0];
//                    substring(1, line[0].length() - 2);//remove ()
                String[] keypair = stringKeyPair.split("\t"); //splits key
                String stringOfValueArray = line[1];//trims leading whitespace
                String[] valueArray = stringOfValueArray.split(" "); //3,4,...
                for (int i = 0; i < valueArray.length; i++) {
                    //write first
                    IntPairWritable outvalue = new IntPairWritable(Integer.parseInt(keypair[0]), Integer.parseInt(keypair[1])); //value is the known phone number
                    IntPairWritable outkey = new IntPairWritable(Integer.parseInt(valueArray[i]), 2); //key is the new phone number
                    context.write(outkey, outvalue); //afterwars, filtering will find if the new phone number has a known phone number as value
                }
            }
        }
    }

    public static class KrzdrtSecondMapperOriginalFile extends Mapper<LongWritable, Text, IntPairWritable, IntPairWritable> {
        @Override
        public void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
// TODO: 06.11.20 L�sung f�r die urspr�nglichen keys bedenken
            String[] line = value.toString().split(" ");
            if (line[0].matches("[0-9]+")) {
                int i = Integer.parseInt(line[0]);
                int j = Integer.parseInt(line[1]);
                if (i != 1000) //ignore contacts of 100 (we know them later)
                    context.write(new IntPairWritable(i, 2), new IntPairWritable(j, 3));
            }
        }
    }

    /*
    k:(3,2),v:(2,1), (6,3),(7,3)
    k:(4,2), v:(2,1)
    k:(5,2),v:(7,3)

    out:
    (3,2)\t2,1 6,3 7,3
    (4,2)\t2,1
     */
    public static class KrzDrtSecondReducer extends Reducer<IntPairWritable, IntPairWritable, IntPairWritable, Text> {
        //
// TODO: 06.11.20  1000 nicht hard coden sondern als Parameter
        protected void reduce(IntPairWritable key, Iterable<IntPairWritable> values, Context context)
                throws IOException, InterruptedException {
            boolean ok = false;
            LinkedList<IntPairWritable> list = new LinkedList<>();
            for (IntPairWritable v : values) {
                if (v.getY() == 1) { //list contains y=1 if person is reachable with one phone call
                    ok = true;
                }
                list.add(new IntPairWritable(v.getX(), v.getY()));
            }

            if (ok) {
                StringBuilder sb = new StringBuilder();

                for (IntPairWritable k : list) {
                    if (k.getX() != 1000) //removes 1000
                        sb.append(k.getX() + "," + k.getY() + ";");
                }
                context.write(new IntPairWritable(key.getX(), key.getY()), new Text(";" + sb.toString()));
            }

        }

    }
/*
in:
(3,2)\t2,1 6,3 7,3
(4,2)\t2,1
out:
k(6,3),v(3,2),(2,1)
k(7,3),v(3,2),(2,1)
no out
 */

    public static class KrzdrtThirdPhaseMapperForGeneratedInput extends Mapper<LongWritable, Text, IntPairWritable, Text> {
        @Override
        public void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {

            String[] line = value.toString().split(";"); //(3,2), (2,1), (6,3)
            if (line.length > 1) {
                StringBuilder sb = new StringBuilder();
                String stringKeyPair;
                String[] splittedKeyPair;
                List<IntPairWritable> listOfKeys = new TreeList();


                for (int i = 0; i < line.length; i++) {

                    String f = line[i].replaceFirst("\t", ",");
                    String s = f.replaceAll("\t", "");
                    if (s.split(",")[1].equals("3")) {
                        stringKeyPair = (s);
                        splittedKeyPair = stringKeyPair.split(",");
                        listOfKeys.add(new IntPairWritable(Integer.parseInt(splittedKeyPair[0]), Integer.parseInt(splittedKeyPair[1])));
                    } else {
                        String addToStringBuilder = s.replaceAll("\\(", "").replaceAll("\\)", "");
                        sb.append(addToStringBuilder + ";");
                    }
                }

                for (IntPairWritable ip : listOfKeys) {
                    context.write(ip, new Text(sb.toString()));

                }

            }
        }
    }

    public static class KrzdrtThirdPhaseMapperForOriginalInput extends Mapper<LongWritable, Text, IntPairWritable, Text> {
        @Override
        public void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
// TODO: 06.11.20 L�sung f�r die urspr�nglichen keys bedenken
            String[] line = value.toString().split(" ");
            if (line[0].matches("[0-9]+")) {
                int i = Integer.parseInt(line[0]);
                int j = Integer.parseInt(line[1]);
                if (i != 1000) //ignore contacts of 100 (we know them later)
                    context.write(new IntPairWritable(i, 3), new Text(new IntPairWritable(j, 4).toString()));
            }
        }
    }

    /*
       k:(3,2),v:(2,1), (6,3),(7,3)
       k:(4,2), v:(2,1)
       k:(5,2),v:(7,3)

       out:
       (3,2)\t2,1 6,3 7,3
       (4,2)\t2,1
        */
    public static class KrzDrtThirdPhaseReducer extends Reducer<IntPairWritable, Text, IntPairWritable, Text> {
        //
        protected void reduce(IntPairWritable key, Iterable<Text> values, Context context)
                throws IOException, InterruptedException {
            boolean ok = false;

//            List<IntPairWritable> intWritableValues = new ArrayList<>();
//
//            for (Text v : values) {
//                String[] split = v.toString().split(";");
//                for (String s : split) {
//                    if (s.length() > 1) {
//                        String[] splitted = s.split("\t");
//                        intWritableValues.add(new IntPairWritable(Integer.parseInt(splitted[0]), Integer.parseInt(splitted[1])));
//                    }
//                }
//
//            }
//
//
//            LinkedList<IntPairWritable> list = new LinkedList<>();
//            for (IntPairWritable v : intWritableValues) {
//                if (v.getY() == 2) { //list contains y=1 if person is reachable with one phone call
//                    ok = true;
//                }
//                list.add(new IntPairWritable(v.getX(), v.getY()));
//            }
//
//            if (ok) {
            StringBuilder sb = new StringBuilder();

//                for (IntPairWritable k : list) {
//                    if (k.getX() != 1000) //removes 1000
//                        sb.append(k.getX() + "," + k.getY() + ";");
//                }
            context.write(new IntPairWritable(key.getX(), key.getY()), new Text(";" + sb.toString()));
        }

//        }

    }

    public static class KrzdrtFourthMapper extends Mapper<LongWritable, Text, IntWritable, IntWritable> {
        @Override
        public void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
            String[] line = value.toString().split(";");
            String[] first = line[0].split("\t");
            context.write(new IntWritable(Integer.parseInt(first[0])), new IntWritable(Integer.parseInt(first[1])));
            for (int c = 1; c < line.length; c++) {
                String s = line[c];
                s = s.replaceAll("\\(", "").replaceAll("\\)", "");
                String[] keyvaluepair = s.split(",");
                int i = Integer.parseInt(keyvaluepair[0]);
                int j = Integer.parseInt(keyvaluepair[1]);
                context.write(new IntWritable(i), new IntWritable(j));


            }
        } //  Reducer sortiert aus, sobald ein Element mehr als einfach vorkommt
    }

    public static class FourthPhaseReducer extends Reducer<IntWritable, IntWritable, IntWritable, IntWritable> {
        //
        protected void reduce(IntWritable key, Iterable<IntWritable> values, Context context)
                throws IOException, InterruptedException {
            IntWritable minNumberOfCalls = new IntWritable(4);
            for (IntWritable i : values) {
                if (i.get() < minNumberOfCalls.get()) {
                    minNumberOfCalls = new IntWritable(i.get());
                }
            }
            context.write(new IntWritable(key.get()), new IntWritable(minNumberOfCalls.get()));

        }

    }
}
