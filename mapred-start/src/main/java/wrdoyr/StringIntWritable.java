package wrdoyr;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.WritableComparable;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;


public class StringIntWritable implements WritableComparable<StringIntWritable>, Cloneable {

    private Text t;
    private int i;

    public StringIntWritable() {
        this.t = new Text();
    }


    public StringIntWritable(Text t, int i) {
        this.i = i;
        this.t = new Text(t);
    }


    @Override
    public void write(DataOutput dataOutput) throws IOException {
        t.write(dataOutput);
        dataOutput.writeInt(i);

    }

    @Override
    public void readFields(DataInput dataInput) throws IOException {
        t.readFields(dataInput);
        i = dataInput.readInt();
    }

    public String getT() {
        return t.toString();
    }

    public Text getTText(){
        return t;
    }

    public int getI() {
        return i;
    }

    public void setT(Text t) {
        this.t = t;
    }

    public void setTstring(String t){
        this.t = new Text(t);
    }

    public void setI(int i) {
        this.i = i;
    }

    @Override
    public String toString() {
        return
                 t + ","
                + i;
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    //  @Override
    public int compareTo(StringIntWritable other) {
        int delta = this.i - other.i;
        if (delta != 0)
            return delta;
        else
            return this.t.compareTo(other.t);
    }

    @Override
    public boolean equals(Object o) {
        if (!(o instanceof StringIntWritable)) return false;
        return compareTo((StringIntWritable) o) == 0;
    }


    public Object clone() {
        return new StringIntWritable(t, i);
    }

}