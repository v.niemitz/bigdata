package wrdoyr;

import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mrunit.mapreduce.MapDriver;
import org.apache.hadoop.mrunit.mapreduce.MultipleInputsMapReduceDriver;
import org.apache.hadoop.mrunit.mapreduce.ReduceDriver;
import org.junit.Before;
import org.junit.Test;
import types.IntDoubleWritable;
import types.IntPairWritable;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

//NOTE: FOR RUNNING THE TESTS PROPERTLY, THE BUFFERSTZE IN CLASS WORDOFTHEYEAR MUST BE SET TO THREE
public class WordOfTheYearTest {

    MapDriver<LongWritable, Text, Text, IntWritable> wordOfTheYearMapperDriver;
    ReduceDriver<Text, IntWritable, Text, IntWritable> wordOfTheYearReducerDriver;
    ReduceDriver<Text, IntWritable, Text, IntWritable> wordOfTheYearCombinerDriver;
    MultipleInputsMapReduceDriver<IntPairWritable, IntDoubleWritable, IntPairWritable, DoubleWritable> multipleInputsMapReduceDriver;
    MapDriver<LongWritable, Text, IntWritable, StringIntWritable> secondWordOfTheYearMapperDriver;
    ReduceDriver<IntWritable, StringIntWritable, IntWritable, Text> secondWordOfTheYearReducerDriver;
    MapDriver<LongWritable, Text, IntWritable, StringIntWritable> thirdWordOfTheYearMapDriver;
    ReduceDriver<IntWritable, StringIntWritable, Text, IntWritable> thirdWordOfTheYearReduceDriver;
    ReduceDriver<IntWritable, StringIntWritable, IntWritable, StringIntWritable> secondWordOfTheYearCombineDriver;


    @Before
    public void setUp() {
        WordOfTheYear.WordOfTheYearMapper wordOfTheYearMapper = new WordOfTheYear.WordOfTheYearMapper();
        WordOfTheYear.WordOfTheYearReducer wordOfTheYearReducer = new WordOfTheYear.WordOfTheYearReducer();
        WordOfTheYear.WordOfTheYearReducer wordOfTheYearCombiner = new WordOfTheYear.WordOfTheYearReducer();
        WordOfTheYear.SecondWordOfTheYearMapper secondWordOfTheYearMapper = new WordOfTheYear.SecondWordOfTheYearMapper();
        WordOfTheYear.SecondWordOfTheYearReducer secondWordOfTheYearReducer = new WordOfTheYear.SecondWordOfTheYearReducer();
        WordOfTheYear.ThirdWordOfTheYearMapper thirdWordOfTheYearMapper = new WordOfTheYear.ThirdWordOfTheYearMapper();
        WordOfTheYear.ThirdWordOfTheYearReducer thirdWordOfTheYearReducer = new WordOfTheYear.ThirdWordOfTheYearReducer();
        WordOfTheYear.SecondWordOfTheYearCombiner secondWordOfTheYearCombiner = new WordOfTheYear.SecondWordOfTheYearCombiner();

        wordOfTheYearMapperDriver = MapDriver.newMapDriver(wordOfTheYearMapper);
        wordOfTheYearReducerDriver = ReduceDriver.newReduceDriver(wordOfTheYearReducer);
        wordOfTheYearCombinerDriver = ReduceDriver.newReduceDriver(wordOfTheYearCombiner);
        multipleInputsMapReduceDriver = MultipleInputsMapReduceDriver.newMultipleInputMapReduceDriver();
        secondWordOfTheYearMapperDriver = MapDriver.newMapDriver(secondWordOfTheYearMapper);
        secondWordOfTheYearReducerDriver = ReduceDriver.newReduceDriver(secondWordOfTheYearReducer);
        thirdWordOfTheYearMapDriver = MapDriver.newMapDriver(thirdWordOfTheYearMapper);
        thirdWordOfTheYearReduceDriver = ReduceDriver.newReduceDriver(thirdWordOfTheYearReducer);
        secondWordOfTheYearCombineDriver = ReduceDriver.newReduceDriver(secondWordOfTheYearCombiner);
    }

    @Test
    public void testWordOfTheYearMapper() throws IOException {

        wordOfTheYearMapperDriver.withInput(new LongWritable(), new Text("453\t1998\t137\t42"));
        wordOfTheYearMapperDriver.withInput(new LongWritable(), new Text("Yasemin\t1998\t137\t42"));
        wordOfTheYearMapperDriver.withInput(new LongWritable(), new Text("Yasemin\t1999\t283\t43"));
        wordOfTheYearMapperDriver.withInput(new LongWritable(), new Text("yasemin\t2000\t299\t73"));
        wordOfTheYearMapperDriver.withInput(new LongWritable(), new Text("Yasemin\t2001\t150\t79"));

        wordOfTheYearMapperDriver.withInput(new LongWritable(), new Text("Yassir\t1969\t8\t8"));
        wordOfTheYearMapperDriver.withInput(new LongWritable(), new Text("yassir\t1970\t20\t13"));
        wordOfTheYearMapperDriver.withInput(new LongWritable(), new Text("Yassir\t1971\t42\t3"));

        wordOfTheYearMapperDriver.withInput(new LongWritable(), new Text("Yorks_NOUN\t2007\t23\t9"));
        wordOfTheYearMapperDriver.withInput(new LongWritable(), new Text("yorks_NOUN\t2008\t24\t12"));
        wordOfTheYearMapperDriver.withInput(new LongWritable(), new Text("Yorks_NOUN\t2009\t29\t16"));
        wordOfTheYearMapperDriver.withInput(new LongWritable(), new Text("Yorks_NOUN\t2010\t34\t7"));
        wordOfTheYearMapperDriver.withInput(new LongWritable(), new Text("Yorks\t1969\t16\t8"));
        wordOfTheYearMapperDriver.withInput(new LongWritable(), new Text("yorks\t1970\t21\t13"));
        wordOfTheYearMapperDriver.withInput(new LongWritable(), new Text("Yorks\t1971\t12\t3"));


        wordOfTheYearMapperDriver.withOutput(new Text("yasemin"), new IntWritable(137));
        wordOfTheYearMapperDriver.withOutput(new Text("yasemin"), new IntWritable(283));
        wordOfTheYearMapperDriver.withOutput(new Text("yasemin"), new IntWritable(299));
        wordOfTheYearMapperDriver.withOutput(new Text("yasemin"), new IntWritable(150));

        wordOfTheYearMapperDriver.withOutput(new Text("yassir"), new IntWritable(8));
        wordOfTheYearMapperDriver.withOutput(new Text("yassir"), new IntWritable(20));
        wordOfTheYearMapperDriver.withOutput(new Text("yassir"), new IntWritable(42));


        wordOfTheYearMapperDriver.withOutput(new Text("yorks"), new IntWritable(23));
        wordOfTheYearMapperDriver.withOutput(new Text("yorks"), new IntWritable(24));
        wordOfTheYearMapperDriver.withOutput(new Text("yorks"), new IntWritable(29));
        wordOfTheYearMapperDriver.withOutput(new Text("yorks"), new IntWritable(34));
        wordOfTheYearMapperDriver.withOutput(new Text("yorks"), new IntWritable(16));
        wordOfTheYearMapperDriver.withOutput(new Text("yorks"), new IntWritable(21));
        wordOfTheYearMapperDriver.withOutput(new Text("yorks"), new IntWritable(12));

        wordOfTheYearMapperDriver.runTest();
    }

    @Test
    public void testWordOfTheYearConbiner() throws IOException {

        List<IntWritable> values2 = new ArrayList<>(Arrays.asList(new IntWritable(137), new IntWritable(283), new IntWritable(299), new IntWritable(150)));
        List<IntWritable> values3 = new ArrayList<>(Arrays.asList(new IntWritable(8), new IntWritable(20), new IntWritable(42)));
        List<IntWritable> values4 = new ArrayList<>(Arrays.asList(new IntWritable(23), new IntWritable(24), new IntWritable(29), new IntWritable(34), new IntWritable(16), new IntWritable(21), new IntWritable(3)));

        wordOfTheYearCombinerDriver.withInput(new Text("yasemin"), values2);
        wordOfTheYearCombinerDriver.withInput(new Text("yassir"), values3);
        wordOfTheYearCombinerDriver.withInput(new Text("yorks"), values4);

        wordOfTheYearCombinerDriver.withOutput(new Text("yasemin"), new IntWritable(869));
        wordOfTheYearCombinerDriver.withOutput(new Text("yassir"), new IntWritable(70));
        wordOfTheYearCombinerDriver.withOutput(new Text("yorks"), new IntWritable(150));

        wordOfTheYearCombinerDriver.runTest();
    }

    @Test
    public void testWordOfTheYearReducer() throws IOException {

        List<IntWritable> values2 = new ArrayList<>(Arrays.asList(new IntWritable(137), new IntWritable(283), new IntWritable(299), new IntWritable(150)));
        List<IntWritable> values3 = new ArrayList<>(Arrays.asList(new IntWritable(8), new IntWritable(20), new IntWritable(42)));
        List<IntWritable> values4 = new ArrayList<>(Arrays.asList(new IntWritable(23), new IntWritable(24), new IntWritable(29), new IntWritable(34), new IntWritable(16), new IntWritable(21), new IntWritable(3)));

        wordOfTheYearReducerDriver.withInput(new Text("yasemin"), values2);
        wordOfTheYearReducerDriver.withInput(new Text("yassir"), values3);
        wordOfTheYearReducerDriver.withInput(new Text("yorks"), values4);

        wordOfTheYearReducerDriver.withOutput(new Text("yasemin"), new IntWritable(869));
        wordOfTheYearReducerDriver.withOutput(new Text("yassir"), new IntWritable(70));
        wordOfTheYearReducerDriver.withOutput(new Text("yorks"), new IntWritable(150));
//        wordOfTheYearReducerDriver.withOutput(new Text(""), new IntWritable());
//        wordOfTheYearReducerDriver.withOutput(new Text(""), new IntWritable());
//        wordOfTheYearReducerDriver.withOutput(new Text(""), new IntWritable());
//        wordOfTheYearReducerDriver.withOutput(new Text(""), new IntWritable());
//        wordOfTheYearReducerDriver.withOutput(new Text(""), new IntWritable());
//        wordOfTheYearReducerDriver.withOutput(new Text(""), new IntWritable());


        wordOfTheYearReducerDriver.runTest();
    }


    /*

Yorks 76
Yorstellens 2

Y, (Yorks,76)
Y, (Yorstellens, 2)

//     */
//    @Test
//    public void testSecondWordOfTheYearMapper() throws IOException {
//// TODO ggf. in der Funktion .toLowercase entfernen, da dies schon im ersten Mapper geschehen ist und hier dann Y in y ersetzen
//        secondWordOfTheYearMapperDriver.withInput(new LongWritable(), new Text("Yasemin\t869"));
//        secondWordOfTheYearMapperDriver.withInput(new LongWritable(), new Text("yassir\t70"));
//        secondWordOfTheYearMapperDriver.withInput(new LongWritable(), new Text("yorks\t150"));
//        //secondWordOfTheYearMapperDriver.withInput(new LongWritable(), new Text());
//
//        secondWordOfTheYearMapperDriver.withOutput(new IntWritable('y'), new StringIntWritable("yasemin", 869));
//        secondWordOfTheYearMapperDriver.withOutput(new IntWritable('y'), new StringIntWritable("yassir", 70));
//        secondWordOfTheYearMapperDriver.withOutput(new IntWritable('y'), new StringIntWritable("yorks", 150));
//        // secondWordOfTheYearMapperDriver.withOutput(new IntWritable(), new StringIntWritable("",));
//
//        secondWordOfTheYearMapperDriver.run();
//    }
//
//    @Test
//    public void testSecondWordOfTheYearReducer() throws IOException {
//
//        List<StringIntWritable> values2 = new ArrayList<>(Arrays.asList(new StringIntWritable("yasemin", 869), new StringIntWritable("yassir", 70)));
//        List<StringIntWritable> values3 = new ArrayList<>(Arrays.asList(new StringIntWritable("applepie", 60), new StringIntWritable("action", 200), new StringIntWritable("art", 900), new StringIntWritable("amused", 650)));
//        List<StringIntWritable> values4 = new ArrayList<>(Arrays.asList(new StringIntWritable("give", 869), new StringIntWritable("gif", 670), new StringIntWritable("gratulation", 333), new StringIntWritable("grade", 75)));
//
//        secondWordOfTheYearReducerDriver.withInput(new IntWritable('y'), values2);
//        secondWordOfTheYearReducerDriver.withInput(new IntWritable('a'), values3);
//        secondWordOfTheYearReducerDriver.withInput(new IntWritable('g'), values4);
//
//
//        secondWordOfTheYearReducerDriver.withOutput(new IntWritable(869), new Text("yasemin"));
//        secondWordOfTheYearReducerDriver.withOutput(new IntWritable(70), new Text("yassir"));
//
//        secondWordOfTheYearReducerDriver.withOutput(new IntWritable(650), new Text("amused"));
//        secondWordOfTheYearReducerDriver.withOutput(new IntWritable(200), new Text("action"));
//        secondWordOfTheYearReducerDriver.withOutput(new IntWritable(900), new Text("art"));
//
//        secondWordOfTheYearReducerDriver.withOutput(new IntWritable(869), new Text("give"));
//        secondWordOfTheYearReducerDriver.withOutput(new IntWritable(670), new Text("gif"));
//        secondWordOfTheYearReducerDriver.withOutput(new IntWritable(333), new Text("gratulation"));
//
//        secondWordOfTheYearReducerDriver.runTest();
//    }
//
//    @Test
//    public void testThirdWordOfTheYearMapper() throws IOException {
//// TODO ggf. in der Funktion .toLowercase entfernen, da dies schon im ersten Mapper geschehen ist und hier dann Y in y ersetzen
//       thirdWordOfTheYearMapDriver.withInput(new LongWritable(), new Text("333\tgratulation"));
//        //secondWordOfTheYearMapperDriver.withInput(new LongWritable(), new Text());
//
//        thirdWordOfTheYearMapDriver.withOutput(new IntWritable(1), new StringIntWritable("gratulation", 333));
//        // secondWordOfTheYearMapperDriver.withOutput(new IntWritable(), new StringIntWritable("",));
//
//        thirdWordOfTheYearMapDriver.runTest();
//    }
//
//
//    @Test
//    public void testThirdWordOfTheYearReducer() throws IOException {
//
//        List<StringIntWritable> values3 = new ArrayList<>(Arrays.asList(new StringIntWritable("applepie", 60), new StringIntWritable("art", 900), new StringIntWritable("yasemin", 869), new StringIntWritable("amused", 650)));
//        List<StringIntWritable> values4 = new ArrayList<>(Arrays.asList(new StringIntWritable("horse", 869), new StringIntWritable("dog", 670), new StringIntWritable("pony", 333), new StringIntWritable("grade", 75)));
//
//        thirdWordOfTheYearReduceDriver.withInput(new IntWritable(1), values3);
//        thirdWordOfTheYearReduceDriver.withInput(new IntWritable(1), values4);
//        thirdWordOfTheYearReduceDriver.withOutput( new Text("amused"),new IntWritable(650));
//        thirdWordOfTheYearReduceDriver.withOutput( new Text("art"), new IntWritable(900));
//        thirdWordOfTheYearReduceDriver.withOutput( new Text("yasemin"),new IntWritable(869));
//
//
//        thirdWordOfTheYearReduceDriver.withOutput( new Text("horse"),new IntWritable(869));
//        thirdWordOfTheYearReduceDriver.withOutput( new Text("dog"),new IntWritable(670));
//        thirdWordOfTheYearReduceDriver.withOutput(new Text("pony"),new IntWritable(333) );
//
//        thirdWordOfTheYearReduceDriver.runTest();
//    }
//
//    @Test
//    public void secondWordOfTheYearCombinerTest() throws IOException {
//
//        List<StringIntWritable> values2 = new ArrayList<>(Arrays.asList(new StringIntWritable("yasemin", 869), new StringIntWritable("yassir", 70)));
//        List<StringIntWritable> values3 = new ArrayList<>(Arrays.asList(new StringIntWritable("applepie", 60), new StringIntWritable("action", 200), new StringIntWritable("art", 900), new StringIntWritable("amused", 650)));
//    //    List<StringIntWritable> values4 = new ArrayList<>(Arrays.asList(new StringIntWritable("give", 869), new StringIntWritable("gif", 670), new StringIntWritable("gratulation", 333), new StringIntWritable("grade", 75)));
//
//        secondWordOfTheYearCombineDriver.withInput(new IntWritable('y'), values2);
//        secondWordOfTheYearCombineDriver.withInput(new IntWritable('a'), values3);
//      //  secondWordOfTheYearCombineDriver.withInput(new IntWritable('g'), values4);
//
//
//        secondWordOfTheYearCombineDriver.withOutput(new IntWritable('y'), new StringIntWritable("yasemin", 869));
//        secondWordOfTheYearCombineDriver.withOutput(new IntWritable('y'), new StringIntWritable("yassir", 70));
//
//        secondWordOfTheYearCombineDriver.withOutput(new IntWritable('a'), new StringIntWritable("amused", 650));
//
//        secondWordOfTheYearCombineDriver.withOutput(new IntWritable('a'), new StringIntWritable("action",200));
//        secondWordOfTheYearCombineDriver.withOutput(new IntWritable('a'), new StringIntWritable("art",900));
//
//
//        secondWordOfTheYearCombineDriver.runTest();
//    }

}
