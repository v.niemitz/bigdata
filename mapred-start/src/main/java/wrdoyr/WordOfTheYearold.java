package wrdoyr;

import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;
import wordcnt.WordCount;

import java.io.IOException;

public class WordOfTheYearold extends Configured implements Tool {

    public static void main(String[] args) throws Exception {
        int exitcode = ToolRunner.run(new WordCount(), args);
        System.exit(exitcode);
    }

    public int run(String[] args) throws Exception {
        if (args.length != 2) {
            System.err.printf("Usage: %s [generic options] <input> <output>\n", getClass().getSimpleName());
            ToolRunner.printGenericCommandUsage(System.err);
            return -1;
        }

        Job job = Job.getInstance(getConf(), getClass().getSimpleName());

        FileInputFormat.addInputPath(job, new Path(args[0]));
        FileOutputFormat.setOutputPath(job, new Path(args[1]));

        job.setJarByClass(WordOfTheYearold.class);
        job.setMapperClass(WordOfTheYearMapper.class);
        job.setCombinerClass(WordOfTheYearReducer.class);
        job.setReducerClass(WordOfTheYearReducer.class);
        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(IntWritable.class);

        return (job.waitForCompletion(true) ? 0 : 1);
    }

    /*
    Abdruckserlaubnis 1969 4 3
Abdruckserlaubnis 1971 2 2
Abdruckserlaubnis 1973 3 3
Abdruckserlaubnis 1974 2 2
Abdruckserlaubnis 1975 2 1
Abdruckserlaubnis 1977 2 2
Abdruckserlaubnis 1978 2 2
Abdruckserlaubnis 1979 1 1
Abdruckserlaubnis 1983 1 1
Abdruckserlaubnis 1984 1 1
Abdruckserlaubnis 1985 1 1
Abdruckserlaubnis 1986 1 1
Abdruckserlaubnis 1988 4 4
     */

    public static class WordOfTheYearMapper extends
            Mapper<LongWritable, Text, Text, IntWritable> {
        public void map(LongWritable key, Text value, Context context)
                throws IOException, InterruptedException {
//TODO Vorkommen eines Wortes mit verschiednen Suffixen eliminieren
            //Wortarten ggf weg schneiden

            String line = value.toString();
//            StringTokenizer itr = new StringTokenizer(value.toString());

        String[] split = line.split("\t");
        Text cleanWord = new Text(split[0].split("_")[0].toLowerCase());
                context.write(new Text(cleanWord), new IntWritable(Integer.parseInt(split[2])));

        }

    }

    public static class WordOfTheYearReducer extends
            Reducer<Text, IntWritable, Text, IntWritable> {
        public void reduce(Text key, Iterable<IntWritable> values,
                           Context context) throws IOException, InterruptedException {
            int sumOccurences = 0;

            for (IntWritable v : values) {
                sumOccurences += v.get();
            }
            context.write(key, new IntWritable(sumOccurences)); //write out the sums

        }
    }



    /*

Yorks 76
Yorstellens 2

Y, (Yorks,76)
Y, (Yorstellens, 2)

     */

    public static class SecondWordOfTheYearMapper extends
            Mapper<LongWritable, Text, IntWritable, StringIntWritable> {
        public void map(LongWritable key, Text value, Context context)
                throws IOException, InterruptedException {

            int firstLetter = 0;
            int number = 0;
            String word = "";

            String line = value.toString();
            String[] split = line.split("\t");

             word = split[0].toLowerCase(); //Stichwort
             firstLetter = split[0].charAt(0); //Anfangsbuchstabe des Stichwortes
             number = Integer.parseInt(split[1]); //H�ufigkeit des Stichwortes (Summe berechnet aus vorhergegangenem Schritt)

//            context.write(new IntWritable(firstLetter), new StringIntWritable(word, number));


        }
    }


    public static class SecondWordOfTheYearReducer extends
            Reducer<IntWritable, StringIntWritable, IntWritable, StringIntWritable> {
        public void reduce(IntWritable key, Iterable<StringIntWritable> values,
                           Context context) throws IOException, InterruptedException {

            int bufferlength = 3;

            //TODO L�nge abwandeln
            int counter = 0;
            StringIntWritable[] buffer = new StringIntWritable[bufferlength];
            int pointer = 0;
//            boolean fullbuffer = false;


            for (StringIntWritable v : values) {
                if (counter < buffer.length) { //initial buffer.size steps
//                    buffer[pointer] = new StringIntWritable(v.getT(),v.getI()); //write value in buffer array
                    pointer++; //pointer shows to next free space
                } else { //set pointer to next lowest space
                    if(pointer >= buffer.length){
                        pointer --;
                    }
                    for(int i = 0; i < buffer.length; i++){
                        if (buffer[i].getI()< buffer[pointer].getI()){
                            pointer = i;
                        }
                    }
                    if (buffer[pointer].getI() <v.getI()) {
//                        buffer[pointer] = new StringIntWritable(v.getT(),v.getI());
                        //write highest known value to buffer array
                    }
                }
                counter++;
            }

            for(int i = 0; i  < buffer.length; i++){
                context.write(key, buffer[i] );
            }

        }
    }


}
