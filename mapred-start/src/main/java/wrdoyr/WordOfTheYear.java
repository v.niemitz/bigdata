package wrdoyr;

import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Partitioner;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

import java.io.IOException;

public class WordOfTheYear extends Configured implements Tool {

    public static int bufferlength = 100;

    public static void main(String[] args) throws Exception {
        int exitcode = ToolRunner.run(new WordOfTheYear(), args);
        System.exit(exitcode);
    }

    public int run(String[] args) throws Exception {

        if (args.length != 2) {
            System.err.printf("Usage: %s [generic options] <input> <output>\n", getClass().getSimpleName());
            ToolRunner.printGenericCommandUsage(System.err);
            return -1;
        }

        Job job = Job.getInstance(getConf(), getClass().getSimpleName());

        FileInputFormat.addInputPath(job, new Path(args[0]));
        FileOutputFormat.setOutputPath(job, new Path(args[1]));

        job.setJarByClass(WordOfTheYear.class);
        job.setMapperClass(WordOfTheYearMapper.class);
        job.setCombinerClass(WordOfTheYearReducer.class);
        job.setReducerClass(WordOfTheYearReducer.class);
        job.setPartitionerClass(WordOfTheYearPartitioner.class);
        job.setNumReduceTasks(26);
        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(IntWritable.class);
        job.waitForCompletion(true);


        Job secondJob = Job.getInstance(getConf(), WordOfTheYear.class.getSimpleName());

        FileInputFormat.addInputPath(secondJob, new Path(args[1]));
        FileOutputFormat.setOutputPath(secondJob, new Path("second"));

        secondJob.setJarByClass(WordOfTheYear.class);
        secondJob.setMapperClass(WordOfTheYear.SecondWordOfTheYearMapper.class);
        secondJob.setReducerClass(WordOfTheYear.SecondWordOfTheYearReducer.class);
        secondJob.setCombinerClass(WordOfTheYear.SecondWordOfTheYearCombiner.class);
        secondJob.setPartitionerClass(SecondWordOfTheYearPartitioner.class);
        secondJob.setNumReduceTasks(26);
        secondJob.setMapOutputValueClass(StringIntWritable.class);
        secondJob.setMapOutputKeyClass(IntWritable.class);

        secondJob.setOutputKeyClass(IntWritable.class);
        secondJob.setOutputValueClass(Text.class);

        secondJob.waitForCompletion(true);
        Job thirdJob = Job.getInstance(getConf(), getClass().getSimpleName());

        FileInputFormat.addInputPath(thirdJob, new Path("second"));
        FileOutputFormat.setOutputPath(thirdJob, new Path("third"));

        thirdJob.setJarByClass(WordOfTheYear.class);
        thirdJob.setMapperClass(WordOfTheYear.ThirdWordOfTheYearMapper.class);
        thirdJob.setCombinerClass(WordOfTheYear.SecondWordOfTheYearCombiner.class);
        thirdJob.setReducerClass(WordOfTheYear.ThirdWordOfTheYearReducer.class);
        thirdJob.setMapOutputKeyClass(IntWritable.class);
        thirdJob.setMapOutputValueClass(StringIntWritable.class);
        thirdJob.setOutputKeyClass(IntWritable.class);
        thirdJob.setOutputValueClass(Text.class);
        thirdJob.waitForCompletion(true);

        return (thirdJob.waitForCompletion(true) ? 0 : 1);
    }


    public static class WordOfTheYearMapper extends
            Mapper<LongWritable, Text, Text, IntWritable> {

        Text cleanWord = new Text();
        IntWritable wordCount = new IntWritable();

        public void map(LongWritable key, Text value, Context context)
                throws IOException, InterruptedException {

            String line = value.toString();
            String[] split = line.split("\t");
            String wordToClean = split[0].split("_")[0];

            if (wordToClean.matches("[a-zA-Z]+")) {

                wordCount.set(Integer.parseInt(split[2]));
                cleanWord.set(wordToClean.toLowerCase()); //eliminate word suffixes
                context.write(cleanWord, wordCount);
                //Text cleanWord = new Text(wordToClean.toLowerCase()); //eliminate word suffixes
                //context.write(new Text(cleanWord), new IntWritable(Integer.parseInt(split[2])));
            }
        }
    }

    //Note: The Combiner it's the same like WordOfTheYearReducer, we take it as combiner!

    /**
     * partitioner works with using first letter
     *
     * @param <Text>
     * @param <IntWritable>
     */
    public static class WordOfTheYearPartitioner<Text, IntWritable> extends Partitioner<Text, IntWritable> {

        @Override
        public int getPartition(Text key, IntWritable value, int numReduceTasks) {
            String line = key.toString();
            String[] split = line.split("\t");

            int ersterBuchstabe = split[0].charAt(0); //Anfangsbuchstabe des Stichwortes
            return ersterBuchstabe % numReduceTasks;
        }
    }

    /**
     * partition by key
     */
    public static class SecondWordOfTheYearPartitioner extends Partitioner<IntWritable, StringIntWritable> {

        @Override
        public int getPartition(IntWritable key, StringIntWritable value, int numReduceTasks) {
            return key.get() % numReduceTasks;
        }
    }

    /**
     * first reducer returns wordcount over all years
     */
    public static class WordOfTheYearReducer extends
            Reducer<Text, IntWritable, Text, IntWritable> {

        IntWritable sumPrevalence = new IntWritable();

        public void reduce(Text key, Iterable<IntWritable> values,
                           Context context) throws IOException, InterruptedException {
            int sumOccurences = 0;

            for (IntWritable v : values) {
                sumOccurences += v.get();
                sumPrevalence.set(sumOccurences);
            }
            context.write(key, sumPrevalence); //write out the sums
            //context.write(key, new IntWritable(sumOccurences)); //write out the sums
        }
    }

    /**
     * returns tupels of first letter of word and word with number of occurences
     */
    public static class SecondWordOfTheYearMapper extends
            Mapper<LongWritable, Text, IntWritable, StringIntWritable> {

        IntWritable firstChar = new IntWritable();
        StringIntWritable wordAndNumber = new StringIntWritable();

        public void map(LongWritable key, Text value, Context context)
                throws IOException, InterruptedException {

            int firstLetter = 0;
            int number = 0;
            String word = "";

            String line = value.toString();
            String[] split = line.split("\t");
            word = split[0].toLowerCase(); //Stichwort
            firstLetter = split[0].charAt(0); //Anfangsbuchstabe des Stichwortes
            firstChar.set(firstLetter);
            number = Integer.parseInt(split[1]); //Häufigkeit des Stichwortes (Summe berechnet aus vorhergegangenem Schritt)

            wordAndNumber.setTstring(word);
            wordAndNumber.setI(number);

            context.write(firstChar, wordAndNumber);
            //context.write(new IntWritable(firstLetter), new StringIntWritable(word, number));
        }
    }

    /**
     * reduces most 100 occuring elements
     */
    public static class SecondWordOfTheYearCombiner extends Reducer<IntWritable, StringIntWritable, IntWritable, StringIntWritable> {

        StringIntWritable wordAndSumprevalence = new StringIntWritable();

        public void reduce(IntWritable key, Iterable<StringIntWritable> values,
                           Context context) throws IOException, InterruptedException {
            //Laenge der Ausgabe final ist die bufferlength
            int counter = 0;

            StringIntWritable[] buffer = new StringIntWritable[bufferlength];
            int pointer = 0;

            for (StringIntWritable v : values) {
                if (counter < buffer.length) { //initial buffer.size steps
//                    buffer[pointer] = new StringIntWritable(v.getT(), v.getI()); //write value in buffer array
                    pointer++; //pointer shows to next free space
                } else { //set pointer to next lowest space
                    if (pointer >= buffer.length) {
                        pointer--;
                    }
                    for (int i = 0; i < buffer.length; i++) {
                        if (buffer[i].getI() < buffer[pointer].getI()) {
                            pointer = i;
                        }
                    }
                    if (buffer[pointer].getI() < v.getI()) {
//                        buffer[pointer] = new StringIntWritable(v.getT(), v.getI());
                        //write highest known value to buffer array
                    }
                }
                counter++;
            }


            for (StringIntWritable stringIntWritable : buffer) {
                if (stringIntWritable != null) {

                    wordAndSumprevalence.setI(stringIntWritable.getI());
                    wordAndSumprevalence.setTstring(stringIntWritable.getT());

                    context.write((key), wordAndSumprevalence);
                    //context.write((key), new StringIntWritable(stringIntWritable.getT(), stringIntWritable.getI()));
                }
            }
        }
    }

    /**
     * returns 100 most occuring numbers
     */
    public static class SecondWordOfTheYearReducer extends
            Reducer<IntWritable, StringIntWritable, IntWritable, Text> {

        IntWritable sumprevalence = new IntWritable();
        Text word = new Text();

        public void reduce(IntWritable key, Iterable<StringIntWritable> values,
                           Context context) throws IOException, InterruptedException {
            //Laenge der Ausgabe final ist die bufferlength
            int counter = 0;
            StringIntWritable[] buffer = new StringIntWritable[bufferlength];
            int pointer = 0;

            for (StringIntWritable v : values) {
                if (counter < buffer.length) { //initial buffer.size steps
//                    buffer[pointer] = new StringIntWritable(v.getT(), v.getI()); //write value in buffer array
                    pointer++; //pointer shows to next free space
                } else { //set pointer to next lowest space
                    if (pointer >= buffer.length) {
                        pointer--;
                    }
                    for (int i = 0; i < buffer.length; i++) {
                        if (buffer[i].getI() < buffer[pointer].getI()) {
                            pointer = i;
                        }
                    }
                    if (buffer[pointer].getI() < v.getI()) {
//                        buffer[pointer] = new StringIntWritable(v.getT(), v.getI());
                        //write highest known value to buffer array
                    }
                }
                counter++;
            }


            for (StringIntWritable stringIntWritable : buffer) {
                if (stringIntWritable != null) {

                    word.set(stringIntWritable.getT());
                    sumprevalence.set(stringIntWritable.getI());

                    context.write(sumprevalence, word);
                    //context.write(new IntWritable(stringIntWritable.getI()), new Text(stringIntWritable.getT()));
                }
            }
        }
    }

    /**
     * returns all elements with the same key so they are processed by te same reducer
     */
    public static class ThirdWordOfTheYearMapper extends
            Mapper<LongWritable, Text, IntWritable, StringIntWritable> {

        IntWritable index = new IntWritable();
        StringIntWritable wordAndCount= new StringIntWritable();

        public void map(LongWritable key, Text value, Context context)
                throws IOException, InterruptedException {

            String line = value.toString();
            String[] split = line.split("\t");

            if (split.length > 1) {
                index.set(1);
                wordAndCount.setTstring(split[1]);
                wordAndCount.setI(Integer.parseInt(split[0]));

                context.write(index , wordAndCount);
                //context.write(new IntWritable(1), new StringIntWritable(split[1], Integer.parseInt(split[0])));
            }
        }
    }


    /**
     * returns 100 most occuring numbers
     */
    public static class ThirdWordOfTheYearReducer extends
            Reducer<IntWritable, StringIntWritable, Text, IntWritable> {

        IntWritable sumprevalence = new IntWritable();
        Text word = new Text();

        public void reduce(IntWritable key, Iterable<StringIntWritable> values,
                           Context context) throws IOException, InterruptedException {
            //Laenge der Ausgabe final ist die bufferlength
            int counter = 0;
            StringIntWritable[] buffer = new StringIntWritable[bufferlength];
            int pointer = 0;

            for (StringIntWritable v : values) {
                if (counter < buffer.length) { //initial buffer.size steps
//                    buffer[pointer] = new StringIntWritable(v.getT(), v.getI()); //write value in buffer array
                    pointer++; //pointer shows to next free space
                } else { //set pointer to next lowest space
                    if (pointer >= buffer.length) {
                        pointer--;
                    }
                    for (int i = 0; i < buffer.length; i++) {
                        if (buffer[i].getI() < buffer[pointer].getI()) {
                            pointer = i;
                        }
                    }
                    if (buffer[pointer].getI() < v.getI()) {
//                        buffer[pointer] = new StringIntWritable(v.getT(), v.getI());
                        //write highest known value to buffer array
                    }
                }
                counter++;
            }


            for (StringIntWritable stringIntWritable : buffer) {
                if (stringIntWritable != null) {

                    word.set(stringIntWritable.getT());
                    sumprevalence.set(stringIntWritable.getI());

                    context.write(word, sumprevalence);
                    //context.write(new IntWritable(stringIntWritable.getI()), new Text(stringIntWritable.getT()));
                }
            }
        }
    }


}