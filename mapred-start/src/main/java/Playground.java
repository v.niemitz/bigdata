
import lsh.LSH;
import lsh.LSHMinHash;
import wrdnbh.UtilFunctions;

import java.util.Arrays;
import java.util.List;

public class Playground {

    public static void main(String[] args) {

        int[] testARray= new int[]{1,2,3,4,5,6};
        int[] secondArray = new int[]{1,2,3,4,5,7};
        int[] thirdArray = new int[]{11,12,14,15,10,3};

int numberOfStages = Math.max(thirdArray.length / 3,1);
        System.out.println(numberOfStages);
        UtilFunctions utilFunctions = new UtilFunctions(numberOfStages, 50, thirdArray.length);
//        int[] outarray = utilFunctions.hashSignature(testARray);
//        int[] secondout = utilFunctions.hashSignature(secondArray);
//        System.out.println( Arrays.toString(outarray));
//        System.out.println( Arrays.toString(secondout
//        ));
        int[] thirdARray = utilFunctions.hashSignature(thirdArray);
        System.out.println(Arrays.toString(thirdARray));


    }


}
