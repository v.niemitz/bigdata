package crkhsh;

import gpstjn.IntStringTriple;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.util.Tool;
import wrdoyr.StringIntWritable;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.*;


public class CrkHsh extends Configured implements Tool {


    private static void swap(String[] input, int a, int b) {
        String tmp = input[a];
        input[a] = input[b];
        input[b] = tmp;
    }

    private static void printArray(String[] input, char delimiter) {
        System.out.print('\n');
        for(int i = 0; i < input.length; i++) {
            System.out.print(input[i]);
        }
    }


    public static  void printAllRecursive(
            int n, String[] elements, char delimiter) {

        if(n == 1) {
            printArray(elements, delimiter);
        } else {
            for(int i = 0; i < n-1; i++) {
                printAllRecursive(n - 1, elements, delimiter);
                if(n % 2 == 0) {
                    swap(elements, i, n-1);
                } else {
                    swap(elements, 0, n-1);
                }
            }
            printAllRecursive(n - 1, elements, delimiter);
        }
    }
    public static <T extends Comparable<T>> void printAllOrdered(
            String[] elements, char delimiter) {

        Arrays.sort(elements);
        boolean hasNext = true;

        while(hasNext) {
            printArray(elements, delimiter);
            int k = 0, l = 0;
            hasNext = false;
            for (int i = elements.length - 1; i > 0; i--) {
                if (elements[i].compareTo(elements[i - 1]) > 0) {
                    k = i - 1;
                    hasNext = true;
                    break;
                }
            }

            for (int i = elements.length - 1; i > k; i--) {
                if (elements[i].compareTo(elements[k]) > 0) {
                    l = i;
                    break;
                }
            }

            swap(elements, k, l);
            Collections.reverse(Arrays.asList(elements).subList(k + 1, elements.length));
        }
    }

    public static void main(String[] args) throws Exception {

        

//        String[] alphabet = new String[]{"A", "B", "C", "D"};
//
//        List<String[]> list = new LinkedList<>();
////        String[] wort = new String[alphabet.length - 1];
////        for (int i = 0; i <= alphabet.length - 2; i++) {
////            wort[i] = alphabet[i];
////        }
//        for (int i = alphabet.length - 2; i >= 0; i--) {
//            String[] wort = new String[alphabet.length - 1];
//            for (int j = 0; j <= alphabet.length - 2; j++) {
//                wort[j] = alphabet[j];
//            }
//            String[] buffer = wort; //alphabet needs to be parametrisierbar
//            for (String s : alphabet) {
//                buffer[i] = s; //an Stelle i wird s rein geschrieben und rotiert �ber das Alphabet
////                System.out.println("buffer"+buffer[i]);
//                String[] res = buffer.clone();
//                list.add(res);
//            }
//        }
//
////        List<String[]> resultList = new LinkedList<>();
//        Map<String, String> hintRainbowTable = new HashMap<>();
//        for (String[] s : list) {
////array to quque q
//            Queue<String> queue = new LinkedList<>(Arrays.asList(s));
//            for (int i = s.length - 1; i >= 0; i--) {
//
//                String pop = queue.poll();
//                queue.add(pop);
////q to array a
//                StringBuilder sb = new StringBuilder();
//
//                for (String q : queue) {
////                    System.out.println(q.toString());
//                    sb.append(q);
//                }
//
//
////                System.out.println(queue.size());
//                hintRainbowTable.put(sb.toString(), hashFunction(sb.toString()));
//                System.out.println(sb.toString());
////                resultList.add((String[]) queue.toArray());
//            }
//
//
//        }
//        System.out.println(hintRainbowTable.size());
//        System.out.println(hintRainbowTable);

    }

    /**
     * the given hash function
     *
     * @param code
     * @return
     * @throws NoSuchAlgorithmException
     * @throws UnsupportedEncodingException
     */
    public static String hashFunction(String code) throws NoSuchAlgorithmException, UnsupportedEncodingException {
        MessageDigest digest = MessageDigest.getInstance("SHA-256");
        byte[] hashedBytes = digest.digest(code.getBytes(StandardCharsets.UTF_8));
        StringBuffer stringBuffer = new StringBuffer();
        for (int i = 0; i < hashedBytes.length; i++)
            stringBuffer
                    .append(Integer.toHexString((hashedBytes[i] & 0xff) + 0x100).substring(1));
        String hash = stringBuffer.toString();
        return hash;
    }


    /**
     * Prints all subsets of the list with k elements.
     * If two or more subsets contain the same elements in a different order, only
     * one of those subsets (that in which the order is like in the original list)
     * is printed.
     */
    public static <T> Map<String, String> printPowerset(List<T> list, int k) {
        if (list == null) throw new NullPointerException();
        if (k < 0 || k > list.size()) throw new IllegalArgumentException();
        if (k == 0) return new HashMap<>();
        List<T> given = new LinkedList<T>(list);
        List<T> chosen = new LinkedList<T>();

        chosen.addAll(printRec(given, chosen, k, 0));

        Map<String, String> hintRainbowTable = new HashMap<>();
        StringBuilder sb = new StringBuilder();

        for (int i = 0; i < chosen.size(); i++) {
            sb.append(chosen.get(i));
            if ((i + 1) % k == 0) {
                hintRainbowTable.put(sb.toString(), "");
                sb.setLength(0);

            }
        }
        return hintRainbowTable;
//        System.out.println(chosen);
    }


    /**
     * Takes all elements (starting at element l) from given, adds them to chosen
     * and then calls itself.
     * If chosen contains k elements, the list is printed, the function returns
     * and the last element is put back in the given list.
     */
    private static <T> List<T> printRec(List<T> given, List<T> chosen, int k, int l) {
        List<T> finalList = new ArrayList<>();

        if (chosen.size() == k) {
            return chosen;
        } else {
            for (int i = l; i < given.size(); i++) {
                T elem = given.get(i);
                given.remove(elem);
                chosen.add(elem);
                finalList.addAll(printRec(given, chosen, k, 0)); // put 0 instead of i and all subsets will
                given.add(i, elem);            // be printed, even if they are permutations
                chosen.remove(elem);
            }
        }
        return finalList;
    }

    public int run(String[] args) throws Exception {

        String letters = args[0].split("-")[1].substring(0, 1); //show which letter is the maximal used in the password
        int passwordLength = Integer.parseInt(args[0].split("-")[1].substring(1, 2));
        Configuration configuration = new Configuration();
        configuration.set("letters", letters); //gives in the letters used for the passwords
        configuration.setInt("passwordLength", passwordLength);//shows password length
        Job job = Job.getInstance(configuration, getClass().getSimpleName());

        FileInputFormat.addInputPath(job, new Path(args[0]));
        FileOutputFormat.setOutputPath(job, new Path(args[1] + "/temp"));

        job.setJarByClass(CrkHsh.class);
        job.setMapperClass(CrkHshMapper.class);

        job.setMapOutputValueClass(IntBooleanTextWritable.class);
        job.setMapOutputKeyClass(Text.class);
//        job.setCombinerClass(crkhsh.CrkHashCombiner.class);
        job.setReducerClass(CrkHsh.CrkHshReducer.class);

//        job.setPartitionerClass(crkhsh.CrkHshPartitioner.class);
        //job.setNumReduceTasks(26);
        job.setOutputKeyClass(IntWritable.class);
        job.setOutputValueClass(Text.class);
        job.waitForCompletion(true);


        Job secondJob = Job.getInstance(getConf(), CrkHsh.class.getSimpleName());

        FileInputFormat.setInputPaths(secondJob, new Path(args[1] + "/temp"));
        FileOutputFormat.setOutputPath(secondJob, new Path(args[1] + "/final"));

        secondJob.setJarByClass(CrkHsh.class);
        secondJob.setMapperClass(CrkHsh.CrkHshMapperSecond.class);
        secondJob.setReducerClass(CrkHsh.CrkHshReducerSecond.class);
        //secondJob.setCombinerClass(CrkHsh.CrkHshCombinerSecond.class);
        //secondJob.setPartitionerClass(CrkHsh.CrkHshPartitionerSecond.class);
        //secondJob.setNumReduceTasks(10);
        secondJob.setMapOutputValueClass(IntStringTriple.class);
        secondJob.setMapOutputKeyClass(Text.class);

        secondJob.setOutputKeyClass(Text.class);
        secondJob.setOutputValueClass(Text.class);

        //secondJob.waitForCompletion(true);
        return (secondJob.waitForCompletion(true) ? 0 : 1);
    }


    public static class CrkHshMapper extends
            Mapper<LongWritable, Text, Text, IntBooleanTextWritable> {

        List alphabet = Arrays.asList("A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"); //wir passen das dann an
        int hintLenght = 3;

        @Override
        protected void setup(Context context) throws IOException, InterruptedException {
            String maximumAlphabet = "D"; //junit test cases
            if (context.getConfiguration().get("letters") != null) { //cluster case
                maximumAlphabet = context.getConfiguration().get("letters");
            }
            hintLenght = (int) maximumAlphabet.charAt(0) - 'A';
            alphabet = alphabet.subList(0, maximumAlphabet.charAt(0) - 'A' + 1); //crate sublist of alphabet list with length of provided maximum letter +1


            Map<String, String> hintRainbowTable = printPowerset(alphabet, hintLenght);

            for (Map.Entry<String, String> entry : hintRainbowTable.entrySet()) {
                try {
                    Text hashfunction = new Text(hashFunction(entry.getKey()));
                    context.write(new Text(hashfunction), new IntBooleanTextWritable(-1, false, new Text(entry.getKey())));
                } catch (NoSuchAlgorithmException e) {
                    e.printStackTrace();
                }
//                System.out.println(entry.getKey() + "/" + entry.getValue());
            }

        }

        public void map(LongWritable key, Text value, Context context)
                throws IOException, InterruptedException {

            String line = value.toString();
            String[] split = line.split(","); //split at comma
            //key is the number at the end of the line, trimmed because there is a linebreak
            int contextKey = (Integer.parseInt(split[split.length - 1].trim()));

            //this is the password, boolean passowrd = true
            context.write(new Text(split[0]), new IntBooleanTextWritable(contextKey, true, new Text("")));

            //key: number of line, values: hashed hints for the password, boolean password= false
            for (int i = 1; i < split.length - 1; i++) {
                context.write(new Text(split[i]), new IntBooleanTextWritable(contextKey, false, new Text("")));
            }

        }
    }


//    public static class CrkHashCombiner extends Reducer<Text, IntWritable, Text, IntWritable> {
//
////TODO
//    }

    /** TODO
     * partitioner works with using first letter
     *
     * @param <Text>
     * @param <IntWritable>
     */
//    public static class WordOfTheYearPartitioner<Text, IntWritable> extends Partitioner<Text, IntWritable> {
//
//        @Override
//        public int getPartition(Text key, IntWritable value, int numReduceTasks) {
////            String line = key.toString();
////            String[] split = line.split("\t");
////
////            int ersterBuchstabe = split[0].charAt(0); //Anfangsbuchstabe des Stichwortes
////            return ersterBuchstabe % numReduceTasks;
//            return 0;
//        }
//    }


    /**
     * first reducer returns wordcount over all years
     */
    public static class CrkHshReducer extends
            Reducer<Text, IntBooleanTextWritable, Text, StringIntWritable> {
        Text possibleLettersText = new Text();
        StringIntWritable sir = new StringIntWritable();

        /**
         * Reduce method looks up values which are marked as hints in the rainbow table and writes their clear representation to output.
         *
         * @param key
         * @param values
         * @param context
         * @throws IOException
         * @throws InterruptedException
         */
        public void reduce(Text key, Iterable<IntBooleanTextWritable> values,
                           Context context) throws IOException, InterruptedException {

            IntWritable lineNumber = new IntWritable();
            List<Text> clearValue = new ArrayList<>();
            Text hashvalue = new Text();
            String possibleLetters = "";
            StringBuilder sb = new StringBuilder();
            List<String> negationList = new ArrayList<>();


            for (IntBooleanTextWritable v : values) { //iterate values for key

                if (v.isPassword()) { //set line number if password is found
                    lineNumber.set(v.getI());
                    hashvalue = key;
                }

                //t is not empty if it is clear value
                if (!v.getT().toString().isEmpty()) {
                    clearValue.add(v.getT());


                    //read in possible letters
                    if (context.getConfiguration().get("letters") != null) {
                        possibleLetters = context.getConfiguration().get("letters");
                    } else {
                        possibleLetters = "D";
                    }
                    //reduce possible letters by hints
                    List<String> impossibleLettersList = new ArrayList();
                    for (int i = 'A'; i <= possibleLetters.charAt(0); i++) {
                        impossibleLettersList.add(Character.toString((char) i));
                        negationList.add(Character.toString((char) i));
                    } //filters out letters based on hints
                    for (char c : clearValue.toString().toUpperCase().toCharArray()) {
                        impossibleLettersList.remove(Character.toString((c)));
                    }//negates result of impossible letters
                    for (String s : impossibleLettersList) {
                        negationList.remove(s);
                    }
                    //result is possible letters
                    sb.append(negationList);
                }
//                out: possible letters for password, hashvalue and line number
                possibleLettersText.set(sb.toString().replace("[", "").replace("]", ""));
                sir.setI(lineNumber.get());
                sir.setT(hashvalue);
                //writes out: possible letters for password, line number, hashvalue


            }
            context.write(possibleLettersText, sir);
        }

    }

    //TODO hat noch keine map methode und datentyp text muss ggf auch noch angepasst werden
    public static class CrkHshMapperSecond extends
            Mapper<LongWritable, Text, Text, IntStringTriple> {
        Text hashedValue = new Text();
        Text combinedWord = new Text();
        IntStringTriple intStringTriple = new IntStringTriple();
        List alphabetList = Arrays.asList("A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z");

        @Override
        protected void setup(Context context) throws IOException, InterruptedException {
            Map<String, String> passwordRainbowtable = new HashMap<String, String>();
            String alphabet = "ABCD";

            String maximumAlphabet = "D"; //junit test cases
            if (context.getConfiguration().get("letters") != null) { //cluster case
                maximumAlphabet = context.getConfiguration().get("letters");
                alphabetList = alphabetList.subList(0, maximumAlphabet.charAt(0) - 'A' + 1); //crate sublist of alphabet list with length of provided maximum letter +1
                alphabet = alphabetList.toString().replace("[", "").replace("]", "");
            }

            //Berechne alle Kombinationen des Alphabets
            /*Quelle vom Algorithmus zur Generierung der Kombinationen des Alphabets:
             * https://www.tutorials.de/threads/alle-moeglichen-kombinationen.222183/
             * Dieser Algorithmus wurde von uns erweitert. Bei Fragen stehen wir gerne zur Verfuegung.
             */


            int passwordLength = 5;
            if (context.getConfiguration().get("passwordLength") != null) { //cluster case
                passwordLength = context.getConfiguration().getInt("passwordLength", 0);
            }

            int[] a = new int[passwordLength];
            intStringTriple.setI(-1);
            String newString = "";


            //Math.pow berechnet die Potenz
            for (int i = 0; i < Math.pow(alphabet.length(), passwordLength); i++) {

                for (int j = 0; j < a.length; j++) {
                    newString += alphabet.charAt(a[j]);

                }

                //System.out.println(newString);
                String hashValue = null;
                try {
                    hashValue = hashFunction(newString);
                } catch (NoSuchAlgorithmException e) {
                    e.printStackTrace();
                }
                //System.out.println(hashValue);

                passwordRainbowtable.put(hashValue, newString);


                List<Character> uniqueCharacters = new ArrayList<>();

                for (char c : newString.toCharArray()) {
                    if (!uniqueCharacters.contains(c)) {
                        uniqueCharacters.add(c);
                    }
                }

//                System.out.println(uniqueCharacters);
                hashedValue.set(hashValue);
                combinedWord.set(uniqueCharacters.toString().replace("[", "").replace("]", ""));
                intStringTriple.setCleanvalue(new Text(newString));
                try {
                    intStringTriple.setHashvalue(new Text(hashFunction(newString)));
                } catch (NoSuchAlgorithmException e) {
                    e.printStackTrace();
                }

                context.write(combinedWord, intStringTriple);

                newString = "";
                int tmp = i;
                int counter = a.length - 1;
                while (tmp != 0) {
                    a[counter] = tmp % alphabet.length();
                    tmp /= alphabet.length();
                    counter--;
                }
            }
        }

        /*
           crkHshReduceDriver.withOutput(new Text("D"), new StringIntWritable(new Text("d4634ff9846abed501225eccb30a61e2c2c20173f2efc2a0ff3246db7e5f8101"), 0));

         */
        @Override
        public void map(LongWritable key, Text value, Context context)
                throws IOException, InterruptedException {

            String line = value.toString();
            String[] split = line.split("\t"); //split at comma
            String[] intStringTripleInput = split[1].split(",");
            context.write(new Text(split[0]), new IntStringTriple(intStringTripleInput[0], "", Integer.parseInt(intStringTripleInput[1])));

        }
    }

    public static class CrkHshReducerSecond extends
            Reducer<Text, IntStringTriple, Text, Text> {

        public void reduce(Text key, Iterable<IntStringTriple> values,
                           Context context) throws IOException, InterruptedException {

            List<IntStringTriple> rainbowTable = new ArrayList<>();
            List<IntStringTriple> originalValues = new ArrayList<>();


            for (IntStringTriple intStringTriple : values) {

                if (intStringTriple.getI() < 0) {
                    rainbowTable.add(intStringTriple);

                } else {
                    originalValues.add(intStringTriple);
                }

                for (IntStringTriple originalTriple : originalValues) {
                    for (IntStringTriple rainbowTableTriple : rainbowTable) {
                        if (originalTriple.getHashvalue().equals(rainbowTableTriple.getHashvalue())) {
                            context.write(new Text(String.valueOf(originalTriple.getI())), new Text(rainbowTableTriple.getCleanvalue()));
                        }
                    }
                }


            }

        }
    }
}