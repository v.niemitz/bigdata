package crkhsh;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.WritableComparable;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

public class IntBooleanTextWritable implements WritableComparable<IntBooleanTextWritable>, Cloneable {

    private boolean password;
    private int i;
    private Text t;

    public IntBooleanTextWritable() {
        this.t = new Text();
    }


    public IntBooleanTextWritable(int i, boolean password, Text t) {
        this.i = i;
        this.password = password;
        this.t = new Text(t);
    }


    @Override
    public void write(DataOutput dataOutput) throws IOException {
        dataOutput.writeInt(i);
        dataOutput.writeBoolean(password);
        t.write(dataOutput);

    }

    @Override
    public void readFields(DataInput dataInput) throws IOException {
        i = dataInput.readInt();
        password = dataInput.readBoolean();
        t.readFields(dataInput);
    }

    public Text getT() {
        return t;
    }

    public void setT(Text t) {
        this.t = new Text(t);
    }

    public boolean isPassword() {
        return password;
    }

    public void setPassword(boolean password) {
        this.password = password;
    }

    public int getI() {
        return i;
    }

    public void setI(int i) {
        this.i = i;
    }


    @Override
    public String toString() {
        return "IntBooleanWritable{" +
                "password=" + password +
                ", i=" + i +
                ", t=" + t +
                '}';
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    public int compareTo(IntBooleanTextWritable other) {
        int delta = this.i - other.i;
        if (delta != 0)
            return delta;
        if(this.t.compareTo(other.t) != 0){
            return this.t.compareTo(other.t);
        }
        else if (this.isPassword() == this.isPassword()) {
            return 0;
        } else {
            return 1;
        }
    }

    @Override
    public boolean equals(Object o) {
        if (!(o instanceof IntBooleanTextWritable)) return false;
        return compareTo((IntBooleanTextWritable) o) == 0;
    }


    public Object clone() {
        return new IntBooleanTextWritable(i, password, t);
    }

}


