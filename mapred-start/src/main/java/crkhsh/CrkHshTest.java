package crkhsh;

import gpstjn.IntStringTriple;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mrunit.mapreduce.MapDriver;
import org.apache.hadoop.mrunit.mapreduce.ReduceDriver;
import org.junit.Before;
import org.junit.Test;
import wrdoyr.StringIntWritable;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

public class CrkHshTest {


    MapDriver<LongWritable, Text, Text, IntBooleanTextWritable> crkHshMapDriver;
    ReduceDriver<Text, IntBooleanTextWritable, Text, StringIntWritable> crkHshReduceDriver;
    MapDriver<LongWritable, Text, Text, IntStringTriple> crkHshMapDriverSecond;


    @Before
    public void setUp() {

        CrkHsh.CrkHshMapper crkHshMapper = new CrkHsh.CrkHshMapper();
        CrkHsh.CrkHshReducer crkHshReducer = new CrkHsh.CrkHshReducer();
        crkHshMapDriver = MapDriver.newMapDriver(crkHshMapper);
        crkHshReduceDriver = ReduceDriver.newReduceDriver(crkHshReducer);
        CrkHsh.CrkHshMapperSecond crkHshMapperSecond = new CrkHsh.CrkHshMapperSecond();
        crkHshMapDriverSecond = MapDriver.newMapDriver(crkHshMapperSecond);

    }

    @Test
    public void testCrkHshMapper() throws IOException {
        crkHshMapDriver.withInput(new LongWritable(), new Text("b3525a45bd28dfbda3d682a026d5150ed1d1ddb26302e11f9b0bc58ae371477e,a065db8768cc17c188fda56d8df431a215f8729c9ffb9129d1aaa8581765d6bd,d4634ff9846abed501225eccb30a61e2c2c20173f2efc2a0ff3246db7e5f8101,000000\n "));
        crkHshMapDriver.withOutput(new Text("b3525a45bd28dfbda3d682a026d5150ed1d1ddb26302e11f9b0bc58ae371477e"), new IntBooleanTextWritable(00000, true, new Text("")));
        crkHshMapDriver.withOutput(new Text("a065db8768cc17c188fda56d8df431a215f8729c9ffb9129d1aaa8581765d6bd"), new IntBooleanTextWritable(00000, false, new Text("")));
        crkHshMapDriver.withOutput(new Text("d4634ff9846abed501225eccb30a61e2c2c20173f2efc2a0ff3246db7e5f8101"), new IntBooleanTextWritable(00000, false, new Text("")));

        crkHshMapDriver.runTest();

    }

    @Test
    public void testCrkHshReducer() throws IOException {
        List<IntBooleanTextWritable> values = Arrays.asList(new IntBooleanTextWritable(00000, true, new Text("")), new IntBooleanTextWritable(0, false, new Text("")),
                new IntBooleanTextWritable(0, false, new Text("BCA") ));

        crkHshReduceDriver.withInput(new Text("d4634ff9846abed501225eccb30a61e2c2c20173f2efc2a0ff3246db7e5f8101"), values);

        crkHshReduceDriver.withOutput(new Text("A,B,C"), new StringIntWritable(new Text("d4634ff9846abed501225eccb30a61e2c2c20173f2efc2a0ff3246db7e5f8101"), 0));
        crkHshReduceDriver.runTest();
    }

    @Test
    public void testCrkHshMapperSecond() throws IOException{
        crkHshMapDriverSecond.withInput(new LongWritable(), new Text("A, B \tfccf7a0431229a74ae38143637cfd0d4d9bb830ff9150009574912f3439ab39c,96"));
            crkHshMapDriverSecond.withOutput(new Text(), new IntStringTriple("", "", 0));
        crkHshMapDriverSecond.runTest();

    }

}
