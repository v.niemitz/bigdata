package types;

import org.apache.hadoop.io.ArrayWritable;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.WritableComparable;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.EOFException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class IntArrayWritable extends ArrayWritable implements WritableComparable<IntArrayWritable>, Cloneable {
    private int[] data;

    public IntArrayWritable() {
        super(IntWritable.class);
        data = new int[0];
    }

    public IntArrayWritable(int[] values) {
        super(IntWritable.class);
        data = values;
    }


    //
    public String printStrings() {
        String strings = "";

        for (int i = 0; i < data.length; i++) {
            strings = strings + " " + data[i];
        }
        return strings;
    }

    @Override
    public IntWritable[] get() {
        return (IntWritable[]) super.get();
    }


    public int[] getIntArray(){
        return this.data;
    }


    public void write(DataOutput out) throws IOException {
        if (data == null) {
            out.write(0);
        } else {
            for (int d : data)
                out.writeInt(d);
        }

    }

    public void readFields(DataInput in) throws IOException {

        List<Integer> integerList = new ArrayList<>();

        Integer readin = 0;

            try {
                while(true) {
                    readin = in.readInt();
                    integerList.add(readin);
                }
            } catch (EOFException e) {
                data = new int[integerList.size()];
                for (int i = 0; i < integerList.size(); i++) {
                    data[i] = integerList.get(i);

            }

        }

//        int length = in.readInt();
//        if (length > 0) {
//            data = new int[length];
//            for (int i = 0; i < length; ++i)
//                data[i] = in.readInt();
//        }
    }

    public int get(int i) {
        return data[i];
    }

    public void set(int i, int val) {
        data[i] = val;
    }

    @Override
    public int compareTo(IntArrayWritable o) {
        int s, i = 0;
        do {
            s = this.data[i] - o.data[i];
        } while ((++i < data.length) && (s == 0));
        return s;
    }

    public boolean equals(Object o) {
        if (!(o instanceof IntArrayWritable)) return false;
        return compareTo((IntArrayWritable) o) == 0;
    }

    public int hashCode() {
        return Arrays.hashCode(data);
    }

    public String toString() {
        return Arrays.toString(data);
    }

    public IntArrayWritable clone() {
        IntWritable[] temp = new IntWritable[data.length];
        return new IntArrayWritable(data);
    }

    public void set(int[] inputArray) {
        data = inputArray.clone();
    }
}
//@Override
//    public String toStringZwei() {
//        IntWritable[] values = get();
//        return values[0].toString() + ", " + values[1].toString();
//    }
//   public final void set(int value){}

// public IntArrayWritable() {
//        data = new int[0];
//    }

//    public IntArrayWritable(int... values) {
//        data = values.clone();
//    }
//    @Override
//    public String toString() {
//        StringBuilder sb = new StringBuilder("[");
//
//        for (String s : super.toStrings())
//        {
//            sb.append(s).append(" ");
//        }
//
//        sb.append("]");
//        return sb.toString();
//    }

//@Override
//public String toString() {
//    StringBuilder sb = new StringBuilder();
//    for (String s : super.toStrings())
//    {
//        sb.append(s).append(" ");
//    }
//    return sb.toString();
//}