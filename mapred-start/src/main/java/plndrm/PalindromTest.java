package plndrm;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mrunit.mapreduce.MapDriver;
import org.apache.hadoop.mrunit.mapreduce.MapReduceDriver;
import org.apache.hadoop.mrunit.mapreduce.ReduceDriver;
import org.junit.Before;
import org.junit.Test;
import types.IntPairWritable;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

public class PalindromTest {

        MapDriver<Object, Text, Text, IntPairWritable> mapDriver;
        ReduceDriver<Text, IntPairWritable, Text, Text> reduceDriver;
        MapReduceDriver<Object, Text, Text, IntPairWritable, Text, Text> mapReduceDriver;

        @Before
        public void setUp() {

            Palindrom.TokenizerMapper mapper = new Palindrom.TokenizerMapper();
            Palindrom.IntSumReducer reducer = new Palindrom.IntSumReducer();

            mapDriver = MapDriver.newMapDriver(mapper);
            reduceDriver = ReduceDriver.newReduceDriver(reducer);
            mapReduceDriver = MapReduceDriver.newMapReduceDriver(mapper, reducer);
        }

        @Test
        public void testMapper() throws IOException {

            mapDriver.withInput(new LongWritable(), new Text(
                    "seil seil lies s3432 sa u"));


            mapDriver.withOutput(new Text("seil+lies"), new IntPairWritable(1,0));
            mapDriver.withOutput(new Text("seil+lies"), new IntPairWritable(1,0));
            mapDriver.withOutput(new Text("seil+lies"), new IntPairWritable(0,1));

            mapDriver.runTest();

        }

        @Test
        public void testReducer() throws IOException {

            reduceDriver.withInput(new Text("seil+lies"), new ArrayList<>(Arrays.asList(new IntPairWritable(1,0), new IntPairWritable(1,0), new IntPairWritable(0,1))));


            reduceDriver.withOutput(new Text("seil"), new Text("2:1"));
            reduceDriver.runTest();
        }

        @Test
        public void testMapperReducer() throws IOException {

            mapReduceDriver.withInput(new LongWritable(), new Text("seil lies seil lies"));

            mapReduceDriver.withOutput(new Text("lies"), new Text("2:2"));

            mapReduceDriver.runTest();
        }

    @Test
    public void testMapperReduceLongText() throws IOException {

        mapReduceDriver.withInput(new LongWritable(), new Text("seil seil lies s3432 sa u"));
        mapReduceDriver.withOutput(new Text("seil"), new Text("2:1"));

        mapReduceDriver.runTest();
    }

}
