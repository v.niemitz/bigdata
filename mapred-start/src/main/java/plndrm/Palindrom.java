package plndrm;

import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;
import types.IntPairWritable;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

public class Palindrom extends Configured implements Tool {


    public static class TokenizerMapper extends
            Mapper<Object, Text, Text, IntPairWritable> {

        private Text word = new Text();

        public void map(Object key, Text value, Context context)
                throws IOException, InterruptedException {
            StringTokenizer itr = new StringTokenizer(value.toString());
            //list marks first words of a pair
            List<String> firstWords = new ArrayList<>();
            String toWrite = "";
            String reverse = "";

            int firstWord = 0;
            int secondWord = 0;

            while (itr.hasMoreTokens()) {
                word.set(itr.nextToken());
                String wordAsString = word.toString().toLowerCase();
                //check if string is too short

                if (wordAsString.matches(".*\\d.*")) {
                    continue;
                }

//                 wordAsString = wordAsString.replaceAll("[,;\.\?!'�`_]" ,"");
                wordAsString = wordAsString.replaceAll("[,|;|.|?|!|'|\"|�|`|_]", "");
                wordAsString = wordAsString.replace("-", "");
//                wordAsString = wordAsString.replace("[a-zA-Z]", "");
                //no words shorter than 3
                if (wordAsString.length() < 3) {
                    continue;
                }
                word = new Text(wordAsString);
                reverse = new StringBuilder(word.toString()).reverse().toString();
                //check if it is first word
                if (firstWords.contains(word)) {
                    toWrite = word.toString() + "+" + reverse;
                    firstWord = 1;
                    secondWord = 0;
                    //check if word is reversed first word
                } else if (firstWords.contains(reverse)) {
                    toWrite = reverse + "+" + word.toString();
                    firstWord = 0;
                    secondWord = 1;
                    //not the word nor the reversed word are in the list
                } else {
                    toWrite = word.toString() + "+" + reverse;
                    firstWords.add(word.toString());
                    firstWord = 1;
                    secondWord = 0;
                }
//                System.out.println(new Text(toWrite).toString() + " " + new IntPairWritable(firstWord, secondWord).toString());
                context.write(new Text(toWrite), new IntPairWritable(firstWord, secondWord));
            }
        }
    }

    public static class IntSumReducer extends
            Reducer<Text, IntPairWritable, Text, Text> {
        private Text result = new Text("");

        public void reduce(Text key, Iterable<IntPairWritable> values,
                           Context context) throws IOException, InterruptedException {
            int sumX = 0;
            int sumY = 0;
            String firstWord = "";
            //Sum up number of first words and number of second words
            for (IntPairWritable val : values) {
                sumX += val.getX();
                sumY += val.getY();
            }
            //both words must be at least one time used
            if (sumX > 0 && sumY > 0) {
                //more of ten word stands before the other
                if (sumX > sumY) {
                    result.set(sumX + ":" + sumY);
                    firstWord =

                            key.toString().split("\\+")[0];
                } else {
                    result.set(sumY + ":" + sumX);
                    firstWord = key.toString().split("\\+")[1];
                }

                System.out.println(firstWord + result);
                context.write(new Text(firstWord), result);
            }
        }
    }

    public int run(String[] args) throws Exception {
        if (args.length != 2) {
            System.err.printf("Usage: %s [generic options] <input> <output>\n", getClass().getSimpleName());
            ToolRunner.printGenericCommandUsage(System.err);
            return -1;
        }

        Job job = Job.getInstance(getConf(), getClass().getSimpleName());

        FileInputFormat.addInputPath(job, new Path(args[0]));
        FileOutputFormat.setOutputPath(job, new Path(args[1]));

        job.setJarByClass(Palindrom.class);
        job.setMapperClass(Palindrom.TokenizerMapper.class);
        job.setReducerClass(Palindrom.IntSumReducer.class);
        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(IntPairWritable.class);


        return (job.waitForCompletion(true) ? 0 : 1);
    }

    public static void main(String[] args) throws Exception {
        int exitcode = ToolRunner.run(new Palindrom(), args);
        System.exit(exitcode);
    }

}
