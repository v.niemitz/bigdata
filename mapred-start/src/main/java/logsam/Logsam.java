package logsam;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;
import wrdoyr.StringIntWritable;

import java.io.IOException;
import java.util.Map;
import java.util.TreeMap;

public class Logsam extends Configured implements Tool {

    public static void main(String[] args) throws Exception {
        int exitcode = ToolRunner.run(new Logsam(), args);
        System.exit(exitcode);
    }

    public int run(String[] args) throws Exception {
        // Configuration processed by ToolRunner
        Configuration conf = new Configuration();


        conf.setDouble("percentage", Double.parseDouble(args[3]));

        Job job = Job.getInstance(conf, getClass().getSimpleName());
        job.setJarByClass(Logsam.class);

        job.setMapperClass(Logsam.LogsamMapper.class);
        job.setCombinerClass(LogsamCombiner.class);
        job.setReducerClass(Logsam.LogsamReducer.class);
//        job.setReducerClass(Logsam.Identityreducer.class);
        job.setMapOutputKeyClass(Text.class);
        job.setMapOutputValueClass(StringIntWritable.class);
        job.setNumReduceTasks(5);

        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(DoubleWritable.class);
//        job.setOutputValueClass(Text.class);
        job.setInputFormatClass(TextInputFormat.class);
        job.setOutputFormatClass(TextOutputFormat.class);

        FileInputFormat.setInputPaths(job, new Path(args[0]));
        FileOutputFormat.setOutputPath(job, new Path(args[1]));

        job.waitForCompletion(true);

        Configuration secondConfiguration = new Configuration();
        Job secondJob = Job.getInstance(secondConfiguration, Logsam.class.getSimpleName());
        secondJob.setJarByClass(Logsam.class);
        FileInputFormat.setInputPaths(secondJob, new Path(args[1]));
        FileOutputFormat.setOutputPath(secondJob, new Path(args[2]));
        secondJob.setJarByClass(Logsam.class);
        secondJob.setMapOutputValueClass(DoubleWritable.class);
        secondJob.setMapOutputKeyClass(IntWritable.class);
        secondJob.setMapperClass(Logsam.LogsamAverageMapper.class);
        secondJob.setReducerClass(Logsam.LogsamAverageReducer.class);
        secondJob.setOutputKeyClass(Text.class);
        secondJob.setOutputValueClass(DoubleWritable.class);


        int result = secondJob.waitForCompletion(true) ? 0 : 1;
        // remove empty output path
//        FileSystem.get(conf).delete(new Path(args[1]), true);

        return result;
    }

    /**
     * output: userhash, (productid, 1)- tuples
     */
    public static class LogsamMapper
            extends Mapper<LongWritable, Text, Text, StringIntWritable> {

        /*
        16-01-01 00:15 faf4b9be eb439cab
         */

        Text userHash = new Text();
        Text productId = new Text();


        @Override
        public void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
            String[] split = value.toString().split(" ");

            userHash.set(split[2]);  //reads out user hash
            productId.set(split[3]); //reads out product id

            context.write(userHash, new StringIntWritable(productId, 1));           //writes out all output tuples
        }
    }

    /**
     * sums up occurences of productid: (userhash, (productid, number of calls))
     */
    public static class LogsamCombiner extends Reducer<Text, StringIntWritable, Text, StringIntWritable> {

        public void reduce(Text key, Iterable<StringIntWritable> values,
                           Context context) throws IOException, InterruptedException {

            TreeMap<Text, Integer> textIntegerTreeMap = new TreeMap<>();
            for (StringIntWritable t : values) {
                if (textIntegerTreeMap.containsKey(t.getTText())) { //key is non unique
//sum
                    textIntegerTreeMap.put(new Text(t.getTText()), textIntegerTreeMap.get(t.getTText()) + t.getI());

                } else {//key first time
                    textIntegerTreeMap.put(new Text(t.getTText()), t.getI());

                }
            }

            for (Map.Entry<Text, Integer> mapEntry : textIntegerTreeMap.entrySet()) {
                context.write(key, new StringIntWritable(mapEntry.getKey(), mapEntry.getValue()));
            }

        }


    }

    /**
     * used for testing reasons only
     */
    public static class Identityreducer
            extends Reducer<Text, StringIntWritable, Text, Text> {
        public void reduce(Text key, Iterable<StringIntWritable> values,
                           Context context) throws IOException, InterruptedException {
//counts number of inputs
            for (StringIntWritable t : values) {
                context.write(key, new Text(t.toString()));
            }
        }
    }

    /**
     * output: (userhash, percentage of unique calls per user)
     */
    public static class LogsamReducer
            extends Reducer<Text, StringIntWritable, Text, DoubleWritable> {
        DoubleWritable percentageOfUserNonUniqueCalls = new DoubleWritable();

        public void reduce(Text key, Iterable<StringIntWritable> values,
                           Context context) throws IOException, InterruptedException {
            double percentage = context.getConfiguration().getDouble("percentage", 1.0);
            TreeMap<StringIntWritable, Integer> textIntegerTreeMap = new TreeMap<>();
            int countNonUniquevalues = 0; //counter for non unique values
            int countAllvalues = 0;//counts number of inputs
            for (StringIntWritable t : values) {
                if (textIntegerTreeMap.containsKey(t)) { //key is non unique
                    if (textIntegerTreeMap.get(t) == 1) { //breaking point for non unique value if contained once and minimum once additional occurence appears
                        countNonUniquevalues++;
                    }

                    textIntegerTreeMap.put(new StringIntWritable(t.getTText(), t.getI()), textIntegerTreeMap.get(t) + t.getI());

                } else {//key first time
                    textIntegerTreeMap.put(new StringIntWritable(t.getTText(), t.getI()), 1);
                    if (t.getI() > 1) {
                        countNonUniquevalues++; //if directly occuring more often then once, breaking point for non unique value
                    }
                    countAllvalues++; //sums up with values in t

                }
            }

            percentageOfUserNonUniqueCalls.set((double) countNonUniquevalues / countAllvalues);
            double random = Math.random();
            if (random < percentage) //filter 10 percent of output
                context.write(key, percentageOfUserNonUniqueCalls); //write out percentage per user
        }


    }

    /**
     * out: (1, average per user) -> goes to one reducer
     */
    public static class LogsamAverageMapper
            extends Mapper<LongWritable, Text, IntWritable, DoubleWritable> {

        DoubleWritable percentagePerUser = new DoubleWritable();
        IntWritable keyout = new IntWritable(1);

        @Override
        public void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
            String[] split = value.toString().split("\t");

            percentagePerUser.set(Double.parseDouble(split[1]));
            context.write(keyout, percentagePerUser); //read in percentage per user
        }
    }

    /*
    calculates average percentage for values
     */
    public static class LogsamSecondCombiner extends Reducer<Text, DoubleWritable, Text, DoubleWritable> {
        protected void reduce(Text key, Iterable<DoubleWritable> values, Context context) throws IOException, InterruptedException {
            Text avgPercentage = new Text("average percentage: ");
            DoubleWritable outputPercentage = new DoubleWritable();

            double temp = 0.0;
            int counter = 0;
            for (DoubleWritable v : values) {
                temp += v.get();
                counter++;
            }

            if (counter > 0 && temp > 0.0)
                outputPercentage.set(temp / counter);//divide double values by number of values
            context.write(key, outputPercentage); //calculates global average
        }
    }

    /*
    out: describing text (because I did not have an idea for a useful key) and global average user percentage of non-unique calls
     */
    public static class LogsamAverageReducer
            extends Reducer<IntWritable, DoubleWritable, Text, DoubleWritable> {

        Text avgPercentage = new Text("average percentage: ");
        DoubleWritable outputPercentage = new DoubleWritable();

        protected void reduce(IntWritable key, Iterable<DoubleWritable> values, Context context) throws IOException, InterruptedException {
            double temp = 0.0;
            int counter = 0;
            for (DoubleWritable d : values) { //sums up double values
                temp += d.get();
                counter++;
            }
            if (counter > 0 && temp > 0.0)
                outputPercentage.set(temp / counter);//divide double values by number of values
            context.write(avgPercentage, outputPercentage); //calculates global average
        }
    }


}
