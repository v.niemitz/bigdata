package logsam;

import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mrunit.mapreduce.MapDriver;
import org.apache.hadoop.mrunit.mapreduce.ReduceDriver;
import org.junit.Before;
import org.junit.Test;
import wrdoyr.StringIntWritable;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

public class LogsamTest {

    MapDriver<LongWritable, Text, Text, StringIntWritable> mapDriver;
    ReduceDriver<Text, StringIntWritable, Text, DoubleWritable> reduceDriver;
    ReduceDriver<Text, StringIntWritable, Text, Text> identityreduceDriver;
    //    MapReduceDriver<Object, Text, Text, IntPairWritable, Text, Text> mapReduceDriver;
    MapDriver<LongWritable, Text, IntWritable, DoubleWritable> averageMapperDriver;
    ReduceDriver<IntWritable, DoubleWritable, Text, DoubleWritable> logsamAverageReduceDriver;
    ReduceDriver<Text, StringIntWritable, Text, StringIntWritable> logsamCombineDriver;
    ReduceDriver<Text, DoubleWritable, Text, DoubleWritable> logsamSecondCombineDriver;

    @Before
    public void setUp() {

        Logsam.LogsamMapper mapper = new Logsam.LogsamMapper();
        Logsam.LogsamReducer reducer = new Logsam.LogsamReducer();
        Logsam.Identityreducer identityreducer = new Logsam.Identityreducer();
        Logsam.LogsamAverageMapper averageMapper = new Logsam.LogsamAverageMapper();
        Logsam.LogsamAverageReducer logsamAverageReducer = new Logsam.LogsamAverageReducer();
        Logsam.LogsamCombiner logsamCombiner = new Logsam.LogsamCombiner();
        Logsam.LogsamSecondCombiner logsamSecondCombiner = new Logsam.LogsamSecondCombiner();

        mapDriver = MapDriver.newMapDriver(mapper);
        reduceDriver = ReduceDriver.newReduceDriver(reducer);
        identityreduceDriver = ReduceDriver.newReduceDriver(identityreducer);
        averageMapperDriver = MapDriver.newMapDriver(averageMapper);
        logsamAverageReduceDriver = ReduceDriver.newReduceDriver(logsamAverageReducer);
        logsamCombineDriver = ReduceDriver.newReduceDriver(logsamCombiner);
        logsamSecondCombineDriver = ReduceDriver.newReduceDriver(logsamSecondCombiner);

    }

    /*
    Hier sollten 0-2 Ausgaben heraus kommen.
     */
    @Test
    public void testMapper() throws IOException {

        mapDriver.withInput(new LongWritable(), new Text("16-01-01 07:52 dad5c39a 6e004e25"));
        mapDriver.withInput(new LongWritable(), new Text("16-01-01 07:52 fd8b0248 469b5c29"));
        mapDriver.withInput(new LongWritable(), new Text("16-01-01 07:52 dad5c39a 6e004e25"));
        mapDriver.withInput(new LongWritable(), new Text("16-01-01 07:50 4d847c12 63e6cc5b"));
        mapDriver.withInput(new LongWritable(), new Text("16-01-01 07:48 f932d83 ab33f233"));
        mapDriver.withInput(new LongWritable(), new Text("16-01-01 07:45 9334b158 3142a336"));
        mapDriver.withInput(new LongWritable(), new Text("16-01-01 07:44 d88ab6c9 2ae5bcb6"));
        mapDriver.withInput(new LongWritable(), new Text("16-01-01 07:42 56415a02 3d31e22e"));
        mapDriver.withInput(new LongWritable(), new Text("16-01-01 07:40 2dd1d472 f50e8801"));
        mapDriver.withInput(new LongWritable(), new Text("16-01-01 07:38 12bce409 8ddf88eb"));


        mapDriver.withOutput(new Text("dad5c39a"), new StringIntWritable(new Text("6e004e25"), 1));
        mapDriver.withOutput(new Text("fd8b0248"), new StringIntWritable(new Text("469b5c29"), 1));
        mapDriver.withOutput(new Text("dad5c39a"), new StringIntWritable(new Text("6e004e25"), 1));
        mapDriver.withOutput(new Text("4d847c12"), new StringIntWritable(new Text("63e6cc5b"), 1));
        mapDriver.withOutput(new Text("f932d83"), new StringIntWritable(new Text("ab33f233"), 1));
        mapDriver.withOutput(new Text("9334b158"), new StringIntWritable(new Text("3142a336"), 1));
        mapDriver.withOutput(new Text("d88ab6c9"), new StringIntWritable(new Text("2ae5bcb6"), 1));
        mapDriver.withOutput(new Text("56415a02"), new StringIntWritable(new Text("3d31e22e"), 1));
        mapDriver.withOutput(new Text("2dd1d472"), new StringIntWritable(new Text("f50e8801"), 1));
        mapDriver.withOutput(new Text("12bce409"), new StringIntWritable(new Text("8ddf88eb"), 1));


        mapDriver.runTest();

    }

    @Test
    public void testReducer() throws IOException {

        reduceDriver.withInput(new Text("dad5c39a"), new ArrayList<>(Arrays.asList(new StringIntWritable(new Text("469b5c29"), 1), new StringIntWritable(new Text("469b5c29"), 1),
                new StringIntWritable(new Text("f50e8801"), 1))));
        reduceDriver.withInput(new Text("56415a02"), new ArrayList<>(Arrays.asList(new StringIntWritable(new Text("469b5c29"), 1), new StringIntWritable(new Text("469b5c29"), 1),
                new StringIntWritable(new Text("469b5c29"), 1),
                new StringIntWritable(new Text("469b5c29"), 1))));
        reduceDriver.withInput(new Text("469b5c29"), new ArrayList<>(Arrays.asList(new StringIntWritable(new Text("469b5c29"), 3),
                new StringIntWritable(new Text("56415a02"), 1), new StringIntWritable(new Text("f50e8801"), 1))));


        reduceDriver.withOutput(new Text("dad5c39a"), new DoubleWritable(0.5));
        reduceDriver.withOutput(new Text("56415a02"), new DoubleWritable(1.0));
        reduceDriver.withOutput(new Text("469b5c29"), new DoubleWritable((double) 1 / 3));

        reduceDriver.runTest();
    }

    /*
    used to lookup matter output
     */
    @Test
    public void testIdentityReducer() throws IOException {
        identityreduceDriver.withInput(new Text("dad5c39a"), new ArrayList<>(Arrays.asList(new StringIntWritable(new Text("469b5c29"), 1), new StringIntWritable(new Text("469b5c29"), 1),
                new StringIntWritable(new Text("f50e8801"), 1), new StringIntWritable(new Text("8ddf88eb"), 1))));
        identityreduceDriver.withOutput(new Text("dad5c39a"), new Text("469b5c29,1"));
        identityreduceDriver.withOutput(new Text("dad5c39a"), new Text("469b5c29,1"));
        identityreduceDriver.withOutput(new Text("dad5c39a"), new Text("f50e8801,1"));
        identityreduceDriver.withOutput(new Text("dad5c39a"), new Text("8ddf88eb,1"));
        identityreduceDriver.runTest();
    }

    @Test
    public void testAverageMapper() throws IOException {
        averageMapperDriver.withInput(new LongWritable(), new Text("2fce95f\t0.6666666666666666"));
        averageMapperDriver.withInput(new LongWritable(), new Text("300cca7f\t0.0"));
        averageMapperDriver.withInput(new LongWritable(), new Text("3036ed17\t0.5"));
        averageMapperDriver.withInput(new LongWritable(), new Text("2fce95f\t0.25"));
        averageMapperDriver.withOutput(new IntWritable(1), new DoubleWritable(0.6666666666666666));
        averageMapperDriver.withOutput(new IntWritable(1), new DoubleWritable(0.0));
        averageMapperDriver.withOutput(new IntWritable(1), new DoubleWritable(0.5));
        averageMapperDriver.withOutput(new IntWritable(1), new DoubleWritable(0.25));
        averageMapperDriver.runTest();
    }


    @Test
    public void testAverageReducer() throws IOException {

        logsamAverageReduceDriver.withInput(new IntWritable(1), new ArrayList<>(Arrays.asList(new DoubleWritable(0.0), new DoubleWritable(1.0), new DoubleWritable(0.5))));
        logsamAverageReduceDriver.withOutput(new Text("average percentage: "), new DoubleWritable(0.5));
        logsamAverageReduceDriver.runTest();
    }

    @Test
    public void testCombiner() throws IOException {


        logsamCombineDriver.withInput(new Text("dad5c39a"), new ArrayList<>(Arrays.asList(new StringIntWritable(new Text("3036ed17"), 1), new StringIntWritable(new Text("3036ed17"), 2),
                new StringIntWritable(new Text("2fce95f"), 1), new StringIntWritable(new Text("300cca7f"), 3))));
        logsamCombineDriver.withOutput(new Text("dad5c39a"), new StringIntWritable(new Text("2fce95f"), 1));
        logsamCombineDriver.withOutput(new Text("dad5c39a"), new StringIntWritable(new Text("300cca7f"), 3));
        logsamCombineDriver.withOutput(new Text("dad5c39a"), new StringIntWritable(new Text("3036ed17"), 3));


        logsamCombineDriver.runTest();
    }

    @Test
    public void testSecondCombiner() throws IOException {
        logsamSecondCombineDriver.withInput(new Text("dad5c39a"), new ArrayList<>(Arrays.asList(new DoubleWritable(1.0), new DoubleWritable(3.0), new DoubleWritable(2.0))));
        logsamSecondCombineDriver.withOutput(new Text("dad5c39a"), new DoubleWritable(2.0));
        logsamSecondCombineDriver.runTest();

    }

}
