package wordcnt;

import mrunit.SMSCDRMapper;
import mrunit.SMSCDRReducer;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Counter;
import org.apache.hadoop.mrunit.mapreduce.MapDriver;
import org.apache.hadoop.mrunit.mapreduce.MapReduceDriver;
import org.apache.hadoop.mrunit.mapreduce.ReduceDriver;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class WordCountTest {
//mapdriver input 1 geändert zu: object, war vorher: intwritable
    //mapreducedriver analog

    MapDriver<Object, Text, Text, IntWritable> mapDriver;
    ReduceDriver<Text, IntWritable, Text, IntWritable> reduceDriver;
    MapReduceDriver<Object, Text, Text, IntWritable, Text, IntWritable> mapReduceDriver;

    @Before
    public void setUp() {

        WordCount.TokenizerMapper mapper = new WordCount.TokenizerMapper();
        WordCount.IntSumReducer reducer = new WordCount.IntSumReducer();

        // SMSCDRMapper mapper = new SMSCDRMapper();
        //SMSCDRReducer reducer = new SMSCDRReducer();
        mapDriver = MapDriver.newMapDriver(mapper);
        reduceDriver = ReduceDriver.newReduceDriver(reducer);
        mapReduceDriver = MapReduceDriver.newMapReduceDriver(mapper, reducer);
    }

    @Test
    public void testMapper() throws IOException {

        mapDriver.withInput(new LongWritable(), new Text(
                "cat cat dog"));


        mapDriver.withOutput(new Text("cat"), new IntWritable(1));
        mapDriver.withOutput(new Text("cat"), new IntWritable(1));
        mapDriver.withOutput(new Text("dog"), new IntWritable(1));

        mapDriver.runTest();

    }

    @Test
    public void testReducer() throws IOException {
        List<IntWritable> valuesSecond = new ArrayList<IntWritable>();
        List<IntWritable> values = new ArrayList<IntWritable>();
        values.add(new IntWritable(1));
        values.add(new IntWritable(1));
        valuesSecond.add(new IntWritable(1));
        reduceDriver.withInput(new Text("cat"), values);
        reduceDriver.withInput(new Text("dog"),valuesSecond);
        reduceDriver.withOutput(new Text("cat"), new IntWritable(2));
        reduceDriver.withOutput(new Text("dog"), new IntWritable(1));
        reduceDriver.runTest();
    }

    @Test
    public void testMapperReducer() throws IOException {
        mapReduceDriver.withInput(new LongWritable(), new Text(
                "cat cat dog"));
        mapReduceDriver.withOutput(new Text("cat"), new IntWritable(2));
        mapReduceDriver.withOutput(new Text("dog"), new IntWritable(1));

        mapReduceDriver.runTest();
    }

}
